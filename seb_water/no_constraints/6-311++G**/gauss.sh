#!/bin/bash

####### Job submission for amarel,perceval 
#SBATCH --partition=main # Partition (job queue) 
#SBATCH --job-name=g16_smp # Assign an 8-character name to your job 
###SBATCH -J $INPUT
#SBATCH --nodes=1 # Number of nodes 
#SBATCH --ntasks=1 # Total # of tasks across all nodes 
#SBATCH --cpus-per-task=24 # Number of cores per task
#SBATCH --mem=65000     # Real memory required (MB) +1GB of %mem Gaussian bug 
#SBATCH --time=1-00:00:00 # Total run time limit (HH:MM:SS) 
#SBATCH --output=slurm.%N.%j.out # STDOUT output file 
#SBATCH --error=slurm.%N.%j.out # STDERR output file 
#SBATCH --export=ALL # Export you current env to the job env 
##SBATCH --exclude=cuda[001-008]

### Gaussian does not use MPI, only OpenMP - one node!!
### make sure that number of task in slurm is the same as 
### number of %nprocs in input/com file
### same with memory
### --ntasks == %nprocs
### --mem    == %mem + 1GB -> known gaussian bug 

#### CHECKING the file name

if test -z "$1"
then
        echo "Input file is not set"
        echo "USAGE sbatch g16_submit_amarel.sh  <gaus_input.com>"
        exit  ## EXIT the program
else
echo "Processing file... $1"
fi


### Calculating number or cpus and memory per node for gaussian input file
cpus=$((SLURM_CPUS_PER_TASK-1))
mems=$((SLURM_MEM_PER_NODE/1000))
mems=$((mems-1))
### Writing gaussian configuration header into input

INPUT=$1 ## assign an input file
#INPUT=h2o.com
OUTPUT=${INPUT}_${SLURM_CPUS_PER_TASK}cores_out.${SLURM_JOB_ID}


echo "%nproc=$SLURM_CPUS_PER_TASK
%mem=${mems}gb
%chk=${OUTPUT}.chk
" > gaus.input.simple.${SLURM_JOB_ID}

cat $INPUT >> gaus.input.simple.${SLURM_JOB_ID}
echo "INPUT GAUSSIAN FILE"
echo "==========================================================="
cat gaus.input.simple.${SLURM_JOB_ID}
echo "==========================================================="

WORKDIR=$(pwd)
SRUN='srun -N '$SLURM_NNODES' -n '$SLURM_NNODES #' --ntasks-per-node=10'
#echo " SRUN is $SRUN "

time srun g16 < gaus.input.simple.${SLURM_JOB_ID} > ${OUTPUT}.log
##srun g16 h2o.com

wait

echo "DONE with GAUSSIAN JOB ${SLURM_JOB_ID}, input file $INPUT" 


