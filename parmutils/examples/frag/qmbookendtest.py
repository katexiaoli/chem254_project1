#!/usr/bin/python

import parmed
import parmutils
import copy
import random

def run_ti_equil(fsys,prefix,init,nlam,\
                 mmparm="origmm.parm7",\
                 nstlim=10000,\
                 printfreq=1000):
    #
    # Initial equilibration
    #
    fsys.mdin = parmutils.Mdin()
    fsys.mdin.Set_NVT()
    fsys.mdin.Set_Restart(False)
    fsys.mdin.Set_PrintFreq(printfreq)
    fsys.mdin.cntrl["nstlim"] = nstlim
    fsys.set_mm_parm( mmparm )

    if fsys.disang is not None:
        fsys.mdin.cntrl["nmropt"]=1
        fsys.mdin.DUMPFREQ=nstlim+1
    
    for ilam in range(nlam):
        if nlam == 1:
            lam = 1.
            fsys.mdin.Set_Restart(True)
        else:
            lam = ilam / float(nlam-1)
        fsys.ig = random.randint(1,650663)
        fsys.write_mdin( prefix=prefix,init=init, lam=lam )

        print "lambda = %.8f"%(lam)
        for ifrag,f in enumerate(fsys.frags):
            print "%8s %17.8e %5.2f %3.0f %4i %s"%(\
                f.method,f.get_coef(lam),
                f.mmcharge,f.qmcharge,f.ncores,
                parmutils.ListToSelection(f.atomsel))

def run_ti_prod(fsys,prefix,init,nlam,\
                mmparm="origmm.parm7",\
                nstlim=500000,\
                printfreq=1000,\
                init_from_same_rst=False):
    fsys.mdin = parmutils.Mdin()
    fsys.mdin.Set_NVT()
    fsys.mdin.Set_Restart(True)
    fsys.mdin.Set_PrintFreq(printfreq)
    fsys.mdin.cntrl["nstlim"] = nstlim
    fsys.set_mm_parm( mmparm )
    
    if fsys.disang is not None:
        fsys.mdin.cntrl["nmropt"]=1
        fsys.mdin.DUMPFREQ=nstlim+1
  
    for ilam in range(nlam):
        if nlam == 1:
            lam = 1.
        else:
            lam = ilam / float(nlam-1)
        fsys.ig = random.randint(1,650663)
        fsys.write_mdin( prefix=prefix,init=init, lam=lam, \
                         init_from_same_rst=init_from_same_rst )

        print "lambda = %.8f"%(lam)
        for ifrag,f in enumerate(fsys.frags):
            print "%8s %17.8e %5.2f %3.0f %4i %s"%(\
                f.method,f.get_coef(lam),
                f.mmcharge,f.qmcharge,f.ncores,
                parmutils.ListToSelection(f.atomsel))


def mm2qm_template(nlam,comp,parm,fullqmmask,\
                   equil_nstlim=10000,\
                   prod_nstlim=500000,\
                   printfreq=1000,\
                   nrestart=1,\
                   nprimedrestart=None,\
                   disang=None,\
                   method="AM1D"):
    nrest=nrestart
    prest=nprimedrestart
    if prest is None:
        prest=nrest

    pfreq=printfreq

    fsys = parmutils.FragmentedSys( parm, comp )
    fsys.add_fragment(fullqmmask, coef0=0, coef1=1, method=method )
    fsys.add_mm()
    fsys.sort()
    fsys.check_overlaps()
    fsys.write_parm( "origmm.parm7" )
    fsys.redistribute_residue_charges()
    fsys.write_parm( "fullqm.parm7" )
    fsys.disang = disang
    mm2fullqm = copy.deepcopy(fsys)
    #
    # Initial equilibration
    #
    fsys = mm2fullqm
    name = "mm2fullqm"
    start="fullqm"
    mlam=nlam
    if method != "AM1D" and method != "DFTB":
        mlam=1
        pfreq=500
    run_ti_equil(fsys,"equil.%s"%(name),start,mlam,mmparm="fullqm.parm7",nstlim=equil_nstlim,printfreq=pfreq)
    for irest in range(nrest):
        init="equil.%s"%(name)
        if irest > 0:
           init="prod%02i.%s"%(irest,name)
        run_ti_prod( fsys,"prod%02i.%s"%(irest+1,name),init,mlam,mmparm="fullqm.parm7",nstlim=prod_nstlim,printfreq=pfreq)
    if nrest > 1:
        lams = [ ilam/(nlam-1.) for ilam in range(nlam) ]
        for lam in [ lams[-1] ]:
            fname="prod%02i.%s.%.8f.slurm"%(irest+1,name,lam) 
            fh = open(fname,"a+")
            fh.write("\ncat <<EOF > cpptraj.in\n")
            fh.write("parm %s\n"%(fsys.parmfilename))
            for irest in range(nrest):
                fh.write("trajin prod%02i.%s.mm_%.8f.nc\n"%(irest+1,name,lam))
            fh.write("trajout prod.%s.mm_%.8f.nc\n"%(name,lam))
            fh.write("run\n")
            fh.write("quit\n")
            fh.write("EOF\n\n")
            fh.write("cpptraj -i cpptraj.in\n")
            fh.write("\n")
            fh.close()

    #
    # Optimize the MM parameters to match the QM distribution
    #
    fsys = mm2fullqm
    name = "mm2fullqm"
    fsys.mdin = parmutils.Mdin()
    fsys.mdin.Set_NVT()
    fsys.mdin.Set_Restart(True)
    fsys.set_mm_parm( None )
    reftraj="prod%02i.%s.mm_%.8f.nc"%(nrest,name,1)
    refmdin="prod%02i.%s.mm_%.8f.mdin"%(nrest,name,1)
    if nrest > 1:
       reftraj="prod.%s.mm_%.8f.nc"%(name,1)
    fsys.write_mm_optimization(reftraj,refmdin)
    #
    # (primed) MM -> QM TI
    #
    fsys = mm2fullqm
    mm2fullqm_parmfilename = fsys.parmfilename
    fsys.parmfilename = "trial.parm7"
    for irest in range(prest):
        init_from_same_rst=False
        if irest > 0:
            init="prod%02i.primed2fullqm"%(irest)
        else:
#            if mlam > 1:
#                init="equil.origmm2fullqm"
#            else:
            init="prod%02i.mm2fullqm.mm_%.8f"%(nrest,1)
            init_from_same_rst=True
        run_ti_prod( fsys,"prod%02i.primed2fullqm"%(irest+1),init,nlam,\
                     nstlim=prod_nstlim,\
                     printfreq=printfreq,\
                     mmparm="trial.parm7",
                     init_from_same_rst=init_from_same_rst )
    #
    # (original) MM -> (primed) MM
    #
    # -- can't really do that here because we need to generate trial.parm7 first
    #
    #
    mm2fullqm.parmfilename = mm2fullqm_parmfilename
    ids={}
    iid=0
    dp = file("submitdeptree.sh","w")
    fh = file("submitall.sh","w")
    fh.write("# equilibration\n")
    mlam=nlam
    if method != "AM1D" and method != "DFTB2":
        mlam=1
    for ilam in range(mlam):
        if mlam == 1:
            lam = 1.
        else:
            lam = ilam / ( nlam-1. )
        job = "equil.%s.%.8f.slurm"%("mm2fullqm",lam)
        iid += 1
#        if iid%5 == 0:
#           fh.write("sleep 3\n")
        ids[job] = "j%04i"%(iid)
        fh.write("line=($(sbatch %s)); %s=${line[3]}\n"%(job,ids[job]))
        dp.write("SubJobWithDeps %s\n"%(job))  

    fh.write("# mm->qm production\n")
    for irest in range(nrest):
        init="equil"
        if irest > 0:
           init="prod%02i"%(irest)
        prodjobs=[]
        for ilam in range(mlam):
            if mlam == 1:
                lam = 1.
            else:
                lam = ilam / ( nlam-1. )
            deps = [ "%s.%s.%.8f.slurm"%(init,"mm2fullqm",lam) ]
            job = "prod%02i.%s.%.8f.slurm"%(irest+1,"mm2fullqm",lam)
            prodjobs.append(job)
            iid += 1 
#            if iid%5 == 0:
#               fh.write("sleep 3\n")
            ids[job] = "j%04i"%(iid)
            depline=""
            if len(deps) > 0:
                depline="--dependency=afterok"
                for dep in deps:
                    depline += ":${%s}"%(ids[dep])
            fh.write("line=($(sbatch %s %s)); %s=${line[3]}\n"%(depline,job,ids[job]))
            dp.write("SubJobWithDeps %s %s\n"%(job," ".join(deps)))

    fh.write("# mm optimization\n")
    depline="--dependency=afterok"
    for dep in prodjobs:
        depline += ":${%s}"%(ids[dep])
    job="%s.mmopt.slurm"%(mm2fullqm.parmfilename.replace(".parm7",""))
    optjob=job
    iid += 1            
#    if iid%5 == 0:
#       fh.write("sleep 3\n")

    ids[job] = "j%04i"%(iid)
    fh.write("line=($(sbatch %s %s)); %s=${line[3]}\n"%(depline,job,ids[job]))
    dp.write("SubJobWithDeps %s %s\n"%(optjob," ".join(prodjobs)))

    po = file("submitpostopt.sh","w")
    po.write("mmopt=$(squeue -u ${USER} -o '%i %o' | grep ${PWD}/"+optjob+")\n")
    po.write("depstr=\"\"\n")
    po.write("if [ \"${mmopt}\" != \"\" ]; then\n")
    po.write("    c=(${mmopt})\n")
    po.write("    depstr=\"--dependency=afterok:${c[0]}\"\n")
    po.write("else\n")
    po.write("    if [ ! -e trial.parm7 ]; then\n")
    po.write("        deps=\"\"\n")
    for dep in prodjobs:
        po.write("        line=$(squeue -u ${USER} -o '%i %o' | grep ${PWD}/"+dep+")\n")
        po.write("        if [ \"${line}\" != \"\" ]; then\n")
        po.write("            c=(${line})\n")
        po.write("            deps=\"${deps}:${c[0]}\"\n")
        po.write("        fi\n")
    po.write("        if [ \"${deps}\" != \"\" ]; then\n")
    po.write("            deps=\"--dependency=afterok${deps}\"\n")
    po.write("        fi\n")
    po.write("        line=($(sbatch ${deps} %s)); %s=${line[3]}\n"%(optjob,ids[optjob]))
    po.write("        depstr=\"--dependency=afterok:${%s}\"\n"%(ids[optjob]))
    po.write("    fi\n")
    po.write("fi\n\n")

    
    fh.write("# primed mm -> qm production\n")
    po.write("# primed mm -> qm production\n")
    for irest in range(prest):
        for ilam in range(nlam):
            lam = ilam / ( nlam-1. )
            if irest == 0:
                deps = [ optjob ]
            else:
                deps = [ "prod%02i.%s.%.8f.slurm"%(irest,"primed2fullqm",lam) ]
            job = "prod%02i.%s.%.8f.slurm"%(irest+1,"primed2fullqm",lam)
            iid += 1
#            if iid%5 == 0:
#               fh.write("sleep 3\n")
            ids[job] = "j%04i"%(iid)
            depline=""
            if len(deps) > 0:
                depline="--dependency=afterok"
                for dep in deps:
                    depline += ":${%s}"%(ids[dep])
            fh.write("line=($(sbatch %s %s)); %s=${line[3]}\n"%(depline,job,ids[job]))
            if irest == 0:
                po.write("line=($(sbatch ${depstr} %s)); %s=${line[3]}\n"%(job,ids[job]))
            else:
                po.write("line=($(sbatch %s %s)); %s=${line[3]}\n"%(depline,job,ids[job]))
            dp.write("SubJobWithDeps %s %s\n"%(job," ".join(deps)))
    fh.close()
    po.close()
    dp.close()
 

    
    
def mm2qmff2qm_template(nlam,comp,parm,fullqmmask,fragmasks,\
                        equil_nstlim=10000,\
                        prod_nstlim=500000,\
                        printfreq=1000,\
                        nrestart=1,\
                        nprimedrestart=None,\
                        disang=None,\
                        method="AM1D"):
    nrest=nrestart
    prest=nprimedrestart
    if prest is None:
        prest=nrest
    pfreq=printfreq

    fsys = parmutils.FragmentedSys( parm, comp )
    for fragmask in fragmasks:
        fsys.add_fragment(fragmask, coef0=0, coef1=1, method=method )
    fsys.add_mm()
    fsys.sort()
    fsys.check_overlaps()
    fsys.write_parm( "origmm.parm7" )
    fsys.redistribute_residue_charges()
    fsys.write_parm( "qmff.parm7" )
    fsys.disang=disang
    mm2qmff = copy.deepcopy(fsys)

    fsys = parmutils.FragmentedSys( parm, comp )
    fsys.add_fragment(fullqmmask, coef0=0, coef1=1, method=method )
    for fragmask in fragmasks:
        fsys.add_fragment(fragmask, coef0=1, coef1=0, method=method )
    fsys.add_mm()
    fsys.sort()
    fsys.check_overlaps()
    fsys.redistribute_residue_charges()
    fsys.write_parm( "fullqm.parm7" )
    fsys.disang=disang
    qmff2fullqm = copy.deepcopy(fsys)
    
    #
    # Initial equilibration
    #
    fsys = mm2qmff
    name = "mm2qmff"
    start="qmff"
    mlam=nlam
    if method != "AM1D" and method != "DFTB":
        mlam=1
        pfreq = 500
    run_ti_equil(fsys,"equil.%s"%(name),start,mlam,\
                 mmparm="qmff.parm7",\
                 nstlim=equil_nstlim,\
                 printfreq=pfreq)
    for irest in range(nrest):
        init="equil.%s"%(name)
        if irest > 0:
            init="prod%02i.%s"%(irest,name)
        run_ti_prod( fsys,"prod%02i.%s"%(irest+1,name),init,mlam,\
                     mmparm="qmff.parm7",\
                     nstlim=prod_nstlim,\
                     printfreq=pfreq)
    if nrest > 1:
        lams = [ ilam/(nlam-1.) for ilam in range(nlam) ]
        for lam in [ lams[-1] ]:
            fname="prod%02i.%s.%.8f.slurm"%(irest+1,name,lam)
            fh = open(fname,"a+")
            fh.write("\ncat <<EOF > cpptraj.in\n")
            fh.write("parm %s\n"%(fsys.parmfilename))
            for irest in range(nrest):
                fh.write("trajin prod%02i.%s.mm_%.8f.nc\n"%(irest+1,name,lam))
            fh.write("trajout prod.%s.mm_%.8f.nc\n"%(name,lam))
            fh.write("run\n")
            fh.write("quit\n")
            fh.write("EOF\n\n")
            fh.write("cpptraj -i cpptraj.in\n")
            fh.write("\n")
            fh.close()

    #
    # Optimize the MM parameters to match the QMFF distribution
    #
    fsys = mm2qmff
    name = "mm2qmff"
    fsys.mdin = parmutils.Mdin()
    fsys.mdin.Set_NVT()
    fsys.mdin.Set_Restart(True)
    fsys.set_mm_parm( None )
    reftraj="prod%02i.%s.mm_%.8f.nc"%(nrest,name,1)
    refmdin="prod%02i.%s.mm_%.8f.mdin"%(nrest,name,1)
    if nrest > 1:
       reftraj="prod.%s.mm_%.8f.nc"%(name,1)
    fsys.write_mm_optimization(reftraj,refmdin)
    #
    # (primed) MM -> QMFF TI
    #
    fsys = mm2qmff
    mm2qmff_parmfilename = fsys.parmfilename
    fsys.parmfilename = "trial.parm7"
    for irest in range(prest):
        init_from_same_rst=False
        if irest > 0:
            init="prod%02i.primed2qmff"%(irest)
        else:
#            if mlam > 1:
#                init="equil.origmm2qmff"
#            else:
            init="prod%02i.mm2qmff.mm_%.8f"%(nrest,1)
            init_from_same_rst=True
        run_ti_prod( fsys,"prod%02i.primed2qmff"%(irest+1),init,nlam,\
                     mmparm="trial.parm7",\
                     nstlim=prod_nstlim,\
                     printfreq=printfreq,\
                     init_from_same_rst=init_from_same_rst)
    #
    # QMFF -> QM TI
    #
    fsys = qmff2fullqm
    fsys.parmfilename = "trial.parm7"
    qmff2fullqm_parmfilename = fsys.parmfilename
    for irest in range(prest):
        init="prod%02i.primed2qmff.mm_%.8f"%(prest,1)
        init_from_same_rst=True
        if irest > 0:
            init="prod%02i.qmff2fullqm"%(irest)
            init_from_same_rst=False
        run_ti_prod( fsys,"prod%02i.qmff2fullqm"%(irest+1),init,nlam,\
                     mmparm="trial.parm7",\
                     nstlim=prod_nstlim,\
                     printfreq=printfreq,\
                     init_from_same_rst=init_from_same_rst)
    #
    # (original) MM -> (primed) MM
    #
    # -- can't really do that here because we need to generate trial.parm7 first
    #
    #
    mm2qmff.parmfilename = mm2qmff_parmfilename
    qmff2fullqm.parmfilename = qmff2fullqm_parmfilename
    ids={}
    iid=0
    dp = file("submitdeptree.sh","w")
    fh = file("submitall.sh","w")
    fh.write("# equilibration\n")
    for ilam in range(mlam):
        if mlam == 1:
            lam = 1.
        else:
            lam = ilam / ( nlam-1. )
        job = "equil.%s.%.8f.slurm"%("mm2qmff",lam)
        iid += 1
#        if iid%5 == 0:
#           fh.write("sleep 3\n")
        ids[job] = "j%04i"%(iid)
        fh.write("line=($(sbatch %s)); %s=${line[3]}\n"%(job,ids[job]))
        dp.write("SubJobWithDeps %s\n"%(job))
        
    fh.write("# mm->qmff production\n")
    for irest in range(nrest):
        prodjobs=[]
        for ilam in range(mlam):
            if mlam == 1:
                lam = 1.
            else:
                lam = ilam / ( nlam-1. )
            if irest==0:
                deps = [ "equil.%s.%.8f.slurm"%("mm2qmff",lam) ]
            else:
                deps = [ "prod%02i.%s.%.8f.slurm"%(irest,"mm2qmff",lam) ]
            job = "prod%02i.%s.%.8f.slurm"%(irest+1,"mm2qmff",lam)
            prodjobs.append(job)
            iid += 1
#            if iid%5 == 0:
#               fh.write("sleep 3\n")
            ids[job] = "j%04i"%(iid)
            depline=""
            if len(deps) > 0:
                depline="--dependency=afterok"
                for dep in deps:
                    depline += ":${%s}"%(ids[dep])
            fh.write("line=($(sbatch %s %s)); %s=${line[3]}\n"%(depline,job,ids[job]))
            dp.write("SubJobWithDeps %s %s\n"%(job," ".join(deps)))

    fh.write("# mm optimization\n")
    depline="--dependency=afterok"
    for dep in prodjobs:
        depline += ":${%s}"%(ids[dep])
    job="%s.mmopt.slurm"%(mm2qmff.parmfilename.replace(".parm7",""))
    optjob=job
    iid += 1
#    if iid%5 == 0:
#       fh.write("sleep 3\n")
    ids[job] = "j%04i"%(iid)
    fh.write("line=($(sbatch %s %s)); %s=${line[3]}\n"%(depline,job,ids[job]))
    dp.write("SubJobWithDeps %s %s\n"%(job," ".join(prodjobs)))

    po = file("submitpostopt.sh","w")
    po.write("mmopt=$(squeue -u ${USER} -o '%i %o' | grep ${PWD}/"+optjob+")\n")
    po.write("depstr=\"\"\n")
    po.write("if [ \"${mmopt}\" != \"\" ]; then\n")
    po.write("    c=(${mmopt})\n")
    po.write("    depstr=\"--dependency=afterok:${c[0]}\"\n")
    po.write("else\n")
    po.write("    if [ ! -e trial.parm7 ]; then\n")
    po.write("        deps=\"\"\n")
    for dep in prodjobs:
        po.write("        line=$(squeue -u ${USER} -o '%i %o' | grep ${PWD}/"+dep+")\n")
        po.write("        if [ \"${line}\" != \"\" ]; then\n")
        po.write("            c=(${line})\n")
        po.write("            deps=\"${deps}:${c[0]}\"\n")
        po.write("        fi\n")
    po.write("        if [ \"${deps}\" != \"\" ]; then\n")
    po.write("            deps=\"--dependency=afterok${deps}\"\n")
    po.write("        fi\n")
    po.write("        line=($(sbatch ${deps} %s)); %s=${line[3]}\n"%(optjob,ids[optjob]))
    po.write("        depstr=\"--dependency=afterok:${%s}\"\n"%(ids[optjob]))
    po.write("    fi\n")
    po.write("fi\n\n")
    
    fh.write("# primed mm -> qmff production\n")
    po.write("# primed mm -> qmff production\n")
    for irest in range(prest):
        for ilam in reversed(range(nlam)):
            lam = ilam / ( nlam-1. )
            if irest==0:
                deps = [ optjob ]
            else:
                deps = [ "prod%02i.%s.%.8f.slurm"%(irest,"primed2qmff",lam) ]
            job = "prod%02i.%s.%.8f.slurm"%(irest+1,"primed2qmff",lam)
            iid += 1
#            if iid%5 == 0:
#               fh.write("sleep 3\n")
            ids[job] = "j%04i"%(iid)
            depline=""
            if len(deps) > 0:
                depline="--dependency=afterok"
                for dep in deps:
                    depline += ":${%s}"%(ids[dep])
            fh.write("line=($(sbatch %s %s)); %s=${line[3]}\n"%(depline,job,ids[job]))
            if irest == 0:
                po.write("line=($(sbatch ${depstr} %s)); %s=${line[3]}\n"%(job,ids[job]))
            else:
                po.write("line=($(sbatch %s %s)); %s=${line[3]}\n"%(depline,job,ids[job]))
            dp.write("SubJobWithDeps %s %s\n"%(job," ".join(deps)))


    fh.write("# qmff -> qm production\n")
    po.write("# qmff -> qm production\n")
    for irest in range(prest):
        for ilam in reversed(range(nlam)):
            lam = ilam / ( nlam-1. )
            if irest==0:
                deps = [ "prod%02i.%s.%.8f.slurm"%(nrest,"primed2qmff",1) ]
            else:
                deps = [ "prod%02i.%s.%.8f.slurm"%(irest,"qmff2fullqm",lam) ]
            job = "prod%02i.%s.%.8f.slurm"%(irest+1,"qmff2fullqm",lam)
            iid += 1
#            if iid%5 == 0:
#               fh.write("sleep 3\n")
            ids[job] = "j%04i"%(iid)
            depline=""
            if len(deps) > 0:
                depline="--dependency=afterok"
                for dep in deps:
                    depline += ":${%s}"%(ids[dep])
            fh.write("line=($(sbatch %s %s)); %s=${line[3]}\n"%(depline,job,ids[job]))
            po.write("line=($(sbatch %s %s)); %s=${line[3]}\n"%(depline,job,ids[job]))
            dp.write("SubJobWithDeps %s %s\n"%(job," ".join(deps)))

    fh.close()
    po.close()
    dp.close()




def mm2qmff2qm_analyze(nlam,comp,parm,fullqmmask,fragmasks):
    import glob
    import os
    import sys
    import traceback
    from collections import defaultdict as ddict
    
    fsys = parmutils.FragmentedSys( parm, comp )
    for fragmask in fragmasks:
        fsys.add_fragment(fragmask, coef0=0, coef1=1, method="AM1D" )
    fsys.add_mm()
    fsys.sort()
    fsys.check_overlaps()
    fsys.redistribute_residue_charges()
    mm2qmff = copy.deepcopy(fsys)

    fsys = parmutils.FragmentedSys( parm, comp )
    fsys.add_fragment(fullqmmask, coef0=0, coef1=1, method="AM1D" )
    for fragmask in fragmasks:
        fsys.add_fragment(fragmask, coef0=1, coef1=0, method="AM1D" )
    fsys.add_mm()
    fsys.sort()
    fsys.check_overlaps()
    fsys.redistribute_residue_charges()
    qmff2fullqm = copy.deepcopy(fsys)

    try:
        mm2qmff.mdouts_to_pymbar( name="origmm2qmff", nlam=nlam )
    except:
        print "\n\nskipping origmm2qmff"
        #traceback.print_exc(file=sys.stdout)
    try:
        mm2qmff.mdouts_to_pymbar( name="mm2qmff", nlam=nlam )
    except:
        print "\n\nfailed to analyze mm2qmff"
        traceback.print_exc(file=sys.stdout)
    try:
        mm2qmff.mdouts_to_pymbar( name="primed2qmff", nlam=nlam )
    except:
        print "\n\nfailed to analyze primed2qmff"
        traceback.print_exc(file=sys.stdout)
    try:
        qmff2fullqm.mdouts_to_pymbar( name="qmff2fullqm", nlam=nlam )
    except:
        print "\n\nfailed to analyze qmff2fullqm"
        traceback.print_exc(file=sys.stdout)
            

def mm2qm_analyze(nlam,comp,parm,fullqmmask):
    import glob
    import os
    import sys
    import traceback
    from collections import defaultdict as ddict
    
    fsys = parmutils.FragmentedSys( parm, comp )
    fsys.add_fragment(fullqmmask, coef0=0, coef1=1, method="AM1D" )
    fsys.add_mm()
    fsys.sort()
    fsys.check_overlaps()
    fsys.redistribute_residue_charges()
    mm2qm = copy.deepcopy(fsys)

    try:
        mm2qm.mdouts_to_pymbar( name="origmm2fullqm", nlam=nlam )
    except:
        print "\n\nskipping origmm2fullqm"
#        traceback.print_exc(file=sys.stdout)
        
    try:
        mm2qm.mdouts_to_pymbar( name="mm2fullqm", nlam=nlam )
    except:
        print "\n\nfailed to analyze mm2fullqm"
        traceback.print_exc(file=sys.stdout)
        
    try:
        mm2qm.mdouts_to_pymbar( name="primed2fullqm", nlam=nlam )
    except:
        print "\n\nfailed to analyze primed2fullqm"
        traceback.print_exc(file=sys.stdout)
       
