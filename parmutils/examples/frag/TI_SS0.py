#!/usr/bin/python

import parmed
import parmutils
import copy

from qmbookendtest import *

equil_nstlim = 10000
prod_nstlim  = 500000
printfreq    = 1000
nrestart     = 2
nnodes       = 2
nlam         = 11
comp = parmutils.CALIBURN(nnodes)
parmfile="SS0.parm7"
rstfile ="SS0.rst7"
fullqmmask="@110-123,526-537"
fragmasks=["@110-123","@526-537"]

parm = parmutils.OpenParm(parmfile,rstfile)

analyze = True


if len(fragmasks) > 0:
    if analyze:
        mm2qmff2qm_analyze(nlam,comp,parm,fullqmmask,fragmasks)
    else:
        mm2qmff2qm_template(nlam,comp,parm,fullqmmask,fragmasks,\
            equil_nstlim=equil_nstlim,prod_nstlim=prod_nstlim,\
            printfreq=printfreq,nrestart=nrestart)
else:
    if analyze:
        mm2qm_analyze(nlam,comp,parm,fullqmmask)
    else:
        mm2qm_template(nlam,comp,parm,fullqmmask,\
            equil_nstlim=equil_nstlim,prod_nstlim=prod_nstlim,\
            printfreq=printfreq,nrestart=nrestart)


