#!/usr/bin/python

import parmed
import parmutils
import copy

from qmbookendtest import *


equil_nstlim = 10000
prod_nstlim  = 500000
printfreq    = 1000
nrestart     = 2
nnodes       = 1
nlam         = 11
comp = parmutils.PERCEVAL(nnodes)

parmfile="refAH.parm7"
rstfile ="refAH.rst7"
fullqmmask=":1-2"
fragmasks=[]


parm = parmutils.OpenParm(parmfile,rstfile)

analyze = False


if len(fragmasks) > 0:
    if analyze:
        mm2qmff2qm_analyze(nlam,comp,parm,fullqmmask,fragmasks)
    else:
        mm2qmff2qm_template(nlam,comp,parm,fullqmmask,fragmasks,\
            equil_nstlim=equil_nstlim,prod_nstlim=prod_nstlim,\
            printfreq=printfreq,nrestart=nrestart)
else:
    if analyze:
        mm2qm_analyze(nlam,comp,parm,fullqmmask)
    else:
        mm2qm_template(nlam,comp,parm,fullqmmask,\
            equil_nstlim=equil_nstlim,prod_nstlim=prod_nstlim,\
            printfreq=printfreq,nrestart=nrestart)






