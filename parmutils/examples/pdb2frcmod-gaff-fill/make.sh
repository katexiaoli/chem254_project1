#!/bin/bash
set -e
set -u

source ${HOME}/devel/git/DataBase/FrankensteinResidues/leap/bashrc

#
# Create a 2-aminopurine residue
# We have to move H1 to H6 and delete O6
#

start_leaprc > tleap.in
cat <<EOF >> tleap.in

AP2 = sequence { G }
set AP2 name "AP2"
set AP2.1 name "AP2"

remove AP2.1 AP2.1.O6
remove AP2.1 AP2.1.H1


H6 = createAtom "H6" "H5" 0.0000
add AP2.1 H6
bond AP2.1.C6 AP2.1.H6
select AP2.1.H6
relax AP2
deselect AP2.1.H6


savepdb AP2 AP2.pdb

quit
EOF

tleap -s -f tleap.in




# ###############################################################
#
# This will create AP2.mol2 and AP2.frcmod
# Specifically, it will use antechamber to re-assign amber atom-types
# and the frcmod will contain the GAFF parameters for any missing
# values
#

parmutils-pdb2frcmod-gaff-fill.py AP2.pdb
rm AP2.tleap.in


start_leaprc > tleap.in
cat <<EOF >> tleap.in

loadamberparams AP2.frcmod
AP2 = loadmol2 AP2.mol2

saveoff  AP2 AP2.lib
saveamberparm AP2 AP2.parm7 AP2.rst7

quit
EOF

tleap -s -f tleap.in

rm tleap.in leap.log 
# ###############################################################




exit

#
# The code below will assign amber atom-types, but the parm7
# won't be generated because there are missing parameters
#

# ###############################################################
# #
# # assign amber atom types
# #
# antechamber -i AP2.pdb -fi pdb -o amber.mol2 -fo mol2 -at amber -pf -an n
# rm ANTECHAMBER* ATOMTYPE*


# start_leaprc > tleap.in
# cat <<EOF >> tleap.in

# AP2 = loadmol2 amber.mol2

# savepdb AP2 amber.pdb
# saveoff  AP2 amber.lib
# savemol2 AP2 amber.mol2 1
# saveamberparm AP2 amber.parm7 amber.rst7

# quit
# EOF

# tleap -s -f tleap.in
# rm -f amber.off amber.mol2 amber.pdb amber.parm7 amber.rst7
# ###############################################################
