#!/bin/bash


tar -xzvf origFiles.tar.gz


for prefix in GHAH GHAX GXAH GXAX; do

    # ===========================================
    # given:
    #   origXXXX.parm7 origXXXX.rst7 origXXXX.nc
    # remove the dummy atoms and create:
    #   XXXX.parm7 XXXX.rst7 XXXX.nc

    # this strips the parm and rst
    parmutils-StripDummyAtoms.py -p orig${prefix}.parm7 -c orig${prefix}.rst7
    # this strips the nc
    parmutils-StripDummyAtoms.py -p orig${prefix}.parm7 -c orig${prefix}.rst7 -x orig${prefix}.nc

    mv stripped_orig${prefix}.parm7 ${prefix}.parm7
    mv stripped_orig${prefix}.rst7  ${prefix}.rst7
    mv stripped_orig${prefix}.nc    ${prefix}.nc

    # ===========================================
    # given XXXX.nc, create XXXX.rst7.1, XXXX.rst7.2, ... for each frame
    
    cat <<EOF > ptraj.in
parm ${prefix}.parm7
trajin ${prefix}.nc
trajout ${prefix}.rst7 multi
run
quit
EOF

    cpptraj -i ptraj.in
    rm ptraj.in

    # ===========================================
    # given XXXX.rst7.1, XXXX.rst7.2 ..., rename them to
    # XXXX.000001.rst7 XXXX.000002.rst7 ...
    
    for rst in ${prefix}.rst7.*; do
	ext="${rst##*.}"
	f=$(printf "${prefix}.%06i.rst7" ${ext})
	mv ${rst} ${f}
    done

    # ===========================================
    # these files are not needed
    rm ${prefix}.nc ${prefix}.rst7
    
done


# Write the mdin and slurm files

python2.7 ./MakeInputs.py


# Change the QM theory to non-hybrid-GGA PBE

for f in *.mdin; do
    sed -i 's/PBE0/PBE/g' $f
done


