#!/usr/bin/python

import parmed
import parmutils
import parmutils.respfit as respfit
import glob

#####################################################################################
#####################################################################################

comp = parmutils.CALIBURN(numnodes=2)
comp.amberhome="${HOME}/caliburn/hfdfamber"

labels=["GHAH","GHAX","GXAH","GXAX"]

for ires in [6,7,45]:
    fit = respfit.ResidueResp(comp,ires)
    for prefix in labels:
        rsts = glob.glob(prefix+".0*.rst7")
        fit.add_state( prefix, prefix+".parm7", rsts )
    fit.write_mdin()
