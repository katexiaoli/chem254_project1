#!/usr/bin/python

import sys
import glob
import parmed
import parmutils
import parmutils.respfit as respfit

#####################################################################################
#####################################################################################

comp = parmutils.CALIBURN(numnodes=2)
comp.amberhome="${HOME}/caliburn/hfdfamber"

labels=["GHAH","GHAX","GXAH","GXAX"]



sys.stdout.write("""#!/usr/bin/python
from collections import defaultdict as ddict

""")


sys.stdout.write("""def OnlyBaseA7andBaseG45change_WithBackboneConstraint():
    QQS = ddict( lambda: ddict( lambda: ddict(float) ) )
    # U6:  common-frames:         common-states: :6|(:7&@P,OP*,*')   charge-con: P,OP*,*5'*
    # A7:  common-frames: OP1=OP2 common-states: :8|(:7&@P,OP*,*')   charge-con: P,OP*,*5'*
    # G45: common-frames: OP1=OP2 common-states: :46|(:45&@P,OP*,*') charge-con: P,OP*,*5'*

""")

for ires in [6,7,45]:
    fit = respfit.ResidueResp(comp,ires)
    for prefix in labels:
        rsts = glob.glob(prefix+".0*.rst7")
        fit.add_state( prefix, prefix+".parm7",rsts )
    if False:
        fit.write_mdin()
    elif True:
        fit.multimolecule_fit()
        fit.apply_backbone_restraint()
        if ires == 6:
            fit.apply_equiv_nonbridge(False)
            fit.perform_fit(":6|(:7&@P,OP*,*')") # equiv atoms across states
        elif ires == 7:
            fit.perform_fit(":8|(:7&@P,OP*,*')") # equiv atoms across states
        elif ires == 45:
            fit.perform_fit(":46|(:45&@P,OP*,*')") # equiv atoms across states
        else:
            fit.perform_fit("@P,OP*,*'") # equiv atoms across states
        fit.read_respfile()
        #fit.preserve_residue_charges_by_shifting()
        fit.print_resp()

sys.stdout.write("""
    return QQS

""")



sys.stdout.write("""def OnlyBaseA7andBaseG45change_WithoutBackboneConstraint():
    QQS = ddict( lambda: ddict( lambda: ddict(float) ) )
    # U6:  common-frames:         common-states: :6|(:7&@P,OP*,*')   charge-con: none
    # A7:  common-frames: OP1=OP2 common-states: :8|(:7&@P,OP*,*')   charge-con: none
    # G45: common-frames: OP1=OP2 common-states: :46|(:45&@P,OP*,*') charge-con: none

""")

for ires in [6,7,45]:
    fit = respfit.ResidueResp(comp,ires)
    for prefix in labels:
        rsts = glob.glob(prefix+".0*.rst7")
        fit.add_state( prefix, prefix+".parm7",rsts )
    if False:
        fit.write_mdin()
    elif True:
        fit.multimolecule_fit()
        #fit.apply_backbone_restraint()
        if ires == 6:
            fit.apply_equiv_nonbridge(False)
            fit.perform_fit(":6|(:7&@P,OP*,*')") # equiv atoms across states
        elif ires == 7:
            fit.perform_fit(":8|(:7&@P,OP*,*')") # equiv atoms across states
        elif ires == 45:
            fit.perform_fit(":46|(:45&@P,OP*,*')") # equiv atoms across states
        else:
            fit.perform_fit("@P,OP*,*'") # equiv atoms across states
        fit.read_respfile()
        fit.preserve_residue_charges_by_shifting()
        fit.print_resp()

sys.stdout.write("""
    return QQS

""")

