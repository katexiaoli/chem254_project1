#!/bin/bash
set -e
set -u

source ${HOME}/devel/git/DataBase/FrankensteinResidues/leap/bashrc



###################################################
#
# Create a Guanine nucleobase (GB) and
# a 2-aminopurine nucleobase (APB)
# Methylate these bases at the N9 position
#

start_leaprc > tleap.in
cat <<EOF >> tleap.in

#
# 2-aminopurine
#

APB = sequence { DG }

set APB name "APB"
set APB.1 name "APB"


remove APB.1 APB.1.O6
remove APB.1 APB.1.H1

H6 = createAtom "H6" "H5" 0.1404
add APB.1 H6
bond APB.1.C6 APB.1.H6
select APB.1.H6
relax APB
deselect APB.1.H6

#
# remove the phosphate and sugar
#

remove APB.1 APB.1.P
remove APB.1 APB.1.OP1
remove APB.1 APB.1.OP2
remove APB.1 APB.1.O5'
remove APB.1 APB.1.C5'
remove APB.1 APB.1.H5'
remove APB.1 APB.1.H5''
remove APB.1 APB.1.C4'
remove APB.1 APB.1.H4'
remove APB.1 APB.1.O4'
remove APB.1 APB.1.C1'
remove APB.1 APB.1.H1'
remove APB.1 APB.1.C3'
remove APB.1 APB.1.H3'
remove APB.1 APB.1.C2'
remove APB.1 APB.1.H2'
remove APB.1 APB.1.O3'
##########################
# DNA
remove APB.1 APB.1.H2''
# RNA
#remove APB.1 APB.1.O2'
#remove APB.1 APB.1.HO2'
##########################


#
# methylate at N9
#

C9 = createAtom "C9" "CA" -0.2886
add APB.1 C9
bond APB.1.N9 APB.1.C9
select APB.1.C9
relax APB
deselect APB.1.C9

H91 = createAtom "H91" "HA" 0.1259
add APB.1 H91
bond APB.1.C9 APB.1.H91
select APB.1.H91
relax APB
deselect APB.1.H91

H92 = createAtom "H92" "HA" 0.1259
add APB.1 H92
bond APB.1.C9 APB.1.H92
select APB.1.H92
relax APB
deselect APB.1.H92

H93 = createAtom "H93" "HA" 0.1259
add APB.1 H93
bond APB.1.C9 APB.1.H93
select APB.1.H93
relax APB
deselect APB.1.H93


select APB.1.H91
select APB.1.H92
select APB.1.H93
relax APB
deselect APB.1.H91
deselect APB.1.H92
deselect APB.1.H93

savepdb APB APB.pdb
savemol2 APB APB.mol2 1


####################################


#
# guanine nucleobase
#

GB = sequence { DG }
set GB name "GB"
set GB.1 name "GB"


#
# remove the phosphate and sugar
#

remove GB.1 GB.1.P
remove GB.1 GB.1.OP1
remove GB.1 GB.1.OP2
remove GB.1 GB.1.O5'
remove GB.1 GB.1.C5'
remove GB.1 GB.1.H5'
remove GB.1 GB.1.H5''
remove GB.1 GB.1.C4'
remove GB.1 GB.1.H4'
remove GB.1 GB.1.O4'
remove GB.1 GB.1.C1'
remove GB.1 GB.1.H1'
remove GB.1 GB.1.C3'
remove GB.1 GB.1.H3'
remove GB.1 GB.1.C2'
remove GB.1 GB.1.H2'
remove GB.1 GB.1.O3'
##########################
# DNA
remove GB.1 GB.1.H2''
# RNA
#remove GB.1 GB.1.O2'
#remove GB.1 GB.1.HO2'
##########################


#
# methylate at N9
#


C9 = createAtom "C9" "CA" -0.2886
add GB.1 C9
bond GB.1.N9 GB.1.C9
select GB.1.C9
relax GB
deselect GB.1.C9

H91 = createAtom "H91" "HA" 0.1259
add GB.1 H91
bond GB.1.C9 GB.1.H91
select GB.1.H91
relax GB
deselect GB.1.H91

H92 = createAtom "H92" "HA" 0.1259
add GB.1 H92
bond GB.1.C9 GB.1.H92
select GB.1.H92
relax GB
deselect GB.1.H92

H93 = createAtom "H93" "HA" 0.1259
add GB.1 H93
bond GB.1.C9 GB.1.H93
select GB.1.H93
relax GB
deselect GB.1.H93


select GB.1.H91
select GB.1.H92
select GB.1.H93
relax GB
deselect GB.1.H91
deselect GB.1.H92
deselect GB.1.H93


savepdb GB GB.pdb
savemol2 GB GB.mol2 1


quit
EOF

tleap -s -f tleap.in


###################################################
#
# Re-assign the amber atom-types and create frcmod files
# containing the GAFF parameters for missing values
#

parmutils-pdb2frcmod-gaff-fill.py APB.mol2
parmutils-pdb2frcmod-gaff-fill.py GB.mol2


###################################################
#
# Gaussian geometry optimization
#

babel GB.pdb GB.xyz

cat <<EOF > GB.g09.opt.inp
%NPROC=12
%MEM=8GB
# HF/6-31G* OPT SCF=TIGHT

title

0 1
EOF
tail -n +3 GB.xyz >> GB.g09.opt.inp
echo "" >> GB.g09.opt.inp

if [ ! -e GB.g09.opt.out ]; then
    g09 < GB.g09.opt.inp > GB.g09.opt.out
fi

# save optimized coordinates to pdb file
parmutils-g092pdb.py GB.g09.opt.out GB.pdb > GBopt.pdb
mv GBopt.pdb GB.pdb




###################################################
#
# Gaussian geometry optimization
#

babel APB.pdb APB.xyz

cat <<EOF > APB.g09.opt.inp
%NPROC=12
%MEM=8GB
# HF/6-31G* OPT SCF=TIGHT

title

0 1
EOF
tail -n +3 APB.xyz >> APB.g09.opt.inp
echo "" >> APB.g09.opt.inp


if [ ! -e APB.g09.opt.out ]; then
    g09 < APB.g09.opt.inp > APB.g09.opt.out
fi

# save optimized coordinates to pdb file
parmutils-g092pdb.py APB.g09.opt.out APB.pdb > APBopt.pdb
mv APBopt.pdb APB.pdb



###################################################
#
# Resp fit using RED scripts
#
./resp_AP2.sh | tee RESP_AP2.TXT

# The 2-aminopurine nucleobase charges -- this is
# the relevant output from ./RunRespFit.sh



function set_charge_AP2()
{
   selection="$1"
   cat <<EOF
set ${selection}.C2    charge   0.950843
set ${selection}.C4    charge   0.246043
set ${selection}.C5    charge   0.287143
set ${selection}.C6    charge   0.092943
set ${selection}.C8    charge   0.200043
set ${selection}.H21   charge   0.422243
set ${selection}.H22   charge   0.422243
set ${selection}.H6    charge   0.140443
set ${selection}.H8    charge   0.177143
set ${selection}.N1    charge  -0.644357
set ${selection}.N2    charge  -0.982857
set ${selection}.N3    charge  -0.722157
set ${selection}.N7    charge  -0.670657
set ${selection}.N9    charge  -0.007857
EOF
}



###################################################
#
# Create a 2-aminopurine residue
#

start_leaprc > tleap.in
cat <<EOF >> tleap.in

AP2 = sequence { DG5 DG DG3 }

set AP2 name "AP2"
set AP2.2 name "AP2"


remove AP2.2 AP2.2.O6
remove AP2.2 AP2.2.H1

H6 = createAtom "H6" "H5" 0.0000
add AP2.2 H6
bond AP2.2.C6 AP2.2.H6
select AP2.2.H6
relax AP2
deselect AP2.2.H6

savemol2 AP2 AP2.mol2 1
quit

EOF

tleap -s -f tleap.in



###################################################
#
# Assign amber atomtypes and create a frcmod for
# the missing parameter values
#

parmutils-pdb2frcmod-gaff-fill.py --mask=":2" AP2.mol2



###################################################
#
# Assign the charges from our RESP fit and
# write the lib file
#

start_leaprc > tleap.in
cat <<EOF >> tleap.in

loadamberparams AP2.frcmod
AP2 = loadmol2 AP2.mol2
set AP2.1 restype  nucleic
set AP2.1 connect0 AP2.1.P
set AP2.1 connect1 AP2.1.O3'
set AP2   head     AP2.1.P
set AP2   tail     AP2.1.O3'
EOF

set_charge_AP2 "AP2.1" >> tleap.in

cat <<EOF >> tleap.in

savepdb  AP2 AP2.pdb
saveoff  AP2 AP2.lib
savemol2 AP2 AP2.mol2 1
saveamberparm AP2 AP2.parm7 AP2.rst7

quit
EOF

tleap -s -f tleap.in



###################################################
#
# Remove temporary work files
#

for f in APB*; do
    if [ "${f}" == "APB.g09.opt.out" ]; then
	continue
    else
	rm -fr ${f}
    fi	
done
for f in GB*; do
    if [ "${f}" == "GB.g09.opt.out" ]; then
	continue
    else
	rm -fr ${f}
    fi	
done

rm -f *.pyc
rm -f *~
rm -f AP2.amber*
rm -f tleap.in
rm -f leap.log
