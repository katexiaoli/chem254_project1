#!/bin/bash

REDHOME=${HOME}/devel/projects/RED_RESP_CHARGE_FITTING/src/RED-III.52-Tools-Files


cat <<'EOF' > ApplyConstraints.py
#!/usr/bin/env python2.7
import sys

use_atom_charge_constraints = False

#####################################################################

def IsConstrained(name):
    result=False
    if name[-1] == "'" or name in [ "C9", "H91", "H92", "H93" ]: ## EDIT THIS
       result=True
    return result

#####################################################################

if __name__ == '__main__':

  fname = sys.argv[1]
  constraints = sys.argv[2:]
  ncon = len(constraints)/2
  #print "ncon=",len(constraints)
  #for arg in constraints:
  #   print arg
  #print ""

  qdict={}
  for line in file(fname,"r"):
      cols=line.strip().split()
      if len(cols) == 10:
         qdict[ cols[1] ] = float( cols[-2] )
         print cols[1],"=>",float( cols[-2] )
      elif len(cols) == 9:
         qdict[ cols[1] ] = float( cols[-1] )
         print cols[1],"=>",float( cols[-1] )
      elif "BOND" in line:
         break


  charges=[]
  names=[]
  for line in file("tmp.p2n","r"):
     cols=line.strip().split()
     if cols[0] == "ATOM":
        print "looking for qdict[%s]"%(cols[-1])
        charges.append( qdict[ cols[-1] ] )      
        names.append( cols[-1] )

  fh=file("tmp.p2n","a")
  if use_atom_charge_constraints:
     for i in range(len(charges)):
         a = i+1
         name = names[i]
         charge = charges[i]
         if IsConstrained(name):
            fh.write("REMARK INTRA-MCC %7.4f | %4i | R\n"%(charge,a))
  else:
     netq = 0
     for i in range(len(charges)):
         name = names[i]
         charge = charges[i]
         if IsConstrained(name):
            netq += charge
     fh.write("REMARK INTRA-MCC %10.6f | "%(netq))
     for i in range(len(charges)):
         a = i+1
         name = names[i]
         if IsConstrained(name):
            fh.write("%4i "%(a))
     fh.write(" | K\n")

  for i in range(ncon):
     name = constraints[0+i*2]
     q    = float(constraints[1+i*2])
     for j in range(len(charges)):
        if name == names[j]:
           fh.write("REMARK INTRA-MCC %7.4f | %4i | K\n"%(q,j+1))

  fh.close()

EOF
chmod u+x ApplyConstraints.py



cat <<'EOF' > Resp2BashFcn.py
#!/usr/bin/env python2.7

from copy import deepcopy as deepcopy
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-a','--amber',
                    help='native mol2 w/amber charges',
                    required=True)
parser.add_argument('-n','--native',
                    help='native mol2 w/resp charges',
                    required=True)
parser.add_argument('-m','--modified',
                    help='modified mol2 w/resp charges',
                    required=True)
parser.add_argument('-c','--changes',
                    help='mol2 listing the atoms whose charges may differ from native amber',
                    required=False)
args = parser.parse_args()



def GetMol2Charges(fname):
    qdict={}
    for line in file(fname,"r"):
        cols=line.strip().split()
        if len(cols) == 10:
            qdict[ cols[1] ] = float( cols[-2] )
        elif len(cols) == 9:
            qdict[ cols[1] ] = float( cols[-1] )
        elif "BOND" in line:
            break
    return qdict



native_amber = GetMol2Charges(args.amber)
native_resp  = GetMol2Charges(args.native)
newmol_resp  = GetMol2Charges(args.modified)
if args.changes is not None:
    changes  = GetMol2Charges(args.changes)
else:
    changes = deepcopy(newmol_resp)
    for atom in native_resp:
        changes[atom] = native_resp[atom]

for atom in native_amber:
    if atom not in changes:
        if atom in newmol_resp:
            newmol_resp.pop(atom,None)
        if atom in native_resp:
            native_resp.pop(atom,None)
dq = 0.
for atom in native_resp:
    if atom not in newmol_resp:
        dq += native_amber[atom]-native_resp[atom]

import ApplyConstraints
newmol_nat = 0
for atom in newmol_resp:
    if not ApplyConstraints.IsConstrained(atom):
        newmol_nat += 1
dq = dq / newmol_nat

print "======================================"
print "Delta Q correction applied to each"
print "  variable-charge atom: %7.4f"%(dq)
for atom in newmol_resp:
    if not ApplyConstraints.IsConstrained(atom):
        newmol_resp[atom] += dq

newmol_amber = {}
for atom in newmol_resp:
    if atom in native_resp:
        newmol_amber[atom] = newmol_resp[atom]-native_resp[atom]+native_amber[atom]
    else:
        newmol_amber[atom] = newmol_resp[atom]

for atom in native_amber:
    if atom not in native_resp and atom not in newmol_amber:
        newmol_amber[atom] = native_amber[atom]

native_amber_net_charge = 0
for atom in native_amber:
    native_amber_net_charge += native_amber[atom]
newmol_amber_net_charge = 0
for atom in newmol_amber:
    newmol_amber_net_charge += newmol_amber[atom]

# renormalize the net charge to the nearest integer
if args.changes is not None:
    dq = round(newmol_amber_net_charge)-newmol_amber_net_charge
    dq = dq / len(newmol_resp)
    for atom in newmol_resp:
        newmol_amber[atom] += dq
        
    newmol_amber_net_charge = 0
    for atom in newmol_amber:
        newmol_amber_net_charge += newmol_amber[atom]


print "======================================"
print "  newQ = newResp - natResp + amberQ\n"
print " name    newQ newResp natResp  amberQ"
print "======================================"
for atom in sorted(set( [ x for x in newmol_amber ] + [ x for x in native_amber ] )):
    amber_charge = "%7s"%("")
    resp_charge  = "%7s"%("")
    new_charge   = "%7s"%("")
    charge = 0
    if atom in native_amber:
        amber_charge="%7.4f"%(native_amber[atom])
    if atom in native_resp:
        resp_charge="%7.4f"%(native_resp[atom])
    if atom in newmol_resp:
        new_charge="%7.4f"%(newmol_resp[atom])
    if atom in newmol_amber:
        charge = newmol_amber[atom]
    print "%-5s %7.4f %s %s %s"%( atom,charge,new_charge,resp_charge,amber_charge )
print "="*38
print "%-5s %7.4f %7s %7s %7.4f"%("netQ",newmol_amber_net_charge,"","",native_amber_net_charge)

print ""
print "function set_charge_FOO()"
print "{"
print "   selection=\"$1\""
print "   cat <<EOF"
for atom in sorted(newmol_resp):
   print "set ${selection}.%-5s charge %10.6f"%(atom,newmol_amber[atom])
for atom in sorted(native_amber):
   if atom not in newmol_resp:
      if atom in native_resp or native_amber[atom] == 0:
         print "set ${selection}.%-5s charge   0.0"%(atom)
print "EOF"
print "}"
print ""

EOF
chmod u+x Resp2BashFcn.py




function run_resp_fit()
{
    origpdb="$1"
    charge="$2"

    mol2=$(realpath ${origpdb%.pdb}.mol2)
    if [ ! -e "${mol2}" ]; then
	echo "run_resp_fit : file not found ${mol2}"
	exit 1
    fi
    
    pdb=$(basename ${origpdb})
    base=${pdb%.pdb}
    p2n=${base}-out.p2n
    com=${base}-gau.com
    chk=${base}-gau.chk

    if [ ! -d ${base} ]; then
	mkdir ${base}
    fi
    cp ${origpdb} ${base}
    
    cd ${base}

    perl ${REDHOME}/Ante_RED-1.5.pl ${pdb}
    rm *-gam.inp *-pcg.inp *info.txt
    
    sed -i "s|%Chk=.*|%CHK=${chk}|" ${com}
    sed -i "s|%Mem=.*|%MEM=8000MB|" ${com}
    sed -i "s|%NProc=.*|%NPROC=16|" ${com}
    sed -i "s| Freq||" ${com}
    sed -i 's|Opt=.Tight,CalcFC.| SP |' ${com}
     
    sed -i "s|^ 0 1| ${charge} 1|" ${com}
    sed -i "s|CHARGE-VALUE 0|CHARGE-VALUE ${charge}|" ${p2n}
    
    # remove the "END" tag
    head -n -1 ${p2n} > tmp.p2n
    
    # fill in charge constraints
    ../ApplyConstraints.py ${mol2} "${@:3}"
    
    # replace the END tag
    echo "END" >> tmp.p2n
    # restore filename
    mv tmp.p2n ${p2n}

    # run gaussian, if necessary
    if [ ! -e Mol_red1.log ]; then
	echo "Running gaussian calculation in ${base}"
	
	cp ${p2n} Mol_red1.p2n
	g09 < ${com} > Mol_red1.log
	echo "Stationary point found" | cat - Mol_red1.log > crap.log
	mv crap.log Mol_red1.log
    fi

    # run the fitting program
    if [ -d Data-RED ]; then
	rm -fr Data-RED
    fi
	
    echo "Running RED-vIII.5.pl in ${base}"
    export GAUSS_SCRDIR=${PWD}
    cp ${p2n} Mol_red1.p2n
    perl ${REDHOME}/RED-vIII.5.pl > Mol_red1-red.out
    
    cd ../
}



run_resp_fit GB.pdb 0
run_resp_fit APB.pdb 0
./Resp2BashFcn.py --amber GB.mol2 \
		  --native GB/Data-RED/Mol_m1-o1-sm.mol2 \
		  --modified APB/Data-RED/Mol_m1-o1-sm.mol2


rm -f ApplyConstraints.py
rm -f Resp2BashFcn.py

