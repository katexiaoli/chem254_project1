#!/usr/bin/env python2.7

import parmed
from numpy import linalg as LA
import numpy as np
import nlopt
import re
from collections import defaultdict as ddict
import parmutils
import subprocess
import sys
import argparse
from shutil import copyfile

def readfrcs( trajfile ):
    from scipy.io import netcdf
    import numpy as np
    nc = netcdf.NetCDFFile(trajfile,'r')
    fvar = nc.variables['forces'] # kcal/mol per amber-charge
    nframe,nat,xyzidx = fvar.shape
    data = np.zeros( (nframe,nat,xyzidx) )
    data[:,:,:] = fvar[:,:,:]
    nc.close()
    return (nframe,nat,xyzidx), data



def openparm( fname, xyz=None ):
    import parmed
    from parmed.constants import IFBOX
    if ".mol2" in fname:
        param = parmed.load_file( fname, structure=True )
        #help(param)
    else:
        param = parmed.load_file( fname,xyz=xyz )
        if xyz is not None:
            if ".rst7" in xyz:
                param.load_rst7(xyz)
    if param.box is not None:
        if abs(param.box[3]-109.471219)<1.e-4 and \
           abs(param.box[4]-109.471219)<1.e-4 and \
           abs(param.box[5]-109.471219)<1.e-4:
            param.parm_data["POINTERS"][IFBOX]=2
            param.pointers["IFBOX"]=2
    return param


def saveparm( param, fname ):
    from parmed.constants import IFBOX
    ifbox=0
    if param.box is not None:
       if abs(param.box[3]-109.471219)<1.e-4 and \
          abs(param.box[4]-109.471219)<1.e-4 and \
          abs(param.box[5]-109.471219)<1.e-4:
           param.parm_data["POINTERS"][IFBOX]=2
           param.pointers["IFBOX"]=2
       ifbox=param.pointers["IFBOX"]
#    try:
#        param.save( fname, overwrite=True )
#    except:
#        param.save( fname )
    p = parmutils.CopyParm( param )
    p.remake_parm()
    param.pointers["IFBOX"]=ifbox
    parmed.amber.AmberFormat.write_parm( p, fname )




def MakeUniqueLJParams( p, qmidxs ):
    mmtypes = ddict( list )
    qmtypes = ddict( list )
    for a in p.atoms:
        if a.idx in qmidxs:
            qmtypes[ a.nb_idx ].append( a.idx )
        else:
            mmtypes[ a.nb_idx ].append( a.idx )

    for nb_idx in qmtypes:
        if nb_idx in mmtypes:
            a = p.atoms[ qmtypes[nb_idx][0] ]
            rmin = p.LJ_radius[a.nb_idx-1]
            eps  = p.LJ_depth[a.nb_idx-1]
            sel  = "@" + ",".join( ["%i"%(i+1) for i in qmtypes[nb_idx] ] )
            parmed.tools.addLJType(p,sel).execute()
            p.LJ_radius[a.nb_idx-1] = rmin
            p.LJ_depth[a.nb_idx-1]  = eps
    p.recalculate_LJ()


    

def ExtractFrcmod(p,sele,fname,masses=True,bonds=True,angles=True,dihedrals=True,lj=True):
    from copy import copy,deepcopy
    from parmed.utils.six import add_metaclass, string_types, iteritems
    from parmed.topologyobjects import BondType,AngleType,DihedralType
    from collections import defaultdict as ddict
    import re
    
    q = parmutils.Extract(p,sele)
    satoms = [ a.idx for a in q.atoms ]
    namemap = ddict(str)
    for a in satoms:
        oldname = q.atoms[a].atom_type.name
        newname = oldname
        if not "MSK" in newname:
            newname += "MSK"
        namemap[newname] = oldname
        q.atoms[a].atom_type.name = newname
        q.atoms[a].type = newname
    
    self = parmed.amber.parameters.AmberParameterSet.from_structure(q)
    angfact=0.9999995714245039
    
    fh = file(fname,"w")

    fh.write("modified parameters")
    fh.write('\n')
    # Write the atom mass
    fh.write('MASS\n')
    if masses:
        for atom, typ in iteritems(self.atom_types):
            if atom in namemap:
                fh.write('%s%11.8f\n' % (namemap[atom].ljust(6), typ.mass))

    fh.write('\n')
        
    # Write the bonds
    fh.write('BOND\n')
    if bonds:
        cdone = set()
        for (a1, a2), typ in iteritems(self.bond_types):
            typ.k = float("%.8f"%(typ.k))
            delta = 0
            if a1 in namemap and a2 in namemap:
                qq = (namemap[a1],namemap[a2])
                if qq in cdone: continue
                qq = (namemap[a2],namemap[a1])
                if qq in cdone: continue
                cdone.add(qq)
            else:
                continue    
            fh.write('%s-%s   %19.14f  %11.8f\n' %
                     (namemap[a1].ljust(2), namemap[a2].ljust(2), typ.k, typ.req))
    fh.write('\n')

    # Write the angles
    fh.write('ANGLE\n')
    if angles:
        cdone = set()
        for (a1, a2, a3), typ in iteritems(self.angle_types):
            typ.k = float("%.8f"%(typ.k))
            delta = 0.
            if a1 in namemap and a2 in namemap and \
               a3 in namemap:
                qq = (namemap[a1],namemap[a2],namemap[a3])
                if qq in cdone: continue
                qq = (namemap[a3],namemap[a2],namemap[a1])
                if qq in cdone: continue
                cdone.add(qq)
            else:
                continue    
            fh.write('%s-%s-%s   %19.14f  %17.3f\n' %
                     (namemap[a1].ljust(2), namemap[a2].ljust(2), namemap[a3].ljust(2), typ.k+delta,
                      typ.theteq * angfact))
    fh.write('\n')

    # Write the dihedrals
    fh.write('DIHE\n')
    if dihedrals:
        cdone = set()
        for (a1, a2, a3, a4), typ in iteritems(self.dihedral_types):
            isnew = False
            if a1 in namemap and a2 in namemap and \
               a3 in namemap and a4 in namemap:
                qq = (namemap[a1],namemap[a2],namemap[a3],namemap[a4])
                if qq in cdone: continue
                qq = (namemap[a4],namemap[a3],namemap[a2],namemap[a1])
                if qq in cdone: continue
                cdone.add(qq)
                isnew = True
            else:
                continue

            if isinstance(typ, DihedralType) or len(typ) == 1:
                if not isinstance(typ, DihedralType):
                    typ = typ[0]
                    typ.phi_k = float("%.8f"%(typ.phi_k))
                if abs(typ.phase-180) < 0.0001:
                    fh.write('%s-%s-%s-%s %4i %20.14f %13.3f %5.1f    '
                             'SCEE=%s SCNB=%s\n' % (namemap[a1].ljust(2), namemap[a2].ljust(2),
                                                    namemap[a3].ljust(2), namemap[a4].ljust(2), 1, typ.phi_k, typ.phase * angfact,
                                                    typ.per, typ.scee, typ.scnb))
                else:
                    fh.write('%s-%s-%s-%s %4i %20.14f %13.8f %5.1f    '
                             'SCEE=%s SCNB=%s\n' % (namemap[a1].ljust(2), namemap[a2].ljust(2),
                                                    namemap[a3].ljust(2), namemap[a4].ljust(2), 1, typ.phi_k, typ.phase * angfact,
                                                    typ.per, typ.scee, typ.scnb))
            else:
                typ = sorted( typ, key=lambda x: x.per, reverse=False )
                for dtyp in typ[:-1]:
                    dtyp.phi_k = float("%.8f"%(dtyp.phi_k))
                    if abs(dtyp.phase-180) < 0.0001:
                        #print "%20.16f"%(180.0/dtyp.phase)
                        fh.write('%s-%s-%s-%s %4i %20.14f %13.3f %5.1f    '
                                 'SCEE=%s SCNB=%s\n'%(namemap[a1].ljust(2), namemap[a2].ljust(2),
                                                      namemap[a3].ljust(2), namemap[a4].ljust(2), 1, dtyp.phi_k,
                                                      dtyp.phase * angfact, -dtyp.per, dtyp.scee, dtyp.scnb))
                    else:
                        fh.write('%s-%s-%s-%s %4i %20.14f %13.8f %5.1f    '
                                 'SCEE=%s SCNB=%s\n'%(namemap[a1].ljust(2), namemap[a2].ljust(2),
                                                      namemap[a3].ljust(2), namemap[a4].ljust(2), 1, dtyp.phi_k,
                                                      dtyp.phase * angfact, -dtyp.per, dtyp.scee, dtyp.scnb))
                dtyp = typ[-1]
                dtyp.phi_k = float("%.8f"%(dtyp.phi_k))
                if abs(dtyp.phase-180) < 0.0001:
                    fh.write('%s-%s-%s-%s %4i %20.14f %13.3f %5.1f    '
                             'SCEE=%s SCNB=%s\n' % (namemap[a1].ljust(2), namemap[a2].ljust(2),
                                                    namemap[a3].ljust(2), namemap[a4].ljust(2), 1, dtyp.phi_k,
                                                    dtyp.phase * angfact, dtyp.per, dtyp.scee, dtyp.scnb))
                else:
                    fh.write('%s-%s-%s-%s %4i %20.14f %13.8f %5.1f    '
                             'SCEE=%s SCNB=%s\n' % (namemap[a1].ljust(2), namemap[a2].ljust(2),
                                                    namemap[a3].ljust(2), namemap[a4].ljust(2), 1, dtyp.phi_k,
                                                    dtyp.phase * angfact, dtyp.per, dtyp.scee, dtyp.scnb))
                    
    fh.write('\n')
    # Write the impropers
    fh.write('IMPROPER\n')
    if dihedrals:
        for (a1, a2, a3, a4), typ in iteritems(self.improper_periodic_types):
            # Make sure wild-cards come at the beginning
            if a2 == 'X':
                assert a4 == 'X', 'Malformed generic improper!'
                a1, a2, a3, a4 = a2, a4, a3, a1
            elif a4 == 'X':
                a1, a2, a3, a4 = a4, a1, a3, a2

            typ.phi_k = float("%.8f"%(typ.phi_k))
            if a1 in namemap and a2 in namemap and \
               a3 in namemap and a4 in namemap:

                if abs(typ.phase-180) < 0.0001:
                    fh.write('%s-%s-%s-%s %20.14f %13.3f %5.1f\n' %
                             (namemap[a1].ljust(2), namemap[a2].ljust(2), namemap[a3].ljust(2), namemap[a4].ljust(2),
                              typ.phi_k, typ.phase * angfact, typ.per))
                else:
                    fh.write('%s-%s-%s-%s %20.14f %13.8f %5.1f\n' %
                             (namemap[a1].ljust(2), namemap[a2].ljust(2), namemap[a3].ljust(2), namemap[a4].ljust(2),
                              typ.phi_k, typ.phase * angfact, typ.per))

                
    fh.write('\n')
    
    # Write the LJ terms
    fh.write('NONB\n')
    if lj:
        if True:
            for atom, typ in iteritems(self.atom_types):
                #typ.rmin = float("%.8f"%(typ.rmin))
                typ.epsilon = float("%.9f"%(typ.epsilon))
                if atom in namemap:
                    fh.write('%-3s  %12.8f %18.9f\n' %
                             (namemap[atom].ljust(2), typ.rmin, typ.epsilon))

    fh.write('\n')
    
    # Write the NBFIX terms
    if lj:
        if self.nbfix_types:
            fh.write('LJEDIT\n')
            for (a1, a2), (eps, rmin) in iteritems(self.nbfix_types):
                if a1 in namemap and a2 in namemap:
                    fh.write('%s %s %13.8f %13.8f %13.8f %13.8f\n' %
                             (namemap[a1].ljust(2), namemap[a2].ljust(2), eps, rmin/2,
                              eps, rmin/2))
    

class param(object):
    def __init__(self,value,lb,ub):
        self.v = value
        self.u = ub
        self.l = lb

class optimizer(object):

    pinf =  float('inf')
    minf = -float('inf')

    __callback_instance = False
    @classmethod 
    def Genesis(cls,x):
        cls.__callback_instance.SetParamValues(x)
        return cls.__callback_instance
    
    def __init__(self):
        self.ps = []
        self.BestValue = optimizer.pinf
        self.InequalityFcns = []
        self.InequalityTols = []
        self.EqualityFcns = []
        self.EqualityTols = []

    def PushParam(self,p):
        self.ps.append(p)
        optimizer.static_data = self

    def SetParamValues(self,x):
        if len(x) != len(self.ps):
            raise Exception("optimizer.SetParamValues values dont match number of parameters")
        for i in range(len(self.ps)):
            self.ps[i].v = x[i]

    def GetNumParams(self):
        return len(self.ps)

    def GetParamValues(self):
        return [ self.ps[i].v for i in range(len(self.ps)) ]

    def GetParamLowerBounds(self):
        return [ self.ps[i].l for i in range(len(self.ps)) ]
    
    def GetParamUpperBounds(self):
        return [ self.ps[i].u for i in range(len(self.ps)) ]
    
    def PushInequalityConstraint( self , fcn_lambda_xg , tol=0 ):
        self.InequalityFcns.append(fcn_lambda_xg)
        self.InequalityTols.append(tol)

    def PushEqualityConstraint( self , fcn_lambda_xg , tol=0 ):
        self.EqualityFcns.append(fcn_lambda_xg)
        self.EqualityTols.append(tol)

    @staticmethod
    def CptChisq(x,g): pass

    
    def optimize(self,nlopt_algo_code):
        optimizer.__callback_instance = self

        o = nlopt.opt(nlopt_algo_code, self.GetNumParams())

        pit = iter(self.ps)
        for p in pit:
            o.set_lower_bounds(self.GetParamLowerBounds())
            o.set_upper_bounds(self.GetParamUpperBounds())

        o.set_min_objective(self.CptChisq)

        for i in range(len(self.InequalityFcns)):
            o.add_inequality_constraint( self.InequalityFcns[i], self.InequalityTols[i] )

        for i in range(len(self.EqualityFcns)):
            o.add_equality_constraint( self.EqualityFcns[i], self.EqualityTols[i] )

        o.set_xtol_abs(1e-6)
        o.set_ftol_rel(1e-8)
        o.set_maxeval(250)

        x = self.GetParamValues()
        xout = o.optimize(x)
        self.SetParamValues( xout )
        self.BestValue = o.last_optimum_value()

        return self.BestValue

# ###############################################################

class MyProblem(optimizer):

    def __init__(self,launch,ljk,onlyA):
        optimizer.__init__(self)

        self.launch=launch
        self.best_chisq = 1.e+10
        self.onlyA = onlyA
        
        parmobj = openparm( "ljopt/QMNtp.parm7", "ljopt/QM.rst7" )
        self.refparm = parmutils.CopyParm( parmobj )

        
        self.nbidxs = []
        selatoms = []
        if 'OPT_LJ_ATOMSEL' in parmobj.parm_data:
            selatoms = [ i-1 for i in parmobj.parm_data['OPT_LJ_ATOMSEL'] ]
        else:
            raise Exception("ljopt/QMNtp.parm7 does not contain OPT_LJ_ATOMSEL section")
        if len(selatoms) == 0:
            raise Exception("ljopt/QMNtp.parm7 does not have any atoms to optimize")
        MakeUniqueLJParams( parmobj, selatoms )
        for i in selatoms:
            self.nbidxs.append( parmobj.atoms[i].nb_idx - 1 )

        self.optsel = "@" + ",".join( [ "%i"%(i+1) for i in selatoms ] )
        #print self.nbidxs
        print "optimization selection: ",self.optsel 
        

        qmqm = openparm( "ljopt/QMQMtpo.parm7", "ljopt/QMQM.rst7" )
        if 'TOMMMAP' in qmqm.parm_data:
            self.tommmap = qmqm.parm_data['TOMMMAP']
        else:
            raise Exception("ljopt/QMQMtpo.parm7 does not contain TOMMMAP section")


        
        if True:

            scale=ljk
            
            s2r = pow(2.,1./6.)
            r2s = 1. / s2r

            self.nbidxs = list(set(self.nbidxs))
            self.nbidxs.sort()
            saved = []
            for idx in self.nbidxs:
                r = parmobj.LJ_radius[idx]
                e = parmobj.LJ_depth[idx]
                s = r * r2s
                A = 4.*e*pow(s,12)
                B = 4.*e*pow(s,6)
                if abs(A) > 0:
                    saved.append(idx)
                    if onlyA:
                        self.PushParam( param( A, A*(1-scale), A*(1+scale) ) )
                    else:
                        self.PushParam( param( r, r*(1-scale), r*(1+scale) ) )
                        self.PushParam( param( e, e*(1-scale), e*(1+scale) ) )
            self.nbidxs = saved
            #print "Optimizing idx ",saved

            
            qmqmshape,Ftpo = readfrcs( "ljopt/QMQMtpo.frc.nc" )
            qmqmshape,Fto  = readfrcs( "ljopt/QMQMto.frc.nc" )
            qmqmshape,Fpo  = readfrcs( "ljopt/QMQMpo.frc.nc" )
            qmqmshape,Fo   = readfrcs( "ljopt/QMQMo.frc.nc" )
            self.reffrc = Ftpo-Fto-Fpo+Fo
            qmshape,Ftpo = readfrcs( "ljopt/QMEtpo.frc.nc" )
            qmshape,Fto  = readfrcs( "ljopt/QMEto.frc.nc" )
            qmshape,Fpo  = readfrcs( "ljopt/QMEpo.frc.nc" )
            qmshape,Fo   = readfrcs( "ljopt/QMEo.frc.nc" )
            for f in range(qmqmshape[0]):
                for i in range(qmqmshape[1]):
                    j = self.tommmap[i]
                    self.reffrc[f,i,:] -= (Ftpo[f,j,:]-Fto[f,j,:]-Fpo[f,j,:]+Fo[f,j,:])
            Ftpo=[]
            Fto=[]
            Fpo=[]
            Fo=[]
            qmshape,Ft = readfrcs( "ljopt/QMNt.frc.nc" )
            qmshape,Fp = readfrcs( "ljopt/QMNp.frc.nc" )
            for f in range(qmqmshape[0]):
                for i in range(qmqmshape[1]):
                    j = self.tommmap[i]
                    self.reffrc[f,i,:] -= (-Ft[f,j,:]-Fp[f,j,:])
            Ft=[]
            Fp=[]
            self.qmqmshape = qmqmshape
            

        self.iter = 0
        return

    

    def SetParm7(self,parm):
        ii = 0
        if True:
            s2r = pow(2.,1./6.)
            r2s = 1. / s2r
            for idx in self.nbidxs:
                r = self.refparm.LJ_radius[idx]
                e = self.refparm.LJ_depth[idx]
                s = r * r2s
                A = 4.*e*pow(s,12)
                B = 4.*e*pow(s,6)
                if self.onlyA:
                    A = self.ps[ii].v
                    s = pow( A/B, 1./6. )
                    e = B*B/(4.*A)
                    r = s*s2r
                    self.refparm.LJ_radius[idx] = r
                    self.refparm.LJ_depth[idx]  = e
                    ii += 1
                else:
                    self.refparm.LJ_radius[idx] = self.ps[ii].v
                    ii += 1
                    self.refparm.LJ_depth[idx] = self.ps[ii].v
                    ii += 1
            self.refparm.recalculate_LJ()
            
                
    def RunChisq(self):
        self.iter += 1
        self.SetParm7(self.refparm)
        saveparm( self.refparm, "ljopt/QMNtp.trial.parm7" )

        chisq = 0.

        subprocess.call("%s sander.MPI -O -p ljopt/QMNtp.trial.parm7 -i ljopt/QMNtp.mdin -o ljopt/QMNtp.mdout -c ljopt/QM.rst7 -r ljopt/QMNtp.rst7 -inf ljopt/QMNtp.mdinfo -x ljopt/QMNtp.nc -frc ljopt/QMNtp.frc.nc -y ljopt/QM.nc"%(self.launch),shell=True)

        
        qmshape,Ftp = readfrcs( "ljopt/QMNtp.frc.nc" )
        nf = self.qmqmshape[0]
        nat = self.qmqmshape[1]
        errs=ddict(float)
        for f in xrange(nf):
            for i in xrange(nat):
                j = self.tommmap[i]
                ref = self.reffrc[f,i,:]
                mdl = Ftp[f,j,:]
                err = np.sum( (ref[:]-mdl[:])**2 )
                errs[j] += err / nf
                chisq += err / (nf*nat)
        
        for i,p in enumerate(self.ps):
            print "%3i %14.8f %14.8f %14.8f"%( i+1, p.v, p.l, p.u )
        print ""

        ats=[]
        ers=[]
        for w in sorted(errs,key=errs.get, reverse=True):
              ats.append(w)
              ers.append(errs[w])

        print "Atoms with largest errors"
        print "serial"," ".join( ["%i"%(ats[i]+1) for i in range(min(len(ats),6)) ] )
        print "errors"," ".join( ["%10.4f"%(ers[i]) for i in range(min(len(ats),6)) ] )
        print ""

        best=0
        if chisq < self.best_chisq:
              self.best_chisq = chisq
              best=1
              copyfile("ljopt/QMNtp.trial.parm7","ljopt/QMNtp.best.parm7")
              ExtractFrcmod(self.refparm,self.optsel,"ljopt.frcmod",masses=True,bonds=False,angles=False,dihedrals=False,lj=True)

        print "chisq %5i %19.8f #%i\n"%(self.iter,chisq, best)
        sys.stdout.flush()
        return chisq

    
    @staticmethod
    def CptChisq(x,g):
        if len(g) > 0:
            raise Exception("Can't optimize with gradients")
        
        # Get the parameters for my model
        self = optimizer.Genesis(x)
        return self.RunChisq()

        
    
# ###############################################################

parser = argparse.ArgumentParser \
         ( formatter_class=argparse.RawDescriptionHelpFormatter,
           description="""Parametrize QM LJ parameters to QM/QM forces\n""" )

parser.add_argument \
    ("-O","--onlyA",
     help="if present, then only optimize the A parameter, not rmin and eps",
     action='store_true',
     default=False,
     required=False )

parser.add_argument \
    ("-L","--ljk",
     help="Lennard-Jones scale percentage",
     type=float,
     default=0.3,
     required=False )

parser.add_argument \
    ("-M","--launch",
     help="mpi launch command",
     type=str,
     default="srun --mpi=pmi2 -K1",
     required=False )

args = parser.parse_args()



o = MyProblem( args.launch, args.ljk, args.onlyA )

minf = 0.
x = []
minf = o.optimize( nlopt.LN_COBYLA )
x = o.GetParamValues()
o.CptChisq(x,[])


    
print "=================== RESULTS ===================\n"
print "optimum at "
for i,p in enumerate(o.ps):
    print "%3i %14.8f %14.8f %14.8f"%( i+1, x[i], p.l, p.u )
print "minimum value = ", o.BestValue
print "\n"






