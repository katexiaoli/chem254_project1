#!/usr/bin/env python2.7

import parmed
#import pytraj
from collections import defaultdict as ddict
import os
import subprocess
import shutil
import copy
from os.path import join as pathjoin

from scipy.io import netcdf
import numpy


def readfrcs( trajfile ):
    from scipy.io import netcdf
    import numpy
    nc = netcdf.NetCDFFile(trajfile,'r')
    fvar = nc.variables['forces'] # kcal/mol per amber-charge
    nframe,nat,xyzidx = fvar.shape
    data = numpy.zeros( (nframe,nat,xyzidx) )
    data[:,:,:] = fvar[:,:,:]
    nc.close()
    return (nframe,nat,xyzidx), data


def readcrds( trajfile ):
    from scipy.io import netcdf
    import numpy
    nc = netcdf.NetCDFFile(trajfile,'r')
    fvar = nc.variables['coordinates'] # angstrom
    nframe,nat,xyzidx = fvar.shape
    data = numpy.zeros( (nframe,nat,xyzidx) )
    data[:,:,:] = fvar[:,:,:]
    nc.close()
    return (nframe,nat,xyzidx), data





def OpenParm( fname, xyz=None ):
    import parmed
    from parmed.constants import IFBOX
    if ".mol2" in fname:
        param = parmed.load_file( fname, structure=True )
        #help(param)
    else:
        param = parmed.load_file( fname,xyz=xyz )
        if xyz is not None:
            if ".rst7" in xyz:
                param.load_rst7(xyz)
    if param.box is not None:
        if abs(param.box[3]-109.471219)<1.e-4 and \
           abs(param.box[4]-109.471219)<1.e-4 and \
           abs(param.box[5]-109.471219)<1.e-4:
            param.parm_data["POINTERS"][IFBOX]=2
            param.pointers["IFBOX"]=2
    return param


def SaveParm( param, fname ):
    from parmed.constants import IFBOX
    if param.box is not None:
       if abs(param.box[3]-109.471219)<1.e-4 and \
          abs(param.box[4]-109.471219)<1.e-4 and \
          abs(param.box[5]-109.471219)<1.e-4:
           param.parm_data["POINTERS"][IFBOX]=2
           param.pointers["IFBOX"]=2
    try:
        param.save( fname, overwrite=True )
    except:
        param.save( fname )


def SaveCrds( param, fname ):
    parmed.tools.writeCoordinates( param, fname ).execute()

def CopyParm( parm ):
    import copy
    try:
        parm.remake_parm()
    except:
        pass
    p = copy.copy( parm )
    p.coordinates = copy.copy( parm.coordinates )
    p.box = copy.copy( parm.box )
    try:
        p.hasbox = copy.copy( parm.hasbox )
    except:
        p.hasbox = False
    return p


def Strip( parm, mask ):
    p = CopyParm( parm )
    p.strip( "%s"%(mask) )
    return p


def Extract( parm, mask ):
    return Strip( parm, "!(%s)"%(mask) )

def GetSelectedAtomIndices(param,maskstr):
    #param = parmed.load_file(parmfile)
    #mask = parmed.amber.mask.AmberMask( param, maskstr )
    #aidxs = mask.Selected()
    #for aidx in aidxs:
    #    atom = param.atoms[aidx]
    #    res  = atom.residue
    sele = []
    if len(maskstr) > 0:
        newmaskstr = maskstr.replace("@0","!@*")
        sele = [ param.atoms[i].idx for i in parmed.amber.mask.AmberMask( param, newmaskstr ).Selected() ]
    return sele

def GetSelectedResidueIndices(param,maskstr):
    a = GetSelectedAtomIndices(param,maskstr)
    b = list(set([ param.atoms[c].residue.idx for c in a ]))
    b.sort()
    return b


def ListToSelection(atomlist):
    alist = list(sorted(set(atomlist)))
    rs=[]
    if len(alist) > 0:
        rs = [ (alist[0],alist[0]) ]
        for a in alist[1:]:
            if a == rs[-1][1]+1:
                rs[-1] = ( rs[-1][0], a )
            else:
                rs.append( (a,a) )
    sarr = []
    for r in rs:
        if r[0] != r[1]:
            sarr.append( "%i-%i"%(r[0]+1,r[1]+1) )
        else:
            sarr.append( "%i"%(r[0]+1) )
    sele = ""
    if len(sarr) > 0:
        sele = ",".join(sarr)
    return sele

def AtomListToSelection(p,atomlist):
    reslist = ddict( list )
    haslist = ddict( list )
    atomlist = list(set(atomlist))
    for iat in atomlist:
        ires = p.atoms[iat].residue.idx
        if not ires in reslist:
            reslist[ires] = [ a.idx for a in p.residues[ires].atoms ]
            haslist[ires] = []
        reslist[ires].remove(iat)
        haslist[ires].append(iat)

    ress = []
    atms = []
    for ires in reslist:
        if len(reslist[ires]) == 0:
            ress.append(ires)
        else:
            for iatm in haslist[ires]:
                atms.append(iatm)
    rsel = ListToSelection(ress)
    asel = ListToSelection(atms)
    sel = ""
    if len(rsel) > 0:
        sel = ":"+rsel
        if len(asel) > 0:
            sel += "|"
    if len(asel) > 0:
        sel += "@"+asel
    return sel


def MakeUniqueLJParams( p, sele ):
    mmtypes = ddict( list )
    qmtypes = ddict( list )
    qmidxs = GetSelectedAtomIndices(p,sele)
    for a in p.atoms:
        if a.idx in qmidxs:
            qmtypes[ a.nb_idx ].append( a.idx )
        else:
            mmtypes[ a.nb_idx ].append( a.idx )

    for nb_idx in qmtypes:
        if nb_idx in mmtypes:
            a = p.atoms[ qmtypes[nb_idx][0] ]
            rmin = p.LJ_radius[a.nb_idx-1]
            eps  = p.LJ_depth[a.nb_idx-1]
            sel  = "@" + ",".join( ["%i"%(i+1) for i in qmtypes[nb_idx] ] )
            parmed.tools.addLJType(p,sel).execute()
            p.LJ_radius[a.nb_idx-1] = rmin
            p.LJ_depth[a.nb_idx-1]  = eps
    p.recalculate_LJ()


def MakeUniqueBondParams( p, xlist, k=None, req=None ):
    from collections import defaultdict as ddict
    byidx = ddict( list )
    for x in xlist:
        byidx[ x.type.idx ].append( x )
    for idx in byidx:
        x = byidx[idx][0].type
        
        myk=k
        if k is None:
            myk=x.k
        myreq=req
        if req is None:
            myreq=x.req

        p.bond_types.append( parmed.BondType( myk, myreq, p.bond_types ) )
        p.bond_types[-1].list = p.bond_types
        
        for x in byidx[idx]:
            x.type = p.bond_types[-1]
    p.bonds.changed=True
            

def MakeUniqueAngleParams( p, xlist, k=None, theteq=None ):
    from collections import defaultdict as ddict
    byidx = ddict( list )
    for x in xlist:
        byidx[ x.type.idx ].append( x )
    for idx in byidx:
        x = byidx[idx][0].type

        myk=k
        if k is None:
            myk=x.k  
        mytheteq=theteq
        if theteq is None:
            mytheteq=x.theteq
    
        p.angle_types.append( parmed.AngleType( myk, mytheteq, p.angle_types ) )
        for x in byidx[idx]:
            x.type = p.angle_types[-1]
    p.angles.changed=True
     

def MakeUniqueDihedralParams( p, xlist, phi_k=None ):
    from collections import defaultdict as ddict
    byidx = ddict( list )
    for x in xlist:
        byidx[ x.type.idx ].append( x )
    for idx in byidx:
        x = byidx[idx][0].type

        myphi_k=phi_k
        if phi_k is None:
            myphi_k=x.phi_k
        
        p.dihedral_types.append( parmed.DihedralType( myphi_k, x.per, x.phase, x.scee, x.scnb, p.dihedral_types ) )

        for x in byidx[idx]:
            #p.dihedrals[ x.idx ].type = p.dihedral_types[-1]
            x.type = p.dihedral_types[-1]
    p.dihedrals.changed=True

    

def GetBondsWithinSelection(p,sele):
    if isinstance(sele,list):
        satoms = sele
    else:
        satoms = GetSelectedAtomIndices(p,sele)
    xlist=[]
    for x in p.bonds:
        if x.atom1.idx in satoms and x.atom2.idx in satoms:
            xlist.append(x)
    return xlist

def GetAnglesWithinSelection(p,sele):
    if isinstance(sele,list):
        satoms = sele
    else:
        satoms = GetSelectedAtomIndices(p,sele)
    xlist=[]
    for x in p.angles:
        if x.atom1.idx in satoms and x.atom2.idx in satoms and x.atom3.idx in satoms:
            xlist.append(x)
    return xlist

def GetDihedralsWithinSelection(p,sele):
    if isinstance(sele,list):
        satoms = sele
    else:
        satoms = GetSelectedAtomIndices(p,sele)
    xlist=[]
    for x in p.dihedrals:
        if x.atom1.idx in satoms and x.atom2.idx in satoms and x.atom3.idx in satoms and x.atom4.idx in satoms:
            xlist.append(x)
    return xlist



def ExtractFrcmod(p,sele,fname,masses=True,bonds=True,angles=True,dihedrals=True,lj=True):
    from copy import copy,deepcopy
    from parmed.utils.six import add_metaclass, string_types, iteritems
    from parmed.topologyobjects import BondType,AngleType,DihedralType
    from collections import defaultdict as ddict
    import re
    
    q = Extract(p,sele)
    satoms = [ a.idx for a in q.atoms ]
    namemap = ddict(str)
    for a in satoms:
        oldname = q.atoms[a].atom_type.name
        newname = oldname
        if not "MSK" in newname:
            newname += "MSK"
        namemap[newname] = oldname
        q.atoms[a].atom_type.name = newname
        q.atoms[a].type = newname
    
    self = parmed.amber.parameters.AmberParameterSet.from_structure(q)
    angfact=0.9999995714245039
    
    fh = file(fname,"w")

    fh.write("modified parameters")
    fh.write('\n')
    # Write the atom mass
    fh.write('MASS\n')
    if masses:
        for atom, typ in iteritems(self.atom_types):
            if atom in namemap:
                fh.write('%s%11.8f\n' % (namemap[atom].ljust(6), typ.mass))

    fh.write('\n')
        
    # Write the bonds
    fh.write('BOND\n')
    if bonds:
        cdone = set()
        for (a1, a2), typ in iteritems(self.bond_types):
            typ.k = float("%.8f"%(typ.k))
            delta = 0
            if a1 in namemap and a2 in namemap:
                qq = (namemap[a1],namemap[a2])
                if qq in cdone: continue
                qq = (namemap[a2],namemap[a1])
                if qq in cdone: continue
                cdone.add(qq)
            else:
                continue    
            fh.write('%s-%s   %19.14f  %11.8f\n' %
                     (namemap[a1].ljust(2), namemap[a2].ljust(2), typ.k, typ.req))
    fh.write('\n')

    # Write the angles
    fh.write('ANGLE\n')
    if angles:
        cdone = set()
        for (a1, a2, a3), typ in iteritems(self.angle_types):
            typ.k = float("%.8f"%(typ.k))
            delta = 0.
            if a1 in namemap and a2 in namemap and \
               a3 in namemap:
                qq = (namemap[a1],namemap[a2],namemap[a3])
                if qq in cdone: continue
                qq = (namemap[a3],namemap[a2],namemap[a1])
                if qq in cdone: continue
                cdone.add(qq)
            else:
                continue    
            fh.write('%s-%s-%s   %19.14f  %17.3f\n' %
                     (namemap[a1].ljust(2), namemap[a2].ljust(2), namemap[a3].ljust(2), typ.k+delta,
                      typ.theteq * angfact))
    fh.write('\n')

    # Write the dihedrals
    fh.write('DIHE\n')
    if dihedrals:
        cdone = set()
        for (a1, a2, a3, a4), typ in iteritems(self.dihedral_types):
            isnew = False
            if a1 in namemap and a2 in namemap and \
               a3 in namemap and a4 in namemap:
                qq = (namemap[a1],namemap[a2],namemap[a3],namemap[a4])
                if qq in cdone: continue
                qq = (namemap[a4],namemap[a3],namemap[a2],namemap[a1])
                if qq in cdone: continue
                cdone.add(qq)
                isnew = True
            else:
                continue

            if isinstance(typ, DihedralType) or len(typ) == 1:
                if not isinstance(typ, DihedralType):
                    typ = typ[0]
                    typ.phi_k = float("%.8f"%(typ.phi_k))
                if abs(typ.phase-180) < 0.0001:
                    fh.write('%s-%s-%s-%s %4i %20.14f %13.3f %5.1f    '
                             'SCEE=%s SCNB=%s\n' % (namemap[a1].ljust(2), namemap[a2].ljust(2),
                                                    namemap[a3].ljust(2), namemap[a4].ljust(2), 1, typ.phi_k, typ.phase * angfact,
                                                    typ.per, typ.scee, typ.scnb))
                else:
                    fh.write('%s-%s-%s-%s %4i %20.14f %13.8f %5.1f    '
                             'SCEE=%s SCNB=%s\n' % (namemap[a1].ljust(2), namemap[a2].ljust(2),
                                                    namemap[a3].ljust(2), namemap[a4].ljust(2), 1, typ.phi_k, typ.phase * angfact,
                                                    typ.per, typ.scee, typ.scnb))
            else:
                typ = sorted( typ, key=lambda x: x.per, reverse=False )
                for dtyp in typ[:-1]:
                    dtyp.phi_k = float("%.8f"%(dtyp.phi_k))
                    if abs(dtyp.phase-180) < 0.0001:
                        #print "%20.16f"%(180.0/dtyp.phase)
                        fh.write('%s-%s-%s-%s %4i %20.14f %13.3f %5.1f    '
                                 'SCEE=%s SCNB=%s\n'%(namemap[a1].ljust(2), namemap[a2].ljust(2),
                                                      namemap[a3].ljust(2), namemap[a4].ljust(2), 1, dtyp.phi_k,
                                                      dtyp.phase * angfact, -dtyp.per, dtyp.scee, dtyp.scnb))
                    else:
                        fh.write('%s-%s-%s-%s %4i %20.14f %13.8f %5.1f    '
                                 'SCEE=%s SCNB=%s\n'%(namemap[a1].ljust(2), namemap[a2].ljust(2),
                                                      namemap[a3].ljust(2), namemap[a4].ljust(2), 1, dtyp.phi_k,
                                                      dtyp.phase * angfact, -dtyp.per, dtyp.scee, dtyp.scnb))
                dtyp = typ[-1]
                dtyp.phi_k = float("%.8f"%(dtyp.phi_k))
                if abs(dtyp.phase-180) < 0.0001:
                    fh.write('%s-%s-%s-%s %4i %20.14f %13.3f %5.1f    '
                             'SCEE=%s SCNB=%s\n' % (namemap[a1].ljust(2), namemap[a2].ljust(2),
                                                    namemap[a3].ljust(2), namemap[a4].ljust(2), 1, dtyp.phi_k,
                                                    dtyp.phase * angfact, dtyp.per, dtyp.scee, dtyp.scnb))
                else:
                    fh.write('%s-%s-%s-%s %4i %20.14f %13.8f %5.1f    '
                             'SCEE=%s SCNB=%s\n' % (namemap[a1].ljust(2), namemap[a2].ljust(2),
                                                    namemap[a3].ljust(2), namemap[a4].ljust(2), 1, dtyp.phi_k,
                                                    dtyp.phase * angfact, dtyp.per, dtyp.scee, dtyp.scnb))
                    
    fh.write('\n')
    # Write the impropers
    fh.write('IMPROPER\n')
    if dihedrals:
        for (a1, a2, a3, a4), typ in iteritems(self.improper_periodic_types):
            # Make sure wild-cards come at the beginning
            if a2 == 'X':
                assert a4 == 'X', 'Malformed generic improper!'
                a1, a2, a3, a4 = a2, a4, a3, a1
            elif a4 == 'X':
                a1, a2, a3, a4 = a4, a1, a3, a2

            typ.phi_k = float("%.8f"%(typ.phi_k))
            if a1 in namemap and a2 in namemap and \
               a3 in namemap and a4 in namemap:

                if abs(typ.phase-180) < 0.0001:
                    fh.write('%s-%s-%s-%s %20.14f %13.3f %5.1f\n' %
                             (namemap[a1].ljust(2), namemap[a2].ljust(2), namemap[a3].ljust(2), namemap[a4].ljust(2),
                              typ.phi_k, typ.phase * angfact, typ.per))
                else:
                    fh.write('%s-%s-%s-%s %20.14f %13.8f %5.1f\n' %
                             (namemap[a1].ljust(2), namemap[a2].ljust(2), namemap[a3].ljust(2), namemap[a4].ljust(2),
                              typ.phi_k, typ.phase * angfact, typ.per))

                
    fh.write('\n')
    
    # Write the LJ terms
    fh.write('NONB\n')
    if lj:
        if True:
            for atom, typ in iteritems(self.atom_types):
                #typ.rmin = float("%.8f"%(typ.rmin))
                typ.epsilon = float("%.9f"%(typ.epsilon))
                if atom in namemap:
                    fh.write('%-3s  %12.8f %18.9f\n' %
                             (namemap[atom].ljust(2), typ.rmin, typ.epsilon))

    fh.write('\n')
    
    # Write the NBFIX terms
    if lj:
        if self.nbfix_types:
            fh.write('LJEDIT\n')
            for (a1, a2), (eps, rmin) in iteritems(self.nbfix_types):
                if a1 in namemap and a2 in namemap:
                    fh.write('%s %s %13.8f %13.8f %13.8f %13.8f\n' %
                             (namemap[a1].ljust(2), namemap[a2].ljust(2), eps, rmin/2,
                              eps, rmin/2))
    
    

class MM2QMMM(object):
    
    def __init__(self,inp_parmfile,inp_rstfile):
        self.qmmask=None
        self.qmcharge=None
        self.inp_parmfile = inp_parmfile
        self.inp_rstfile = inp_rstfile
        self.mmparm = OpenParm(self.inp_parmfile,xyz=self.inp_rstfile)
        self.qmparm = None


    def save_qmparm(self,fname,xyz=None):
        path,name = os.path.split(fname)
        if len(path) > 0:
            if not os.path.isdir(path):
                os.makedirs(path)
        SaveParm(self.qmparm,fname)
        if xyz is not None:
            SaveCrds(self.qmparm,xyz)
        
    def save_mmparm(self,fname,xyz=None):
        path,name = os.path.split(fname)
        if len(path) > 0:
            if not os.path.isdir(path):
                os.makedirs(path)
        SaveParm(self.mmparm,fname)
        if xyz is not None:
            SaveCrds(self.mmparm,xyz)

        
    def set_qm(self,qmmask,qmcharge,adjustq=True):

        atomsel = GetSelectedAtomIndices(self.mmparm,qmmask)
        atomsel.sort()
        qmmask = AtomListToSelection(self.mmparm,atomsel)

        # Find dummy atoms
        
        atoms_to_delete = []
        ew_atoms = []
        for i in atomsel:
            a = self.mmparm.atoms[i]
            if abs(a.charge) < 1.e-5 and abs(a.epsilon) < 1.e-5:
                atoms_to_delete.append(i)
            if abs(a.epsilon) < 1.e-5 and a.name == "EPW" and len(a.residue.atoms) == 4:
                if adjustq:
                    atoms_to_delete.append(i)
                    ew_atoms.append(i)

        # Before deleting atoms, move the EW charge onto oxygen

        for i in ew_atoms:
            res = self.mmparm.atoms[i].residue
            if res.atoms[0].name == "O":
                res.name = "qwt"
                res.atoms[0].charge += self.mmparm.atoms[i].charge
                self.mmparm.atoms[i].charge = 0
            else:
                raise Exception("Found EPW particle (%i) but no oxygen"%(i+1))

                
        self.atoms_to_delete = [ i for i in reversed(sorted(atoms_to_delete)) ]

        
        # for each atom in qmparm, track the index of the equivalent atom in mmparm
        self.o2i = [ i for i in range(len(self.mmparm.atoms)) ]
        # for each atom in mmparm, track the index of the equivalent atom in qmparm
        # (or leave a value of None if it is missing)
        self.i2o = [ None for i in range(len(self.mmparm.atoms)) ]
        
        for d in self.atoms_to_delete:
            del self.o2i[d]

        for o in range(len(self.o2i)):
            i = self.o2i[o]
            self.i2o[i] = o
        
        # Remove the deleted atoms from the QM selection
        
        atomsel = [ i for i in atomsel if i not in self.atoms_to_delete ]
        
        # Change the indices of the QM selection
        
        for d in self.atoms_to_delete:
            for i in range(len(atomsel)):
                if atomsel[i] > d:
                    atomsel[i] -= 1

                    
        # Strip the atoms from the parm
        
        if len(self.atoms_to_delete) > 0:
            mask = "@" + ",".join( [ "%i"%(i+1) for i in self.atoms_to_delete] )
            print "The following dummy and/or EW atoms are being deleted: %s\n"%(mask)
            self.qmparm = Strip( self.mmparm, mask )
        else:
            self.qmparm = CopyParm( self.mmparm )

        #self.qmmask = "@" + ",".join( ["%i"%(i+1) for i in atomsel ] )
        self.qmmask = AtomListToSelection(self.qmparm,atomsel)
        self.qmcharge = qmcharge


        tmp_data = []
        for i in range(len(self.qmparm.atoms)):
            a = self.o2i[i]
            tmp_data.append(a)
        if 'TOMMMAP' in self.qmparm.parm_data:
            self.qmparm.parm_data['TOMMMAP'] = tmp_data
        else:
            self.qmparm.add_flag('TOMMMAP','10I8',data=tmp_data)

        
        # Give each QM residue a new name because we will be
        # changing the charges. When a .lib file is generated,
        # it lists only the residues with unique names.
        # Therefore, we need to generate a unique name for any
        # residue we twiddle with.
        
        atomsel = GetSelectedAtomIndices(self.qmparm,self.qmmask)
        ressel = list(set([ self.qmparm.atoms[i].residue.idx for i in atomsel]))

        seenres = {}
        for res in self.qmparm.residues:
            seenres[res.name] = 1
        
        qmres=0
        for i in ressel:
            res = self.qmparm.residues[i]
            if res.name != "qwt":
                while True:
                    qmres += 1
                    name = "Q%02i"%(qmres)
                    if name not in seenres:
                        seenres[name] = 1
                        res.name = name
                        break

        # Adjust the MM charges of the QM atoms to ensure that
        # the MM representation of the QM region has integer charge.
        # If the QM region touches more than 1 residue, then
        # make sure each residue preserves its original charge
        # sum, but also have each QM portion of each residue
        # have integer charge

        num_modified_mm_residues = 0
        modified_mm_residue_names = []
        
        for ires in ressel:
            res = self.qmparm.residues[ires]


            qmq = 0
            mmq = 0
            for a in res.atoms:
                if a.idx in atomsel:
                    qmq += a.charge
                else:
                    mmq += a.charge
            totq = qmq + mmq

            print "BEFORE charge adjustments:"
            print "The net charge of     res %5i [%3s] is %9.5f"%(res.idx+1,res.name,totq)
            print "The  QM charge within res %5i [%3s] is %9.5f"%(res.idx+1,res.name,qmq)
            print "The  MM charge within res %5i [%3s] is %9.5f"%(res.idx+1,res.name,mmq)
            


            
            qma=[]
            for a in res.atoms:
                if a.idx in atomsel:
                    qma.append(a.idx)
            # find connection atoms
            for iat in qma:
                a = self.qmparm.atoms[iat]
                for p in a.bond_partners:
                    if p.residue is None:
                        # We deleted this atom, but it still shows up in bond_partners
                        continue
                    
                    if not p.idx in atomsel:
                        # p is a mm connection atom
                        # which atoms in this residue can we change?
                        changeable = []
                        cbonded = []
                        cres = self.qmparm.atoms[p.idx].residue
                        for c in cres.atoms:
                            if c.idx != p.idx and abs(c.charge) > 1.e-5:
                                changeable.append( c.idx )
                        for d in p.bond_partners:
                            if d.residue is None:
                                continue
                            if d.idx in changeable:
                                cbonded.append(d.idx)

                        if len(cbonded) > 1:
                            changeable = cbonded
                        nc = len(changeable)

                        if not adjustq:
                            nc = 0
                        
                        if nc > 0:
                            if cres.idx not in ressel:
                                if not cres.name in modified_mm_residue_names:
                                    num_modified_mm_residues += 1
                                    cres.name = "M%02i"%(num_modified_mm_residues)
                                    modified_mm_residue_names.append(cres.name)
                            dq = p.charge / nc
                            p.charge = 0
                            for ic in changeable:
                                self.qmparm.atoms[ic].charge += dq

            mmq = 0
            qmq = 0
            mma = []
            for a in res.atoms:
                if a.idx in atomsel:
                    qmq += a.charge
                else:
                    mmq += a.charge
                    if abs(a.charge) > 1.e-5:
                        mma.append(a.idx)


            
            iqmq = round(qmq)
            dqmq = (iqmq - qmq)
            if adjustq:
                for iat in qma:
                    self.qmparm.atoms[iat].charge += dqmq / len(qma)
                for iat in mma:
                    self.qmparm.atoms[iat].charge -= dqmq / len(mma)

            qmq = 0
            mmq = 0
            for a in res.atoms:
                if a.idx in atomsel:
                    qmq += a.charge
                else:
                    mmq += a.charge
            totq = qmq + mmq

            print "AFTER charge adjustments:"
            print "The net charge of     res %5i [%3s] is %9.5f"%(res.idx+1,res.name,totq)
            print "The  QM charge within res %5i [%3s] is %9.5f"%(res.idx+1,res.name,qmq)
            print "The  MM charge within res %5i [%3s] is %9.5f"%(res.idx+1,res.name,mmq)
            print ""

        qmq=0
        for i in atomsel:
            qmq += self.qmparm.atoms[i].charge
        print "The MM charges of the whole QM region is %9.5f"%(qmq)
        print "           The expected net QM charge is %9.5f"%(qmcharge)
        if abs(qmq-qmcharge) >= 0.5:
            raise Exception("The charge adjustments are inconsistent with the desired QM charge")
        print ""
        

    def new_qm_nbtypes(self,frcmod=None,make_nonzero=True):
        #
        # This method creates new LJ types for the QM atoms
        # such that no MM atom shares the same LJ type as a
        # QM atom. This method could be extended to read a
        # frcmod file that defines new LJ parameters for the
        # QM atoms based on the atom-type name.
        #
        self.new_nbtypes(self.qmparm,self.qmmask,\
                         frcmod=frcmod,\
                         make_nonzero=make_nonzero,\
                         dbg=True)

    def new_nbtypes(self,parm,mask,frcmod=None,make_nonzero=False,dbg=False):
        #
        # This method creates new LJ types for the QM atoms
        # such that no MM atom shares the same LJ type as a
        # QM atom. This method could be extended to read a
        # frcmod file that defines new LJ parameters for the
        # QM atoms based on the atom-type name.
        #

        read_types = []
        if frcmod is not None:
            p = parmed.load_file(frcmod)
            read_types = [ t for t in p.atom_types ]
            
        
        mmtypes = ddict( list )
        qmtypes = ddict( list )
        qmidxs = GetSelectedAtomIndices(parm,mask)
        for a in parm.atoms:
            if a.idx in qmidxs:
                qmtypes[ a.nb_idx ].append( a.idx )
            else:
                mmtypes[ a.nb_idx ].append( a.idx )

        for nb_idx in qmtypes:
            idx = qmtypes[nb_idx][0]
            a = parm.atoms[idx]
            name = a.type
            eps = a.epsilon
            rmin = a.rmin

            needs_change = False
            is_new_param = False

            if name in read_types:
                eps  = p.atom_types[name].epsilon
                rmin = p.atom_types[name].rmin
                needs_change = True
                is_new_param = True
            elif (eps < 1.e-4 or rmin < 1.e-4) and make_nonzero:
                eps  = 0.0157
                rmin = 0.6000
                needs_change = True
                is_new_param = True
            elif nb_idx in mmtypes:
                needs_change = True
                
            if needs_change:
                sel = "@" + ",".join( ["%i"%(i+1) for i in qmtypes[nb_idx] ] )
                label="old"
                if is_new_param:
                    label="NEW"
                if dbg:
                    print "New LJ type for QM type %2s using %s parameters (rmin=%8.4f eps=%8.4f): %s"%(name,label,rmin,eps,sel)
                #print "pre",a.nb_idx,parm.parm_data['ATOM_TYPE_INDEX'][a.idx],parm.pointers['NTYPES'],len(parm.LJ_radius)
                parmed.tools.addLJType(parm,sel).execute()
#                for ai in qmtypes[ nb_idx ]:
#                    parm.atoms[ai].nb_idx = parm.pointers['NTYPES'][-1]
                #print "pos",a.nb_idx,parm.parm_data['ATOM_TYPE_INDEX'][a.idx],parm.pointers['NTYPES'],len(parm.LJ_radius)
                parm.LJ_radius[a.nb_idx-1] = rmin
                parm.LJ_depth[a.nb_idx-1] = eps
                #a.atom_type.set_lj_params(eps,rmin,None,None)
        parm.recalculate_LJ()
        
        

    def reorder_mm_residues(self, mmtrajin, mmtrajout, target_sele, reorderable_sele, num_reorder=1 ):
        #
        # If  target_sele=":1", reorderable_sele=":WAT", num_reorder=6
        # then the first 6 waters will be the 6 closest waters to residue 1
        #
        p = CopyParm( self.mmparm )

        path,ext = os.path.splitext( mmtrajin )
        
        fh = file("cpptraj.in","w")
        fh.write("parm %s\n"%(self.inp_parmfile))
        fh.write("trajin %s\n"%(mmtrajin))

        ires = GetSelectedResidueIndices(self.mmparm,target_sele)[0]

        apermol = copy.deepcopy(self.mmparm.parm_data["ATOMS_PER_MOLECULE"])
        for i in range(1,len(apermol)):
            apermol[i] += apermol[i-1]

        first_atom = self.mmparm.residues[ires].atoms[0].idx
        imol = 0
        for i in range(len(apermol)):
            if first_atom < apermol[i]:
                imol = i
                break
        if imol == 0:
            a1 = 0
            a2 = apermol[0]-1
        else:
            a1 = apermol[imol-1]
            a2 = apermol[imol]-1
            
        molmask = "@%i-%i"%(a1+1,a2+1)
        
        fh.write("autoimage %s origin familiar\n"%(molmask))
        fh.write("trajout img%s\n"%(ext))
        fh.write("run\n")
        fh.write("quit\n")
        fh.close()
        subprocess.call("cpptraj -i cpptraj.in",shell=True)
        
        target_atoms = GetSelectedAtomIndices(p,target_sele)
        
        r2 = list(GetSelectedResidueIndices(p,reorderable_sele))
        r2.sort()

        
        #traj = pytraj.load( "img%s"%(ext), self.inp_parmfile )
        #nframes = traj.n_frames
        #nat = traj.n_atoms

        shape,xyz = readcrds( "img%s"%(ext) )
        nframes = shape[0]
        nat = shape[1]
        
        fs = []

        for i in range(nframes):
            for a in range(nat):
                #p.atoms[a].xx = traj.xyz[i,a,0]
                #p.atoms[a].xy = traj.xyz[i,a,1]
                #p.atoms[a].xz = traj.xyz[i,a,2]
                p.atoms[a].xx = xyz[i,a,0]
                p.atoms[a].xy = xyz[i,a,1]
                p.atoms[a].xz = xyz[i,a,2]
                
                
            dd = ddict(float)
            for r in r2:
                r2min = 1.e+30
                for a in p.residues[r].atoms:
                    aidx = a.idx
                    for bidx in target_atoms:
                        #r2v = ((traj.xyz[i,aidx,:]-traj.xyz[i,bidx,:])**2).sum()
                        r2v = ((xyz[i,aidx,:]-xyz[i,bidx,:])**2).sum()
                        r2min = min(r2min,r2v)
                dd[r2min] = r

            idxs = [ dd[r2min] for r2min in sorted(dd) ]
            for iorder in range(num_reorder):
                inew = r2[iorder]
                iold = idxs[iorder]
                for o in range( len(p.residues[inew].atoms) ):
                    a = p.residues[inew].atoms[o]
                    b = p.residues[iold].atoms[o]
                    ai = a.idx
                    bi = b.idx
                    #a.xx = traj.xyz[i,bi,0]
                    #a.xy = traj.xyz[i,bi,1]
                    #a.xz = traj.xyz[i,bi,2]
                    #b.xx = traj.xyz[i,ai,0]
                    #b.xy = traj.xyz[i,ai,1]
                    #b.xz = traj.xyz[i,ai,2]
                    
                    a.xx = xyz[i,bi,0]
                    a.xy = xyz[i,bi,1]
                    a.xz = xyz[i,bi,2]
                    b.xx = xyz[i,ai,0]
                    b.xy = xyz[i,ai,1]
                    b.xz = xyz[i,ai,2]
                     
            f = "tmp.%06i.rst7"%(i)
            fs.append(f)
            parmed.tools.writeCoordinates(p, f).execute()
        
        fh=file("cpptraj.in","w")
        fh.write("parm %s\n"%(self.inp_parmfile))
        for f in fs:
            fh.write("trajin %s\n"%(f))
        fh.write("trajout %s\n"%(mmtrajout))
        fh.write("go\n")
        fh.write("quit\n")
        fh.close()
        subprocess.call("cpptraj -i cpptraj.in",shell=True)
        for f in fs:
            os.remove(f)
        os.remove("cpptraj.in")
        os.remove("img%s"%(ext))


        
    def mmtraj_to_qmtraj(self, mmtrajin, qmtrajout ):
        sel = "@" + ",".join(["%i"%(i+1) for i in self.atoms_to_delete])
        fh=file("cpptraj.in","w")
        fh.write("parm %s\n"%(self.inp_parmfile))
        fh.write("trajin %s\n"%(mmtrajin))
        if sel != "@":
            fh.write("strip \"%s\"\n"%(sel))
        fh.write("trajout %s\n"%(qmtrajout))
        fh.write("go\n")
        fh.write("quit\n")
        fh.close()
        subprocess.call("cpptraj -i cpptraj.in",shell=True)
        os.remove("cpptraj.in")


    def mmmask_to_qmmask(self,mmsele):
        mms = GetSelectedAtomIndices(self.mmparm,mmsele)
        qms = []
        for mm in mms:
            qm = self.i2o[mm]
            if qm is not None:
                qms.append(qm)
        return AtomListToSelection(self.qmparm,qms)


    def make_ghost(self,iparm,sele):
        p = CopyParm(iparm)
        if isinstance(sele,list):
            sele = AtomListToSelection(iparm,sele)
        atms = GetSelectedAtomIndices(p,sele)
        self.new_nbtypes(p,sele)
        for i in atms:
            p.LJ_radius[ p.atoms[i].nb_idx - 1 ] = 0
            p.LJ_depth[ p.atoms[i].nb_idx - 1 ]  = 0
        p.recalculate_LJ()
        for i in atms:
            p.atoms[i].charge = 0
        MakeUniqueBondParams(p,GetBondsWithinSelection(p,sele),k=0)
        MakeUniqueAngleParams(p,GetAnglesWithinSelection(p,sele),k=0)
        MakeUniqueDihedralParams(p,GetDihedralsWithinSelection(p,sele),phi_k=0)
        return p
        
    
    def save_mm_ghost(self,fname,sele):
        p = self.make_ghost(self.mmparm,sele)
        SaveParm(p,fname)
   
    def save_qm_ghost(self,fname,sele):
        p = self.make_ghost(self.qmparm,sele)
        SaveParm(p,fname)
        
    
    def select_first_res_named(self,resname,nres):
        if resname[0] != ":":
            resname = ":" + "resname"
        res = GetSelectedResidueIndices(self.mmparm,resname)
        res.sort()
        n = min(nres,len(res))
        res = res[:n]
        return ":" + ListToSelection(res)
    
    def calc_charge(self,p,sele):
        ats = GetSelectedAtomIndices(p,sele)
        q=0
        for a in ats:
            q += p.atoms[a].charge
        return q

    
    def write_fmatch_mdin(self,fname,noshakemask,qmsele=None,custom_exclusions=False,gasphase=False):
        opts="iwrap=1,ntb=1,cut=10.0"

        if self.mmparm.box is None:
            gasphase = True
            
        if gasphase:
            opts="iwrap=0,ntb=0,cut=99.0,fixcom=1,nscm=1000,vrand=1000,igb=6"
        ifqnt=0
        if qmsele is not None:
            if len(qmsele) > 0:
                ifqnt=1
        fh = file(fname,"w")        
        fh.write("""force matching
&cntrl
irest=0, ntx=1
ntxo=1, ioutfm=1
%s
imin=6, ntpr=1, ntwf=1, ntwr=0, ntwx=0
nstlim=0
dt=0.001
temp0=298, gamma_ln=5, ntt=3, ntp=0, ig=-1
ntc = 2, ntf=1
noshakemask = '%s'
ifqnt = %i
/
"""%(opts,noshakemask,ifqnt))

        if custom_exclusions:
            fh.write("""
&ewald
   chngmask = 0 ! default 1; set to 0 when using custom exclusions
/

""")
        
        if qmsele is not None:
            if len(qmsele) > 0:
                q = self.calc_charge( self.qmparm, qmsele )
                q = round(q)
                opts="qm_ewald = 1, hfdf_ewald = T"
                if gasphase:
                    opts="qm_ewald = 0, hfdf_ewald = F"
                fh.write("""&qmmm
qm_theory='HFDF', hfdf_theory = 'PBE0', hfdf_basis = '6-31G*', scfconv = 1e-07
hfdf_mempercore = 2000, hfdf_mm_percent = 0.0, hfdf_qmmm_wswitch = 0.0, qmmm_switch = 0, qmshake = 0
qmcharge=%i
qmmask='%s'
%s
/
"""%(q,qmsele,opts))
        fh.close()
        


    def write_respfit_mdin(self,fname,noshakemask,qmsele,gasphase=False):
        opts="iwrap=1,ntb=1,cut=10.0"

        if self.mmparm.box is None:
            gasphase = True
            
        if gasphase:
            opts="iwrap=0,ntb=0,cut=99.0,fixcom=1,nscm=1000,vrand=1000,igb=6"
        ifqnt=1

        fh = file(fname,"w")        
        fh.write("""respfit
&cntrl
irest=0, ntx=1
ntxo=1, ioutfm=1
%s
imin=6, ntpr=1, ntwf=1, ntwr=0, ntwx=0
nstlim=0
dt=0.001
temp0=298, gamma_ln=5, ntt=3, ntp=0, ig=-1
ntc = 2, ntf=1
noshakemask = '%s'
ifqnt = %i
/
"""%(opts,noshakemask,ifqnt))

        q = self.calc_charge( self.qmparm, qmsele )
        q = round(q)
        opts="qm_ewald = 1, hfdf_ewald = T"
        if gasphase:
            opts="qm_ewald = 0, hfdf_ewald = F"
        fh.write("""&qmmm
qm_theory='HFDF', hfdf_theory = 'PBE0', hfdf_basis = '6-31G*', scfconv = 1e-07
hfdf_mempercore = 2000, hfdf_mm_percent = 0.0, hfdf_qmmm_wswitch = 0.0, qmmm_switch = 0, qmshake = 0
qmcharge=%i
qmmask='%s'
%s
verbosity=2
/
"""%(q,qmsele,opts))
        fh.close()
        
            
    # def set_mmopt_lj(self,sele):
    #     MakeUniqueLJParams( self.mmparm, sele )

    # def set_mmopt_bonds(self,sele):
    #     xlist = GetBondsWithinSelection( self.mmparm, sele )
    #     MakeUniqueBondParams( self.mmparm, xlist )

    # def set_mmopt_angles(self,sele):
    #     xlist = GetAnglesWithinSelection( self.mmparm, sele )
    #     MakeUniqueAngleParams( self.mmparm, xlist )

    # def set_mmopt_dihedrals(self,sele):
    #     xlist = GetDihedralsWithinSelection( self.mmparm, sele )
    #     MakeUniqueDihedralParams( self.mmparm, xlist )


    def mmmask_to_mmmask(self,sele):
        return AtomListToSelection(self.mmparm, GetSelectedAtomIndices(self.mmparm,sele))
        

        
        
    def setup_fmatch_lj_qmopt(self,ljsele,qmsele,qmcharge,probes,mmtraj):
        self.set_mmopt_lj(ljsele)
        target = self.mmmask_to_mmmask(qmsele)

        irst=self.inp_rstfile
        itrj=mmtraj

        bdir = "ljopt"

        if not os.path.isdir(bdir):
            os.mkdir(bdir)
        
        probe_atoms = []
        nprobes = len(probes)
        for iprobe in range(nprobes):
            # e.g., ":WAT"
            restype_sele = probes[iprobe][0]
            # e.g., 6
            nprobe  = probes[iprobe][1]
            # logical, reorder if True
            reorder = probes[iprobe][2]
            # e.g., returns :61-71 or whatever the first 6 waters are
            if reorder:
                probe_sele = self.select_first_res_named(restype_sele,nprobe)
            else:
                probe_sele = restype_sele
            # a running list of atoms in the probe region
            probe_atoms += GetSelectedAtomIndices(self.mmparm,probe_sele)



        probe = AtomListToSelection( self.mmparm, probe_atoms )
        target_charge = qmcharge
        probe_charge = round( self.calc_charge( self.mmparm, probe ) )
        total_charge = target_charge + probe_charge
        self.set_qm("%s"%(target),target_charge)
        self.new_qm_nbtypes()
        self.save_qmparm(pathjoin(bdir,"QM.parm7"))
        
        
        first_reorder=True
        for iprobe in range(nprobes):
            # e.g., ":WAT"
            restype_sele = probes[iprobe][0]
            # e.g., 6
            nprobe  = probes[iprobe][1]
            # logical, reorder if True
            reorder = probes[iprobe][2]
 
            if reorder:
                if first_reorder:
                    shutil.copyfile(self.inp_rstfile,pathjoin(bdir,"MM.tmp.rst7"))
                    shutil.copyfile(mmtraj,pathjoin(bdir,"MM.tmp.nc"))
                    first_reorder=False
                else:
                    shutil.copyfile(pathjoin(bdir,"MM.rst7"),pathjoin(bdir,"MM.tmp.rst7"))
                    shutil.copyfile(pathjoin(bdir,"MM.nc"),pathjoin(bdir,"MM.tmp.nc"))
                    
                self.reorder_mm_residues(pathjoin(bdir,"MM.tmp.rst7"), pathjoin(bdir,"MM.rst7"), ljsele, restype_sele, num_reorder=nprobe )
                self.reorder_mm_residues(pathjoin(bdir,"MM.tmp.nc"), pathjoin(bdir,"MM.nc"), ljsele, restype_sele, num_reorder=nprobe )
                
        if os.path.exists(pathjoin(bdir,"MM.tmp.rst7")):
            os.remove(pathjoin(bdir,"MM.tmp.rst7"))
        if os.path.exists(pathjoin(bdir,"MM.tmp.nc")):
            os.remove(pathjoin(bdir,"MM.tmp.nc"))
            
        # We never reordered anything
        if first_reorder:
            shutil.copyfile(self.inp_rstfile,pathjoin(bdir,"MM.rst7"))
            shutil.copyfile(mmtraj,pathjoin(bdir,"MM.nc"))
            


        
        self.mmtraj_to_qmtraj( pathjoin(bdir,"MM.rst7"), pathjoin(bdir,"QM.rst7") )
        self.mmtraj_to_qmtraj( pathjoin(bdir,"MM.nc"), pathjoin(bdir,"QM.nc") )
        
        qmtarget       = self.mmmask_to_qmmask(target)
        qmtarget_atoms = GetSelectedAtomIndices(self.qmparm,qmtarget)
        # These are MM atoms in the QM-target parmfile
        qmprobe        = self.mmmask_to_qmmask(probe)
        qmprobe_atoms  = GetSelectedAtomIndices(self.qmparm,probe)

        qmtogether = "((%s)|(%s))"%(qmtarget,qmprobe)
        qmtogether_atoms  = GetSelectedAtomIndices(self.qmparm,qmtogether)

        qmother = "!((%s)|(%s))"%(qmtarget,qmprobe)
        qmother_atoms  = GetSelectedAtomIndices(self.qmparm,qmother)

        qmljsele = self.mmmask_to_qmmask(ljsele)
        qmljsele_atoms = GetSelectedAtomIndices(self.qmparm,qmljsele)
        
        
        ##################################
        p = CopyParm(self.qmparm)
        for i in range(len(p.LJ_radius)):
            p.LJ_radius[i]=0
            p.LJ_depth[i]=0
        p.recalculate_LJ()
        for i in range(len(p.bond_types)):
            p.bond_types[i].k=0
        for i in range(len(p.angle_types)):
            p.angle_types[i].k=0
        for i in range(len(p.dihedral_types)):
            p.dihedral_types[i].phi_k=0
        SaveParm( p, pathjoin(bdir,"QMEtpo.parm7") )
        self.write_fmatch_mdin( pathjoin(bdir,"QMEtpo.mdin"), noshakemask=qmtogether, qmsele=qmtarget )
        q = CopyParm(p)
        for i in range(len(q.atoms)):
            if i in qmprobe_atoms:
                q.atoms[i].charge = 0
        SaveParm( q, pathjoin(bdir,"QMEto.parm7") )
        self.write_fmatch_mdin( pathjoin(bdir,"QMEto.mdin"), noshakemask=qmtogether, qmsele=qmtarget )
        q = CopyParm(p)
        for i in range(len(q.atoms)):
            if i in qmtarget_atoms:
                q.atoms[i].charge = 0
        SaveParm( q, pathjoin(bdir,"QMEpo.parm7") )
        self.write_fmatch_mdin( pathjoin(bdir,"QMEpo.mdin"), noshakemask=qmtogether )
        q = CopyParm(p)
        for i in range(len(q.atoms)):
            if i in qmtarget_atoms or i in qmprobe_atoms:
                q.atoms[i].charge = 0
        SaveParm( q, pathjoin(bdir,"QMEo.parm7") )
        self.write_fmatch_mdin( pathjoin(bdir,"QMEo.mdin"), noshakemask=qmtogether )

        
        # electrostatic interaction energy
        # Eint = QMtpe - QMte - QMpe + QMe
        # for a non-polarizable model, this is also
        # Eint = QMtp - QMt - QMp
        
        ##################################
        p = CopyParm( self.qmparm )
        self.new_nbtypes(p,qmprobe)
        for i in qmtarget_atoms:
            for j in qmtarget_atoms:
                if j != i:
                    p.atoms[i].exclude( p.atoms[j] )
        p.recalculate_LJ()
        for i in range(len(p.atoms)):
            p.atoms[i].charge = 0

        for a in qmljsele_atoms:
            if a not in qmtarget_atoms:
                at = self.qmparm.atoms[a]
                raise Exception("LJ opt of atom :%i@%-4s but it is not in the target region"%(at.residue.idx+1,at.name))
            
        #nbidxs = list(set([ p.atoms[a].nb_idx for a in qmljsele_atoms ]))
        
        if 'OPT_LJ_ATOMSEL' in p.parm_data:
            p.parm_data['OPT_LJ_ATOMSEL'] = [ i+1 for i in qmljsele_atoms ]
        else:
            p.add_flag('OPT_LJ_ATOMSEL','10I8',data=[ i+1 for i in qmljsele_atoms ])
            
        q = self.make_ghost( p, "!(%s)"%(qmprobe) )
        SaveParm( q, pathjoin(bdir,"QMNp.parm7") )
        self.write_fmatch_mdin( pathjoin(bdir,"QMNp.mdin"), noshakemask=qmtogether )
        q = self.make_ghost( p, "!(%s)"%(qmtarget) )
        SaveParm( q, pathjoin(bdir,"QMNt.parm7") )
        self.write_fmatch_mdin( pathjoin(bdir,"QMNt.mdin"), noshakemask=qmtogether, custom_exclusions=True )
        q = self.make_ghost( p, qmother )
        SaveParm( q, pathjoin(bdir,"QMNtp.parm7") )
        self.write_fmatch_mdin( pathjoin(bdir,"QMNtp.mdin"), noshakemask=qmtogether, custom_exclusions=True )

        
        
        o = MM2QMMM(pathjoin(bdir,"QM.parm7"),mmrst)
        o.set_qm("(%s)|(%s)"%(qmtarget,qmprobe),total_charge) #,adjustq=False)
        o.new_qm_nbtypes()
                                         

        tmp_data = []
        for i in range(len(o.qmparm.atoms)):
            a = o.o2i[i]
            tmp_data.append(a)

        if 'TOMMMAP' in o.qmparm.parm_data:
            o.qmparm.parm_data['TOMMMAP'] = tmp_data
        else:
            o.qmparm.add_flag('TOMMMAP','10I8',data=tmp_data)
        
        o.mmtraj_to_qmtraj( pathjoin(bdir,"QM.rst7"), pathjoin(bdir,"QMQM.rst7") )
        o.mmtraj_to_qmtraj( pathjoin(bdir,"QM.nc"), pathjoin(bdir,"QMQM.nc") )
                                         
        qmqmtarget = o.mmmask_to_qmmask( qmtarget )
        qmqmprobe  = o.mmmask_to_qmmask( qmprobe )
        qmqmtarget_atoms = GetSelectedAtomIndices(o.qmparm,qmqmtarget)
        qmqmprobe_atoms  = GetSelectedAtomIndices(o.qmparm,qmqmprobe)
        qmqmtogether = "((%s)|(%s))"%(qmqmtarget,qmqmprobe)
        qmqmtogether_atoms  = GetSelectedAtomIndices(o.qmparm,qmqmtogether)
        qmqmother = "!((%s)|(%s))"%(qmqmtarget,qmqmprobe)
        qmqmother_atoms  = GetSelectedAtomIndices(o.qmparm,qmqmother)
                                         
        o.save_qmparm(pathjoin(bdir,"QMQMtpo.parm7"))
        self.write_fmatch_mdin( pathjoin(bdir,"QMQMtpo.mdin"), noshakemask=qmqmtogether, qmsele=qmqmtogether )
        q = self.make_ghost( o.qmparm, qmqmprobe )
        SaveParm( q, pathjoin(bdir,"QMQMto.parm7") )
        self.write_fmatch_mdin( pathjoin(bdir,"QMQMto.mdin"), noshakemask=qmqmtogether, qmsele=qmqmtarget )
        q = self.make_ghost( o.qmparm, qmqmtarget )
        SaveParm( q, pathjoin(bdir,"QMQMpo.parm7") )
        self.write_fmatch_mdin( pathjoin(bdir,"QMQMpo.mdin"), noshakemask=qmqmtogether, qmsele=qmqmprobe )
        q = self.make_ghost( o.qmparm, qmqmtogether )
        SaveParm( q, pathjoin(bdir,"QMQMo.parm7") )
        self.write_fmatch_mdin( pathjoin(bdir,"QMQMo.mdin"), noshakemask=qmqmtogether )

        fh = file("ljopt_ref.slurm","w")
        fh.write("""#!/bin/bash
#SBATCH --job-name="ljopt_ref"
#SBATCH --output="ljopt_ref.slurmout"
#SBATCH --error="ljopt_ref.slurmerr"
#
#SBATCH --partition=perceval
#SBATCH --ntasks-per-node=24
#SBATCH --exclude=cuda[001-008],memnode001
#
##SBATCH --partition=main
##SBATCH --qos=main-award
##SBATCH -A giese025-001
##SBATCH --ntasks-per-node=36
#
#SBATCH --nodes=4
#SBATCH --exclusive
#SBATCH --export=ALL
#SBATCH --time=2-00:00:00


export MV2_ENABLE_AFFINITY=0


if [ "${SLURM_JOB_NUM_NODES}" != "" ]; then
    if [ "${SLURM_NTASKS_PER_NODE}" == "" ]; then
	echo "SLURM_NTASKS_PER_NODE is not defined"
	exit 1
    fi
    export launch="srun --mpi=pmi2 -K1"
    export gsize=$(bc <<< "${SLURM_JOB_NUM_NODES} * ${SLURM_NTASKS_PER_NODE}")
    export gmmsize="${SLURM_NTASKS_PER_NODE}"
else
    export launch="mpirun -n 6"
    export gsize=6
    export gmmsize=6
fi

export bdir="ljopt/"

if [ ! -d ${bdir} ]; then
    echo "directory not found! ${bdir}"
    exit 1
fi

for base in QMQMtpo QMQMto QMQMpo QMQMo;  do
    echo ${base}
    
    ${launch} sander.MPI -gsize ${gsize} -gmmsize ${gmmsize} -O \
	      -p ${bdir}${base}.parm7 \
	      -o ${bdir}${base}.mdout \
	      -i ${bdir}${base}.mdin \
	      -c ${bdir}QMQM.rst7 \
	      -r ${bdir}foo.rst7 \
	      -x ${bdir}foo.nc \
	      -inf ${bdir}foo.mdinfo \
	      -y ${bdir}QMQM.nc \
	      -frc ${bdir}${base}.frc.nc

done

for base in QMEtpo QMEto QMEpo QMEo QMNt QMNp;  do
    echo ${base}
    
    ${launch} sander.MPI -gsize ${gsize} -gmmsize ${gmmsize} -O \
	      -p ${bdir}${base}.parm7 \
	      -o ${bdir}${base}.mdout \
	      -i ${bdir}${base}.mdin \
	      -c ${bdir}QM.rst7 \
	      -r ${bdir}foo.rst7 \
	      -x ${bdir}foo.nc \
	      -inf ${bdir}foo.mdinfo \
	      -y ${bdir}QM.nc \
	      -frc ${bdir}${base}.frc.nc

done

for f in foo.rst7 foo.nc foo.mdinfo; do
    if [ -e ${bdir}${f} ]; then
       rm ${bdir}${f}
    fi
done

if [ -e hfdf_trajfile ]; then
     rm hfdf_trajfile
fi

""")

                                         


def EXAMPLE_ExtractParameters():
    p = OpenParm("ljopt/QMNtp.best.parm7")
    sele = "@3589-3603"
    ExtractFrcmod(p,sele,"tmp.frcmod",masses=True,bonds=True,angles=True,dihedrals=True,lj=True)


def EXAMPLE_LJOPT_QMMM():
    
    mmparm = "c634t36sG.parm7"
    mmrst  = "sim.rst7"
    mmtraj = "sim.nc"
    
    o = MM2QMMM(mmparm,mmrst)

    optmask  = ":112&@S6"
    qmmask   = ":112&!(@*',P,OP*)"
    qmcharge = 0
    probes = [ (":WAT",3,True),
               ("(:94@*'&!@O5',C5',H5',H5'')|(:95@P,OP*,O5',C5',H5',H5'')",1,False) ]
    
    o.setup_fmatch_lj_qmopt(optmask,qmmask,qmcharge,probes,mmtraj)



    

if __name__ == "__main__":

    mmparm = "c634t36sG.parm7"
    mmrst  = "sim.rst7"
    mmtraj = "sim.nc"
    
    o = MM2QMMM(mmparm,mmrst)

    optmask  = ":112&@S6"
    qmmask   = ":112&!(@*',P,OP*)"
    qmcharge = 0
    probes = [ (":WAT",3,True),
               ("(:94@*'&!@O5',C5',H5',H5'')|(:95@P,OP*,O5',C5',H5',H5'')",1,False) ]

    o.setup_fmatch_lj_qmopt(optmask,qmmask,qmcharge,probes,mmtraj)


    
