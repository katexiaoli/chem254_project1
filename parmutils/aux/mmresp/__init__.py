#!/usr/bin/env python
from atizer import *
from atizer.dbase import *

class mmresp(autoprog):
    def __init__( self, srcdir=None ):
        super( mmresp, self ).__init__( "mmresp", srcdir )

        self.copyright_holder = "Timothy Giese"
        self.license = licenses.MIT

        ## @brief List of autolib objects representing library dependencies
        self.libs = [  ]

        ## @brief List of filenames to be distributed, but not installed
        self.dist_noinst_SCRIPTS = []
        self.EXTRA_DIST = []

        ## @brief If True, then compile the target without installing it
        #         (default False)
        self.noinst = False

        ## @brief If True, "make doxygen-doc" create documentation html
        self.doxygen = False

        # self.enable_openmp()
        # self.enable_mpi()

package = autopackage(
    "mmresp",
    targets=[ mmresp( here() ) ],
    subdirs=[],
    version="0.1",
    apiversion="0:0:0")

if __name__ == "__main__":
    package.configure()
