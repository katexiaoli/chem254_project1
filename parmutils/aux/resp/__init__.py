#!/usr/bin/env python
from atizer import *
from atizer.dbase import *

class resp(autoprog):
    def __init__( self, srcdir=None ):
        super( resp, self ).__init__( "resp", srcdir )

        self.copyright_holder = "UNIVERSITY OF CALIFORNIA"
        self.license = licenses.MIT

        ## @brief List of autolib objects representing library dependencies
        self.libs = [  ]

        self.sources = [ "resp.f" ]
        self.has_fortran=True
        
        ## @brief List of filenames to be distributed, but not installed
        self.dist_noinst_SCRIPTS = []
        self.EXTRA_DIST = []

        ## @brief If True, then compile the target without installing it
        #         (default False)
        self.noinst = False

        ## @brief If True, "make doxygen-doc" create documentation html
        self.doxygen = False

        # self.enable_openmp()
        # self.enable_mpi()

package = autopackage(
    "resp",
    targets=[ resp( here() ) ],
    subdirs=[],
    version="0.1",
    apiversion="0:0:0")

if __name__ == "__main__":
    package.configure()
