#!/usr/bin/env python
from atizer import *
from atizer.dbase import *
from lib import package as lib
from bin import package as bin
from aux import package as aux

package = autopackage(
    "parmutils",
    targets=[],
    subdirs=[lib,bin,aux],
    version="0.1",
    apiversion="0:0:0")

if __name__ == "__main__":
    package.configure()
