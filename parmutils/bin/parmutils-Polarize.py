#!/usr/bin/python

import parmed
import parmutils
import argparse



def UniformPolarization(parmobj,maskstr,factor,outparm):
    from collections import defaultdict as ddict
    atomsel = parmutils.GetSelectedAtomIndices(parmobj,maskstr)
    ressel = ddict( list )
    for idx in atomsel:
        # exclude dummy atoms (atoms with zero charge)
        if abs(parmobj.atoms[idx].charge) > 0.0001:
            ressel[ parmobj.atoms[idx].residue.idx ].append( idx )

    for residx in ressel:
        resq = 0.
        for idx in ressel[residx]:
            resq += parmobj.atoms[idx].charge
        osum=resq
        avgq = resq / len(ressel[residx])
        psum=0.
        changes=[]
        for idx in ressel[residx]:
            q = parmobj.atoms[idx].charge
            parmobj.atoms[idx].charge = factor * (q-avgq) + avgq
            psum += parmobj.atoms[idx].charge
            changes.append( abs(q-parmobj.atoms[idx].charge) )
            print "%6i %5s %10.6f <- %9.6f"%(\
                    residx+1,\
                    parmobj.atoms[idx].name,\
                    parmobj.atoms[idx].charge,\
                    q)
        print "%6i %5s %10.6f <- %9.6f"%(residx+1,"sum",psum,osum)
        print "%6i %5s %10.6f\n"%(residx+1,"<del>",sum(changes)/len(changes))

        
    parmutils.SaveParm( parmobj, outparm )
    
        


if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""The atomic charges are uniformly scaled about the mean to increase or decrease
their polarization. The scaling occurs on a per-residue basis, and each residue
will maintain their original net charge.  If the selection only covers a part
of a residue, then only that part is scaled and its net charge is preserved.
For example, to increase the polarization of all nucleobases by 10%, try:
parmutils-Polarize.py -p in.parm7 -c in.rst7 -o out.parm7 -f 1.1 -m ":1-20&!(@P,OP1,OP2,*')"
""" )
    
    parser.add_argument \
        ("-p","--parm",
         help="parm7 file",
         type=str,
         required=True )

    
    parser.add_argument \
        ("-c","--crd",
         help="input restart file, needed only to determine unit cell info",
         type=str,
         required=True )
    
    parser.add_argument \
        ("-f","--factor",
         help="scale factor (1.0 = no polarization, 0.0 = all atoms have the same charge)",
         type=float,
         default=1.0,
         required=True )

    parser.add_argument \
        ("-m","--mask",
         help="amber mask selection of atoms to polarize",
         type=str,
         required=True )

    parser.add_argument \
        ("-o","--oparm",
         help="output parameter file",
         type=str,
         required=True )

    args = parser.parse_args()
    

    parmfile = args.parm
    crd7file = args.crd
    factor   = args.factor
    maskstr  = args.mask
    outparm  = args.oparm
    
    parmobj = parmutils.OpenParm( parmfile, xyz=crd7file )
    UniformPolarization(parmobj,maskstr,factor,outparm)

    
