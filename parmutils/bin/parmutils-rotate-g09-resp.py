#!/usr/bin/env python2.7

import numpy as np
import parmutils.respfit as rf
import parmed.periodic_table as pt
from collections import defaultdict as ddict

def GetEulerRmat(alpha,beta,gamma):
    import numpy as np
    
    """Function for calculating the z-y-z Euler angle convention rotation matrix.

    Unit vectors
    ============

    The unit mux vector is::

                | -sin(alpha) * sin(gamma) + cos(alpha) * cos(beta) * cos(gamma) |
        mux  =  | -sin(alpha) * cos(gamma) - cos(alpha) * cos(beta) * sin(gamma) |.
                |                    cos(alpha) * sin(beta)                      |

    The unit muy vector is::

                | cos(alpha) * sin(gamma) + sin(alpha) * cos(beta) * cos(gamma) |
        muy  =  | cos(alpha) * cos(gamma) - sin(alpha) * cos(beta) * sin(gamma) |.
                |                   sin(alpha) * sin(beta)                      |

    The unit muz vector is::

                | -sin(beta) * cos(gamma) |
        muz  =  |  sin(beta) * sin(gamma) |.
                |        cos(beta)        |

    Rotation matrix
    ===============

    The rotation matrix is defined as the vector of unit vectors::

        R = [mux, muy, muz].


    @param matrix:  The 3x3 rotation matrix to update.
    @type matrix:   3x3 numpy array
    @param alpha:   The alpha Euler angle in rad.
    @type alpha:    float
    @param beta:    The beta Euler angle in rad.
    @type beta:     float
    @param gamma:   The gamma Euler angle in rad.
    @type gamma:    float
    """

    alpha *= np.pi/180.
    beta  *= np.pi/180.
    gamma *= np.pi/180.

    
    # Trig.
    sin_a = np.sin(alpha)
    sin_b = np.sin(beta)
    sin_g = np.sin(gamma)

    cos_a = np.cos(alpha)
    cos_b = np.cos(beta)
    cos_g = np.cos(gamma)

    matrix = np.zeros( [3,3] )
    
    # The unit mux vector component of the rotation matrix.
    matrix[0,0] = -sin_a * sin_g + cos_a * cos_b * cos_g
    matrix[1,0] = -sin_a * cos_g - cos_a * cos_b * sin_g
    matrix[2,0] =  cos_a * sin_b

    # The unit muy vector component of the rotation matrix.
    matrix[0,1] = cos_a * sin_g + sin_a * cos_b * cos_g
    matrix[1,1] = cos_a * cos_g - sin_a * cos_b * sin_g
    matrix[2,1] = sin_a * sin_b

    # The unit muz vector component of the rotation matrix.
    matrix[0,2] = -sin_b * cos_g
    matrix[1,2] =  sin_b * sin_g
    matrix[2,2] =  cos_b
    return matrix



if __name__ == "__main__":

    import subprocess
    import argparse

    atnums = ddict(str)
    for a in pt.AtomicNum:
        atnums[a] = pt.AtomicNum[a]
        atnums[ a.upper() ] = pt.AtomicNum[a]
        atnums[ a.lower() ] = pt.AtomicNum[a]
    

    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""Writes a Gaussian input file, given a mol2 file""" )
    
    parser.add_argument \
        ("-n","--nproc",
         help="number of cores (default: 6)",
         type=int,
         default=6,
         required=False )

    parser.add_argument \
        ("-a","--alp",
         help="alpha (z, degrees) (default: 0)",
         type=int,
         default=0,
         required=False )

    parser.add_argument \
        ("-b","--bet",
         help="beta (y, degrees) (default: 0)",
         type=int,
         default=0,
         required=False )

    parser.add_argument \
        ("-g","--gam",
         help="gamma (z', degrees) (default: 0)",
         type=int,
         default=0,
         required=False )

    parser.add_argument \
        ("--run",
         help="if present, then run the job",
         action='store_true',
         required=False )

    parser.add_argument \
        ("--test",
         help="if present, then just show how the rotation affects the Cartesian unit vectors",
         action='store_true',
         required=False )

    parser.add_argument \
        ('filename',
         metavar='filename',
         type=str,
         nargs=1,
         help='Gaussian output file (ends in .log)')

    args = parser.parse_args()


    
    m = GetEulerRmat( args.alp, args.bet, args.gam )
    if args.test:
        print "#      --alp=%.2f --bet=%.2f --gam=%.2f"%(args.alp,args.bet,args.gam)
        u = np.array([1,0,0])
        v = np.dot(m,u)
        print "[%6.2f,%6.2f,%6.2f] => [%6.2f,%6.2f,%6.2f]"%(u[0],u[1],u[2],v[0],v[1],v[2])
        u = np.array([0,1,0])
        v = np.dot(m,u)
        print "[%6.2f,%6.2f,%6.2f] => [%6.2f,%6.2f,%6.2f]"%(u[0],u[1],u[2],v[0],v[1],v[2])
        u = np.array([0,0,1])
        v = np.dot(m,u)
        print "[%6.2f,%6.2f,%6.2f] => [%6.2f,%6.2f,%6.2f]"%(u[0],u[1],u[2],v[0],v[1],v[2])
        exit(0)


        
    base = args.filename[0].replace(".log","")
    if base == args.filename[0]:
        raise Exception("Filename %s does not end in .log"%(args.filename[0]))

    atn,crds,charge,mult = rf.ReadGauOutput( args.filename[0] )

    nat    = len(atn)
    zs = []
    for at in atn:
        try:
            z = int(at)
        except:
            if at in atnums:
                z = atnums[at]
            else:
                print "Unknown atom %s : assigning atomic number 1"%(at)
                z = 1
        zs.append(z)
            
        
    zsum   = sum(zs)
    center = [0.,0.,0.]
    for i in range(nat):
        for k in range(3):
            center[k] += (zs[i]/zsum) * crds[k+i*3]
    ocrds = [0.]*(3*nat)
    center = np.array(center)
    for i in range(nat):
        a = np.array( [ crds[0+i*3], crds[1+i*3], crds[2+i*3] ] )
        v = np.dot( a-center, m ) + center
        ocrds[0+i*3] = v[0]
        ocrds[1+i*3] = v[1]
        ocrds[2+i*3] = v[2]


    crds = ocrds
    base = "%s_%.2f_%.2f_%.2f"%(base,args.alp,args.bet,args.gam)
    
    fh = file(base+".com","w")
    fh.write("""%%MEM=2000MB
%%NPROC=%i

#P HF/6-31G* SCF(Conver=6) NoSymm Test 
   Pop=mk IOp(6/33=2) GFInput GFPrint 

RESP

%i %i
"""%(args.nproc,charge,mult))
    for i in range(len(atn)):
        fh.write("%4s %12.8f %12.8f %12.8f\n"%(str(atn[i]),crds[0+i*3],crds[1+i*3],crds[2+i*3]))
    fh.write("\n\n\n")
    fh.close()

    if args.run:
        print "# Running g09 < %s.com > %s.log"%(base,base)
        subprocess.call("g09 < %s.com > %s.log"%(base,base), shell=True)

    
