#!/usr/bin/python

import parmed
import parmutils
import argparse

    

if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    (
        #formatter_class=argparse.RawDescriptionHelpFormatter,
        description="""Write a frcmod, lib, and pdb from a parm7 file that can be read by tleap to
        reproduce the parm file. It also writes a shell script that uses tleap to regenerate the parm file.
""" )
    
    parser.add_argument \
        ("-p","--parm",
         help="parm7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("-c","--rst",
         help="rst7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("-o","--base",
         help="basename of output files (defaults to parm7-file's basename)",
         type=str,
         default=None,
         required=False )

    args = parser.parse_args()
    
    parmfile = args.parm
    rstfile  = args.rst
    if args.base is not None:
        base = args.base
    else:
        base = parmfile.replace(".parm7","")
        if base == parmfile:
            base = "base"
    

if True:
    # WRITE PDB OFF FRCMOD
    print "Reading %s and %s"%(parmfile,rstfile)
    param = parmed.load_file(parmfile,xyz=rstfile)
    
    print "Writing %s.frcmod"%(base)
    # The parmed frcmod writer doesn't consider the difference
    # between the value of PI used in tleap vs python
    # Also, it doesn't always print enough digits.
    #parmed.tools.writeFrcmod(param, base+".frcmod").execute()
    parmutils.WriteMaskedFrcmod(param,[],base+".frcmod",base+".dummy.frcmod")
    
    print "Writing %s.pdb"%(base)
    parmed.tools.writeCoordinates(param, base+".pdb").execute()
    
    print "Writing %s.lib"%(base)
    parmed.tools.writeOFF(param, base+".lib").execute()
    
    print "Writing %s.sh"%(base)
    parmutils.WriteLeapSh \
        ("%s.sh"%(base),
         param,
         ["%s.lib"%(base)],
         ["%s.frcmod"%(base)],
         "%s.pdb"%(base),
         base)
    


    
