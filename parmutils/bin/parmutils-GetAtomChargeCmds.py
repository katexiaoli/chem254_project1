#!/usr/bin/python
import parmed
import sys

if len(sys.argv) < 3:
    raise Exception("GetAtomInfo.py parm.parm7 'mask'")

parmfile = sys.argv[1]
#index = int(sys.argv[2])

param = parmed.load_file(parmfile)
mask = parmed.amber.mask.AmberMask( param, sys.argv[2] )
atoms = mask.Selected()
print """
function set_charge_FOO()
{
    selection="$1"
    cat <<EOF
"""
for at in atoms:
    atom = param.atoms[at]
    res  = atom.residue
    print "set ${selection}.%-5s charge %9.6f"%(atom.name,atom.charge)
#    print "%6i %4s%5i:%4s  [%-4s]   Q=%9.6f   Rmin/2=%9.6f   Eps=%9.6f"%(atom.idx+1,res.name,res.number+1,atom.name,atom.type,atom.charge,atom.rmin,atom.epsilon)
print """EOF
}
"""

