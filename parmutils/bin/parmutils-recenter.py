#!/usr/bin/python
import parmed
import argparse
import subprocess as sp
import os
import fileinput
import glob


def OpenParm( fname, xyz=None ):
    import parmed
    from parmed.constants import IFBOX
    if ".mol2" in fname:
        param = parmed.load_file( fname, structure=True )
        #help(param)
    else:
        param = parmed.load_file( fname,xyz=xyz )
    if param.box is not None:
        if abs(param.box[3]-109.471219)<1.e-4 and \
           abs(param.box[4]-109.471219)<1.e-4 and \
           abs(param.box[5]-109.471219)<1.e-4:
            param.parm_data["POINTERS"][IFBOX]=2
            param.pointers["IFBOX"]=2
    return param


    
def GetSelectedAtomIndices(param,maskstr):
    #param = parmed.load_file(parmfile)
    #mask = parmed.amber.mask.AmberMask( param, maskstr )
    #aidxs = mask.Selected()
    #for aidx in aidxs:
    #    atom = param.atoms[aidx]
    #    res  = atom.residue
    sele = []
    if len(maskstr) > 0:
        sele = [ param.atoms[i].idx for i in parmed.amber.mask.AmberMask( param, maskstr ).Selected() ]
    return sele

def ListToSelection(atomlist):
    alist = list(sorted(set(atomlist)))
    rs=[]
    if len(alist) > 0:
        rs = [ (alist[0],alist[0]) ]
        for a in alist[1:]:
            if a == rs[-1][1]+1:
                rs[-1] = ( rs[-1][0], a )
            else:
                rs.append( (a,a) )
    sarr = []
    for r in rs:
        if r[0] != r[1]:
            sarr.append( "%i-%i"%(r[0]+1,r[1]+1) )
        else:
            sarr.append( "%i"%(r[0]+1) )
    sele = "@0"
    if len(sarr) > 0:
        sele = "@" + ",".join(sarr)
    return sele




if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""Converts an arbitrary amber mask to an atom selection mask
""" )
    
    parser.add_argument \
        ("-p","--parm",
         help="parm7 file",
         type=str,
         required=True )


    parser.add_argument \
        ("-c","--crd",
         help="input restart file",
         type=str,
         default=None,
         required=True )

    
    parser.add_argument \
        ("-r","--rst",
         help="output restart file",
         type=str,
         default=None,
         required=False )
    
    parser.add_argument \
        ("-s","--strip",
         help="strip solvent from output restart file. If a parm7 filename is supplied as an argument to this option, then a stripped parm7 is also written. If the argument is '', then the coordinates are stripped, but the parm7 is not written",
         type=str,
         default=None,
         required=False )

    parser.add_argument \
        ( "-n","--norem",
          help="if present, then remove the temperature field from the restart",
          action='store_true')
    
    
    args = parser.parse_args()
    

    if args.strip is not None:
        if len(args.strip) > 0:
            if not ".parm7" in args.strip:
                raise Exception("If an argument is supplied to --strip, then it must be a .parm7 filename")
            elif args.strip == args.parm:
                raise Exception("I will not overwrite the --parm file with the --strip file under any circumstance")

            
    
    parmfile = args.parm
    crd7file = args.crd
    rst7file = args.rst
    

    parm  = OpenParm( parmfile )
    atoms = GetSelectedAtomIndices( parm, ":1" )

    
    smols=[]
    iat = 0
    for imol,mnat in enumerate(parm.parm_data["ATOMS_PER_MOLECULE"]):
        for i in range(mnat):
            if iat in atoms:
                smols.append( imol )
            iat += 1
    smols=list(set(smols))
    smols.sort()
    atoms = []
    iat=0
    for imol,mnat in enumerate(parm.parm_data["ATOMS_PER_MOLECULE"]):
        for i in range(mnat):
            if imol in smols:
                atoms.append(iat)
            iat += 1 

    res = []
    for a in atoms:
        res.append( parm.atoms[a].residue.idx )
    res = list(set(res))
    res.sort()


    s = ListToSelection( res )
    s = s.replace("@",":")



    ptr = parm.parm_data['SOLVENT_POINTERS']

    moloffs = [ 0 ]
    for m in parm.parm_data['ATOMS_PER_MOLECULE']:
        moloffs.append( moloffs[-1] + m )
        
    molroffs = []
    for m in moloffs[:-1]:
        molroffs.append( parm.atoms[ m ].residue.idx )

    smlres = 1000000
    for im,m in enumerate(parm.parm_data['ATOMS_PER_MOLECULE']):
        if m == 1:
            smlres = molroffs[ im ]+1
            break
    first_solv = min( smlres, ptr[2] )
        
        
    print smlres,ptr[2]
    solv = ":%i-999999"%(first_solv)
    


    path, filename = os.path.split(crd7file)
    basename, ext  = os.path.splitext(filename)
    outfile = os.path.join(path, basename + ".tmp" + ext )
    if rst7file is not None:
        if rst7file != crd7file:
            outfile = rst7file
    
    sin = """
parm %s
trajin %s
center %s origin mass
image origin center familiar
"""%( parm, crd7file, s )

    if args.strip is not None:
        if len(args.strip) > 0:
            sin += "strip %s parmout %s nobox"%(solv,args.strip)
        else:
            sin += "strip %s nobox"%(solv)

    options = ""
    if ".rst7" in outfile:
        options = "keepext"
        
    sin += """
trajout %s %s
go
quit
"""%( outfile, options )

    import sys
    cols=None
    if ".rst7" in crd7file:
        iline=0
        for line in fileinput.FileInput(crd7file,inplace=1):
            iline += 1
            if iline == 2:
                cols =  line.strip().split()
                if len(cols) > 2:
                    print "%5i  %13.7e"%(int(cols[0]), float(cols[1]))
                    # except Exception as e:
                    #     sys.stdout.write("Exception %s\n"%(e))
                    #     sys.stdout.write("cols[0] = %s\n"%(cols[0]))
                    #     sys.stdout.write("cols[1] = %s\n"%(cols[1]))
                    #     raise Exception(e)
                else:
                    print line,
            else:
                print line,

    proc = sp.Popen(["cpptraj"],stdin=sp.PIPE)
    sout,serr = proc.communicate( input=sin )

    if args.norem and cols is not None:
        if len(cols) > 2:
            cols = [ cols[0], cols[1] ]
        else:
            cols = None
            
    if ".rst7" in outfile and cols is not None:
        if len(cols) > 2:
            iline=0
            for line in fileinput.FileInput(outfile,inplace=1):
                iline += 1
                if iline == 2:
                    newcols =  line.strip().split()
                    print "%5i"%(int(newcols[0])),
                    for c in cols[1:]:
                        print "%13.7e"%(float(c)),
                    print ""
                else:
                    print line,

    if ".rst7" in outfile and ".rst7" not in crd7file:
        # there is no velocity info, so strip the second line
        fs = []
        if os.path.exists(outfile):
            fs=[outfile]
        else:
            fs=glob.glob( outfile.replace(".rst7",".*.rst7") )
        for f in fs:
            iline=0
            for line in fileinput.FileInput(f,inplace=1):
                iline += 1
                if iline == 2:
                    cols =  line.strip().split()
                    print cols[0]
                else:
                    print line,
                    
    
    if outfile != rst7file:
        os.rename( outfile, crd7file )
    
