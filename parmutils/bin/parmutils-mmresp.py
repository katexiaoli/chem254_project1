#!/usr/bin/env python2.7

import parmed
import sys
import subprocess
import argparse


def OpenParm( fname, xyz=None ):
    import parmed
    from parmed.constants import IFBOX
    if ".mol2" in fname:
        param = parmed.load_file( fname, structure=True )
        #help(param)
    else:
        param = parmed.load_file( fname,xyz=xyz )
        if xyz is not None:
            if ".rst7" in xyz:
                param.load_rst7(xyz)
    if param.box is not None:
        if abs(param.box[3]-109.471219)<1.e-4 and \
           abs(param.box[4]-109.471219)<1.e-4 and \
           abs(param.box[5]-109.471219)<1.e-4:
            param.parm_data["POINTERS"][IFBOX]=2
            param.pointers["IFBOX"]=2
    return param



if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""Recalculate MM RESP charges of a TI region when some of the atoms are removed
""" )
    
    parser.add_argument \
        ("-p","--parm",
         help="parm7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("-c","--rst7",
         help="rst7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("-t","--timask",
         help="Amber mask of the TI region",
         type=str,
         required=True )

    parser.add_argument \
        ("-s","--scmask",
         help="Amber mask of the softcore region (these atoms are being removed or replaced)",
         type=str,
         required=True )

    parser.add_argument \
        ("-H","--equivH",
         help="enforce equivalent hydrogens in the fit",
         action='store_true',
         default=False,
         required=False )


    parser.add_argument \
        ("-Q","--equivQ",
         help="preserve equivalent charges in the fit",
         action='store_true',
         default=False,
         required=False )

    
    args = parser.parse_args()

    parmfile = args.parm
    rst      = args.rst7
    timask   = args.timask
    scmask   = args.scmask
    while '\\' in timask:
        timask = timask.replace('\\',"")
    while '\\' in scmask:
        timask = scmask.replace('\\',"")

    p = OpenParm(parmfile,xyz=rst)
    atoms = [ p.atoms[i].idx for i in parmed.amber.mask.AmberMask( p, timask ).Selected() ]
    disappearing = [ p.atoms[i].idx for i in parmed.amber.mask.AmberMask( p, scmask ).Selected() ]

    umask = [0]*len(atoms)
    for i in disappearing:
        umask[i] = -1
    for i in range(len(atoms)):
        if i in disappearing:
            continue
        if umask[i] > 0:
            continue
        qi = p.atoms[i].charge
        for j in range(i+1,len(atoms)):
            qj = p.atoms[j].charge
            if args.equivQ:
                if abs(qj-qi) < 1.e-5:
                    umask[j]=i+1
            elif args.equivH:
                if abs(qj-qi) < 1.e-5 and \
                   p.atoms[i].atomic_number==1 and \
                   p.atoms[j].atomic_number==1:
                    umask[j]=i+1
        
        


                    
    q0=0
    fh = file("mmresp.inp","w")
    fh.write("%i\n\n"%(len(atoms)))
    for i in atoms:
        a = p.atoms[i]
        n = a.atomic_number
        if i in disappearing:
            n = -n
        fh.write("%3i %14.8f %14.8f %14.8f   %14.8f  %3i\n"%(n,a.xx,a.xy,a.xz,a.charge,umask[i]))
        q0 += a.charge
    
    fh.close()
    subprocess.call("mmresp mmresp.inp > mmresp.out",shell=True)
    fh = file("mmresp.out","r")
    q=[]
    for line in fh:
        line = line.strip()
        if len(line) > 0:
            q.append( float(line) )
    fh.close()
    q1 = sum(q)
    if len(q) != len(atoms):
        print "Length of fit charges != number of atoms in timask"
        exit(1)


    print """
function set_deq_charges()
{
  local sele="$1"
  cat <<EOF"""
    for ii,i in enumerate(atoms):
        a = p.atoms[i]
        print "set ${sele}.%-5s charge %11.8f"%(a.name,q[ii])
    print """EOF
}
"""
    print "# original qsum = %9.5f  modified qsum = %9.5f"%(q0,q1)


    
