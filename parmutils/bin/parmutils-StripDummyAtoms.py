#!/usr/bin/python

import parmed
import parmutils
import argparse
import os

def StripTraj( param, trajin, trajout, parmfile ):
    import subprocess

    if not os.path.isfile(parmfile):
        print "missing parm7:",parmfile
    elif not os.path.isfile(trajin):
        print "missing nc:",trajin
    else:
        dats = parmutils.FindDummyAtoms(param)
        sele = parmutils.ListToSelection(dats)
        ptrajinp = trajout + ".ptraj.in"
        fh = file( ptrajinp, "w" )
        fh.write("""
parm %s
trajin %s
strip %s
trajout %s
run
quit

"""%( parmfile,trajin,sele,trajout ))
        fh.close()
        subprocess.call("cpptraj -i %s"%(ptrajinp),shell=True)
        os.remove(ptrajinp)

        
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""Writes a new set of parm/rst/nc files that have stripped the dummy atoms
If a trajectory file is given, then only the trajectory file is stripped.
Otherwise, the parm7/rst7 files are stripped.
""" )

    
    parser.add_argument \
        ("-p","--parm",
         help="parm7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("-c","--crd",
         help="restart file",
         type=str,
         required=True )
    
    parser.add_argument \
        ("-x","--nc",
         help="trajectory file",
         type=str,
         required=False )

    parser.add_argument \
        ("-o","--outprefix",
         help="output file prefix",
         type=str,
         default="stripped",
         required=False )

    args = parser.parse_args()
    

    parmfile = args.parm
    crd7file = args.crd
    ncfile   = args.nc
    prefix   = args.outprefix

    
    parmobj = parmutils.OpenParm( parmfile, xyz=crd7file )

    out = "%s_%s"%( prefix, os.path.basename(parmfile) )
    

    if ncfile is not None:
        trajout = out.replace(".parm7","") + ".nc"
        StripTraj( parmobj, ncfile, trajout, parmfile )
    else:
        parmutils.SaveParmWithoutDummyAtoms( parmobj, out )

