#!/usr/bin/python
import parmed
import sys

if len(sys.argv) < 3:
    raise Exception("GetAtomDifferences.py parm1.parm7 parm2.parm7")

#index = int(sys.argv[2])

param1 = parmed.load_file(sys.argv[1])
param2 = parmed.load_file(sys.argv[2])
#mask = parmed.amber.mask.AmberMask( param, sys.argv[2] )
#atoms = mask.Selected()
#for at in atoms:
for a1,a2 in zip(param1.atoms,param2.atoms):
    res1  = a1.residue
    res2  = a2.residue
    ok=True
    if (
            res1.name != res2.name or
            res1.number != res2.number or
            a1.name != a2.name or
            a1.charge != a2.charge or
            a1.rmin != a2.rmin or
            a1.epsilon != a2.epsilon or
            a1.type != a2.type
            ):
        print "%6i %4s%5i:%4s  [%-4s]   Q=%9.6f   Rmin/2=%9.6f   Eps=%9.6f"%(a1.idx+1,res1.name,res1.number+1,a1.name,a1.type,a1.charge,a1.rmin,a1.epsilon)
        print "%6i %4s%5i:%4s  [%-4s]   Q=%9.6f   Rmin/2=%9.6f   Eps=%9.6f"%(a2.idx+1,res2.name,res2.number+1,a2.name,a2.type,a2.charge,a2.rmin,a2.epsilon)
        print ""
