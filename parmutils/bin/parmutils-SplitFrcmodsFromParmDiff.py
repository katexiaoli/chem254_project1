#!/usr/bin/python


import parmed
import parmutils
import argparse
import subprocess


if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""Given a selection mask and 2 parm files corresponding to
the 2 end-states of a TI calculation, this script generates tleap input that
can be used to interpolate between the two parameter files.
It is the user's responsibility to ensure that the atoms/parameters outside of
the selection are the same.
""" )
    
    parser.add_argument \
        ("-O","--overwrite",
         help="overwrite parm7 file, if present",
         action='store_true',
         default=False,
         required=False )

    parser.add_argument \
        ("--parm0",
         help="lambda=0 parm7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("--parm1",
         help="lambda=1 parm7 file",
         type=str,
         required=True )
    
    parser.add_argument \
        ("-c","--rst",
         help="rst7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("-m","--mask",
         help="amber-mask selection string",
         type=str,
         required=True )


    args = parser.parse_args()
    
    parm0 = args.parm0
    parm1 = args.parm1
    rst   = args.rst
    mask  = args.mask

    
if True:
    
#    parm0="ts_O_no-Mg.parm7"
#    parm1="ts_S_no-Mg.parm7"
#    rst="ts_O_no-Mg.rst7"
#    mask="@1637,1671,1696,1697,1698,1699"

    base0 = parm0.replace(".parm7","")
    base1 = parm1.replace(".parm7","")
    
    print "Reading %s and %s"%(parm0,rst)
    param0 = parmed.load_file(parm0,xyz=rst)
    
    print "Reading %s and %s"%(parm1,rst)
    param1 = parmed.load_file(parm1,xyz=rst)

    
    aidxs  = parmutils.GetSelectedAtomIndices(param0,mask)
    if len(aidxs) < 1:
        raise Exception("No selected atoms found from mask '%s'"%(args.mask))

    parmutils.MakeAtomTypesCommon( param0, param1, aidxs )
    
    print "Writing %s.notsele.frcmod and %s.sele.frcmod"%(base0,base0)
    parmutils.WriteMaskedFrcmod(param0,aidxs,"%s.notsele.frcmod"%(base0),"%s.sele.frcmod"%(base0))

    print "Writing %s.lib"%(base0)
    parmed.tools.writeOFF(param0, "%s.lib"%(base0)).execute()

    print "Writing %s.pdb"%(base0)
    parmed.tools.writeCoordinates(param0, "%s.pdb"%(base0)).execute()

    
    print "Writing %s.notsele.frcmod and %s.sele.frcmod"%(base1,base1)
    parmutils.WriteMaskedFrcmod(param1,aidxs,"%s.notsele.frcmod"%(base1),"%s.sele.frcmod"%(base1))
    
    print "Writing %s.lib"%(base1)
    parmed.tools.writeOFF(param1, "%s.lib"%(base1)).execute()
    
    print "Writing %s.pdb"%(base1)
    parmed.tools.writeCoordinates(param1, "%s.pdb"%(base1)).execute()

    fname = "leap.sh"
    print "Writing %s"%(fname)
    fh = file(fname,"w")
    fh.write("#!/bin/bash\n")
    fh.write("echo \"\'diff %s.notsele.frcmod %s.notsele.frcmod\' should yield no output:\"\n"%(base0,base1))
    fh.write("diff %s.notsele.frcmod %s.notsele.frcmod\n\n\n"%(base0,base1))

    parmutils.WriteLeapSh \
        (fname,
         param0,
         ["%s.lib"%(base0)],
         ["%s.notsele.frcmod"%(base0),"%s.sele.frcmod"%(base0)],
         "%s.pdb"%(base0),
         base0,
         fh=fh,
         overwrite=args.overwrite)
    parmutils.WriteLeapSh \
        (fname,
         param0,
         ["%s.lib"%(base1)],
         ["%s.notsele.frcmod"%(base0),"%s.sele.frcmod"%(base1)],
         "%s.pdb"%(base0),
         base1,
         fh=fh,
         overwrite=args.overwrite)
    fh.close()
    
    print "Running tleap script %s"%(fname)
    subprocess.call("bash %s"%(fname),shell=True)



#     fh = file("releap.sh","w")
#     fh.write("""
# cat <<EOF > releap0.cmds
# loadOff mod0.lib
# loadAmberParams native0.frcmod 
# loadAmberParams changed0.frcmod
# x = loadPdb mod0.pdb
# """)
#     if param0.box is not None:
#        fh.write("setbox x centers")
#     fh.write("""
# saveAmberParm x lam0.parm7 lam0.rst7
# quit
# EOF

# cat <<EOF > releap1.cmds
# loadOff mod1.lib
# loadAmberParams native0.frcmod 
# loadAmberParams changed1.frcmod
# x = loadPdb mod0.pdb
# """)
#     if param0.box is not None:
#        fh.write("setbox x centers")
#     fh.write("""
# saveAmberParm x lam1.parm7 lam1.rst7
# quit
# EOF

# tleap -s -f releap0.cmds | grep -v "+---" | grep -v "+Currently" > releap0.out
# tleap -s -f releap1.cmds | grep -v "+---" | grep -v "+Currently" > releap1.out
# """)
#     if param0.box is not None:
#        fh.write("""
# ChBox -X %.12f -Y %.12f -Z %.12f -al %.12f -bt %.12f -gm %.12f -c lam0.rst7 -o tlam0.rst7; mv tlam0.rst7 lam0.rst7
# ChBox -X %.12f -Y %.12f -Z %.12f -al %.12f -bt %.12f -gm %.12f -c lam1.rst7 -o tlam1.rst7; mv tlam1.rst7 lam1.rst7
# """%(param0.box[0],param0.box[1],param0.box[2],param0.box[3],param0.box[4],param0.box[5],
#      param0.box[0],param0.box[1],param0.box[2],param0.box[3],param0.box[4],param0.box[5]))

#        if abs(param0.box[-1]-109.471219000000) < 1.e-4:
#           fh.write("""
# # Reset the ifbox flag in the parm7 files
# for f in lam0.parm7 lam1.parm7; do
#     sed -i 's|0       0       1|0       0       2|' $f
# done

# """)
  

    
