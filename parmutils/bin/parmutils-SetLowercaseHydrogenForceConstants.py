#!/usr/bin/python

import parmed
import parmutils
import argparse



def SetHForceConstants(parmobj,outparm):
    from collections import defaultdict as ddict
    seen = ddict( bool )
    changed_something=False
    for bond in parmobj.bonds:
        a1 = bond.atom1
        a2 = bond.atom2
        if a1.type[0] == "h" or a2.type[0] == "h":
            aa = (a1.type,a2.type)
            bb = (a2.type,a1.type)
            if aa in seen or bb in seen:
                continue
                #pass
            else:
                seen[aa] = True
                seen[bb] = True
            print "%4s - %4s [%-2s-%-2s] req: %6.4f k: %8.4f (now %8.4f)"%\
                (a1.name,a2.name,a1.type,a2.type,bond.type.req,bond.type.k,800)
            bond.type.k = 800.
            changed_something = True
    if changed_something:
        parmutils.SaveParm( parmobj, outparm )
    else:
        print "WARNING: %s not written because there were no valid hydrogens found"%(outparm)
        
        


if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""Resets the bond force constant of all bonds involving atom
type 'h*'; i.e., any type whose first character is a lower-case h.
This is only useful when performing MM -> MM' TI calculations, where the MM shaked
the bonds.  In which case, one mimics the shake-to-no_shake by setting those force
constants to a large number (800 kcal/mol/A^2)
""" )
    
    parser.add_argument \
        ("-p","--parm",
         help="parm7 file",
         type=str,
         required=True )

    
    parser.add_argument \
        ("-c","--crd",
         help="input restart file, needed only to determine unit cell info",
         type=str,
         required=True )
    
    parser.add_argument \
        ("-o","--oparm",
         help="output parameter file",
         type=str,
         required=True )

    args = parser.parse_args()
    

    parmfile = args.parm
    crd7file = args.crd
    outparm  = args.oparm
    
    parmobj = parmutils.OpenParm( parmfile, xyz=crd7file )
    SetHForceConstants(parmobj,outparm)

    
