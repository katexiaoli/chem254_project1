#!/usr/bin/env python2.7

import parmed
import sys

def ReadG09(fname):
    fh = file(fname,"r")
    zs=[]
    crds=[]
    for line in fh:
        if "Standard orientation:" in line:
            zs=[]
            crds=[]
            fh.next()
            fh.next()
            fh.next()
            fh.next()
            for line in fh:
                if "----" in line:
                    break
                cols = line.strip().split()
                atn = int(cols[1])
                x = float(cols[-3])
                y = float(cols[-2])
                z = float(cols[-1])
                zs.append(atn)
                crds.append(x)
                crds.append(y)
                crds.append(z)
    return zs,crds


zs,crds = ReadG09( sys.argv[1] )
pdb = parmed.load_file( sys.argv[2] )

if len(pdb.atoms) != len(zs):
    raise Exception("# atoms in g09 (%i) differs from pdb (%i)"%(len(zs),len(pdb.atoms)))

for i,a in enumerate(pdb.atoms):
    if a.atomic_number != zs[i]:
        print "REMARK atomic number of atom %i in pdb (%i) differs from g09 (%i)"%( i+1, a.atomic_number, zs[i] )
    a.xx = crds[0+i*3]
    a.xy = crds[1+i*3]
    a.xz = crds[2+i*3]

parmed.write_PDB( pdb, sys.stdout )
