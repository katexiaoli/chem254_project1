#!/usr/bin/python

import parmed
import parmutils
import argparse



#fnames = [ "SS0/trial.frcmod.10", "SW0/trial.frcmod.10","WW0/trial.frcmod.10", "smallAX/trial.frcmod.10","SS1/trial.frcmod.10", "SW1/trial.frcmod.10","WW1/trial.frcmod.10", "smallAH/trial.frcmod.10" ]

def AvgFrcmods(ofile,fnames):

    fmods = [ parmed.amber.AmberParameterSet(f) for f in fnames ]

    anames = list(set().union( *[ [ x for x in fmod.atom_types ]
                                  for fmod in fmods ] ))
    for aname in anames:
        v=0
        rmin=0
        eps=0
        n=0
        dval = None
        for fmod in fmods:
            if aname in fmod.atom_types:
                dval = fmod.atom_types[aname]
                rmin += dval.rmin
                eps += dval.epsilon
                v += dval.mass
                n += 1
        if aname not in fmods[0].atom_types:
            print "inserting %s atom_types"%(aname)
            fmods[0].atom_types[aname] = dval
        fmods[0].atom_types[aname].mass = v/n
        fmods[0].atom_types[aname].rmin = rmin/n
        fmods[0].atom_types[aname].epsilon = eps/n

    anames = list(set().union( *[ [ x for x in fmod.bond_types ]
                                  for fmod in fmods ] ))
    for aname in anames:
        eq=0
        k=0
        n=0
        dval = None
        for fmod in fmods:
            if aname in fmod.bond_types:
                dval = fmod.bond_types[aname]
                k += dval.k
                eq += dval.req
                n += 1
        if aname not in fmods[0].bond_types:
            print "inserting %s bond_types"%(str(aname))
            fmods[0].bond_types[aname] = dval
        fmods[0].bond_types[aname].k = k/n
        fmods[0].bond_types[aname].req = eq/n

    anames = list(set().union( *[ [ x for x in fmod.angle_types ]
                                  for fmod in fmods ] ))
    for aname in anames:
        #print aname
        eq=0
        k=0
        n=0
        dval = None
        for fmod in fmods:
            if aname in fmod.angle_types:
                dval = fmod.angle_types[aname]
                k += dval.k
                eq += dval.theteq
                n += 1
        if aname not in fmods[0].angle_types:
            print "inserting %s angle_types"%(str(aname))
            fmods[0].angle_types[aname] = dval
        fmods[0].angle_types[aname].k = k/n
        fmods[0].angle_types[aname].theteq = eq/n


    anames = list(set().union( *[ [ x for x in fmod.dihedral_types ]
                                  for fmod in fmods ] ))
    for aname in anames:
        #print aname
        n=0
        dval = None
        typ = None
        for fmod in fmods:
            if aname in fmod.dihedral_types:
                dval = fmod.dihedral_types[aname]
                if aname not in fmods[0].dihedral_types:
                    print "inserting %s dihedral_types"%(str(aname))
                    fmods[0].dihedral_types[aname] = dval

    anames = list(set().union( *[ [ x for x in fmod.improper_periodic_types ]
                                  for fmod in fmods ] ))
    for aname in anames:
        #print aname
        n=0
        dval = None
        typ = None
        for fmod in fmods:
            if aname in fmod.improper_periodic_types:
                dval = fmod.improper_periodic_types[aname]
                if aname not in fmods[0].improper_periodic_types:
                    print "inserting %s improper_periodic_types"%(str(aname))
                    fmods[0].improper_periodic_types[aname] = dval


    
    

    parmutils.WriteFrcmodObj(fmods[0],ofile,uniqueparams=True)




if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""Averages the mass, bond, angle, and lennard-jones parameters
found in the frcmod files. The dihedral parameters are not averaged."
""" )
    
    parser.add_argument \
        ("-o","--out",
         help="frcmod file containing the average parameters",
         type=str,
         required=True )

    parser.add_argument \
        ('fnames',
         metavar='frcmod_files',
         type=str,
         nargs='+',
         help='list of frcmod files to average')

    args = parser.parse_args()
    
    AvgFrcmods(args.out,args.fnames)

    
