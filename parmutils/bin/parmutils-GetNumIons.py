#!/usr/bin/python
import sys
import parmed

if len(sys.argv) < 2:
    print """
GetNumIons.py parm [conc]
    parm    amber parm7 filename (string)
    conc    ion concentration, M (float, optional, default: 0.14)

This script reports:
(1) the number of waters within the parm file
(2) the current counterion (Na/Cl) concentration
(3) the current net charge
(4) the net charge excluding Na/Cl counterions

The script provides the addions tleap-command that should be
used to add enough Na/Cl to neutralize the system and achieve
an excess counterion concentration of 0.14 M, or the
concentration specified by the optional argument.
"""
    exit(0)

WatConc = 55.  # M
IonConc = 0.14 # M
if len(sys.argv) > 2:
    IonConc = float( sys.argv[2] )



parm = parmed.load_file(sys.argv[1])
NumWatParm  = 0
Charge  = 0.
NonionCharge = 0.
NaParm = 0
ClParm = 0
for atom in parm:
    Charge += atom.charge
    NonionCharge += atom.charge
    if atom.type == "OW" and atom.residue.name == "WAT":
        NumWatParm += 1
    elif atom.type == "Na+" or atom.type == "K+":
        NaParm += 1
        NonionCharge -= 1
    elif atom.type == "Cl-":
        ClParm += 1
        NonionCharge += 1
ChargeFloat = Charge
NonionChargeFloat = NonionCharge
Charge = int( "%.0f"%(Charge) )
NonionCharge = int( "%.0f"%(NonionCharge) )

Na = ( (NumWatParm+NaParm+ClParm) * IonConc / WatConc) - ( NonionCharge if NonionCharge < 0 else 0 ) - NaParm
Cl = ( (NumWatParm+NaParm+ClParm) * IonConc / WatConc) + ( NonionCharge if NonionCharge > 0 else 0 ) - ClParm

Na = int( "%.0f"%(Na) )
Cl = int( "%.0f"%(Cl) )

ParmConc = 0.
if NumWatParm > 0:
   ParmConc = min(NaParm,ClParm) * WatConc / (NumWatParm + NaParm + ClParm)
print "Current ion concentration: %.3f M"%(ParmConc)
print "Current non-counterion charge: %i %.6f"%(NonionCharge,NonionChargeFloat)
print "Current # WAT: ",NumWatParm
print "Current charge: %i %.6f"%(Charge,ChargeFloat)
if Na < 0 or Cl < 0:
    print "ERROR: You already have too many counterions; don't add more"
    if Na < 0:
        print "ERROR: Remove %i sodium"%(-Na)
    if Cl < 0:
        print "ERROR: Remove %i chlorine"%(-Cl)
else:
    print "tleap cmd: addionsrand mol Na+ %i Cl- %i 6.0"%(Na,Cl)

