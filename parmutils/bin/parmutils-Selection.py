#!/usr/bin/python
import parmed
import parmutils
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""Converts an arbitrary amber mask to an atom selection mask
""" )
    
    parser.add_argument \
        ("-p","--parm",
         help="parm7 file",
         type=str,
         required=True )


    parser.add_argument \
        ("-w","--whole-molecule",
         help="if present, then selecting any portion of a molecule selects the whole molecule",
         action='store_true' )

    
    parser.add_argument \
        ("-r","--byresidue",
         help="if present, then return a list of matching residues",
         action='store_true' )


    parser.add_argument \
        ("-n","--norange",
         help="if present, then list each match individually -- as opposed to using dashes to represent ranges",
         action='store_true' )

    
    parser.add_argument \
        ("-c","--crd",
         help="input restart file",
         type=str,
         default=None,
         required=False )
    
    parser.add_argument \
        ("-m","--mask",
         help="amber mask selection of atoms to polarize",
         type=str,
         required=True )
    
    args = parser.parse_args()
    

    parmfile = args.parm
    crd7file = args.crd
    maskstr  = args.mask
    maskstr = maskstr.replace('\\',"").replace('\\',"").replace('\\',"").replace('\\',"").replace('\\',"")

    if crd7file is not None:
        try:
            parm = parmutils.OpenParm( parmfile, xyz=crd7file )
        except:
            parm = parmutils.OpenParm( parmfile )
    else:
        parm = parmutils.OpenParm( parmfile )


        
    atoms = parmutils.GetSelectedAtomIndices( parm, maskstr )

    if args.whole_molecule:
       smols=[]
       iat = 0
       for imol,mnat in enumerate(parm.parm_data["ATOMS_PER_MOLECULE"]):
           for i in range(mnat):
               if iat in atoms:
                  smols.append( imol )
               iat += 1
       smols=list(set(smols))
       smols.sort()
       atoms = []
       iat=0
       for imol,mnat in enumerate(parm.parm_data["ATOMS_PER_MOLECULE"]):
           for i in range(mnat):
               if imol in smols:
                  atoms.append(iat)
               iat += 1 

    res = []
    for a in atoms:
        res.append( parm.atoms[a].residue.idx )
    res = list(set(res))
    res.sort()

    if args.byresidue:
        if args.norange:
            s = ":%s"%( ",".join( ["%i"%(x+1) for x in res ] ) )
        else:
            s = parmutils.ListToSelection( res )
            s = s.replace("@",":")
    else:
        if args.norange:
            s = "@%s"%( ",".join( ["%i"%(x+1) for x in atoms ] ) )
        else:
            s = parmutils.ListToSelection( atoms )
            
    print s

    
