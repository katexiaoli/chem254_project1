#!/usr/bin/python

import parmed
import parmutils
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""Write lib, pdb, and 2 frcmod files from a parm7.
The names of the residues from the mask selection will become lowercase,
and the atom-types of the selection will become lowercase.
The base.sele.frcmod file will contain any parameter that touches a selected atom.
The base.notsele.frcmod will contain all other parameters.
""" )
    

    parser.add_argument \
        ("-p","--parm",
         help="parm7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("-c","--rst",
         help="rst7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("--timask1",
         help="amber-mask selection string",
         type=str,
         required=True )

    parser.add_argument \
        ("--timask2",
         help="amber-mask selection string",
         type=str,
         required=True )

    parser.add_argument \
        ("-o","--base",
         help="basename of output files (defaults to parm7-file's basename)",
         type=str,
         default=None,
         required=False )

    args = parser.parse_args()

    timask1  = args.timask1
    timask2  = args.timask2
    parmfile = args.parm
    rstfile  = args.rst
    if args.base is not None:
        base = args.base
    else:
        base = parmfile.replace(".parm7","")
        if base == parmfile:
            base = "base"
    

parm = parmutils.OpenParm( parmfile, xyz=rstfile )
parmutils.tisplit( parm, timask1, timask2, base=base )
