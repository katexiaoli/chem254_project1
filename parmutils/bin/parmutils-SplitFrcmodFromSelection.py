#!/usr/bin/python

import parmed
import parmutils
import argparse
import subprocess


if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""Write lib, pdb, and 2 frcmod files from a parm7.
The names of the residues from the mask selection will become lowercase,
and the atom-types of the selection will become lowercase.
The base.sele.frcmod file will contain any parameter that touches a selected atom.
The base.notsele.frcmod will contain all other parameters.
""" )
    
    parser.add_argument \
        ("-O","--overwrite",
         help="overwrite parm7 file, if present",
         action='store_true',
         default=False,
         required=False )

    parser.add_argument \
        ("-p","--parm",
         help="parm7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("-c","--rst",
         help="rst7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("-m","--mask",
         help="amber-mask selection string",
         type=str,
         required=True )

    parser.add_argument \
        ("-o","--base",
         help="basename of output files (defaults to parm7-file's basename)",
         type=str,
         default=None,
         required=False )

    args = parser.parse_args()

    parmfile = args.parm
    rstfile  = args.rst
    if args.base is not None:
        base = args.base
    else:
        base = parmfile.replace(".parm7","")
        if base == parmfile:
            base = "base"
    

if True:
    # WRITE NATIVE/CHANGED FRCMOD
    #parmfile="GHAH.parm7"
    #rstfile="GHAHnovel.rst7"
    #maskstr=":1"
    print "Reading %s and %s"%(parmfile,rstfile)
    param = parmed.load_file(parmfile,xyz=rstfile)
    
    aidxs = parmutils.GetSelectedAtomIndices(param,args.mask)
    if len(aidxs) < 1:
        raise Exception("No selected atoms found from mask '%s'"%(args.mask))
        
    print "Writing %s.notsele.frcmod and %s.sele.frcmod"%(base,base)
    parmutils.WriteMaskedFrcmod(param,aidxs,"%s.notsele.frcmod"%(base),"%s.sele.frcmod"%(base))
    
    print "Writing %s.pdb"%(base)
    parmed.tools.writeCoordinates(param, "%s.pdb"%(base)).execute()
    
    print "Writing %s.lib"%(base)
    parmed.tools.writeOFF(param, "%s.lib"%(base)).execute()

    print "Writing %s.sh"%(base)
    parmutils.WriteLeapSh \
        ("%s.sh"%(base),
         param,
         ["%s.lib"%(base)],
         ["%s.notsele.frcmod"%(base),"%s.sele.frcmod"%(base)],
         "%s.pdb"%(base),
         base,
         overwrite=args.overwrite)
    
    print "Running tleap script %s.sh"%(base)
    subprocess.call("bash %s.sh"%(base),shell=True)

