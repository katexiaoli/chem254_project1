#!/usr/bin/python



import parmed
import parmutils
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""Updates bond and angle parameters to reproduce the
bond and angle distributions from a reference trajectory file.
""" )
    
    parser.add_argument \
        ("-p","--parm",
         help="parm7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("--reftraj",
         help="reference trajectory file",
         type=str,
         required=True )

    parser.add_argument \
        ("--trialtraj",
         help="trial trajectory file",
         type=str,
         required=True )

    parser.add_argument \
        ("--trialfrcmod",
         help="trial frcmod file",
         type=str,
         required=True )

    parser.add_argument \
        ("--newfrcmod",
         help="new frcmod file",
         type=str,
         required=True )

    args = parser.parse_args()
    





if True:
    # REWRITE FRCMOD WITH NEW PARAMS
    parmutils.UpdateFrcmod(args.parm,args.reftraj,args.trialtraj,args.trialfrcmod,args.newfrcmod)
