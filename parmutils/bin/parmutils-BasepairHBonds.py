#!/usr/bin/env python2.7

def GetBasepairHBonds(p,tol=2.3):

    from collections import defaultdict as ddict
    import numpy as np

    donor_atoms = []
    accep_atoms = []
    for a in p.atoms:
        # ignore waters and ions
        if len(a.residue.atoms) < 5:
            continue
        # ignore sugar and phosphate
        elif "'" in a.name or "P" in a.name or "OP" in a.name:
            continue
        # possible donor if H and not bonded to a C
        if a.atomic_number == 1 and abs(a.charge) > 0.0001:
            bonds_to_carbon = False
            if 6 not in [ b.atomic_number for b in a.bond_partners ] :
                donor_atoms.append(a)
                #print "donor %4i:%-4s"%(a.residue.idx,a.name)
        # possible acceptor if heavy atom, not C, and not bonded to an H
        elif a.atomic_number != 6 and a.atomic_number > 4:
            bonds_to_hydrogen = False
            if 1 not in [ b.atomic_number for b in a.bond_partners ]:
                accep_atoms.append(a)
            else:
                qasum = 0
                for b in a.bond_partners:
                    if b.atomic_number == 1:
                        qasum += abs( b.charge )
                if qasum < 0.0001:
                    accep_atoms.append(a)
                #print "accep %4i:%-4s"%(a.residue.idx,a.name)

    donor_crds = np.array( [ [a.xx,a.xy,a.xz] for a in donor_atoms ] )
    accep_crds = np.array( [ [a.xx,a.xy,a.xz] for a in accep_atoms ] )
    ndonor = len(donor_atoms)
    naccep = len(accep_atoms)
    pairs = []
    for idonor in xrange(ndonor):
        d = donor_atoms[idonor]
        for iaccep in xrange(naccep):
            r = np.linalg.norm( donor_crds[idonor,:]-accep_crds[iaccep,:] )
            if r < tol:
                a = accep_atoms[iaccep]
                if a.residue.idx != d.residue.idx:
                    pairs.append( (a.idx,d.idx) )
    return pairs


if __name__ == "__main__":


    import parmed
    import parmutils
    import argparse
    import sys
    import numpy as np

    
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""
Find all nucleobase basepair H-bonds
""" )
    

    parser.add_argument \
        ("-p","--parm",
         help="parm7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("-c","--rst",
         help="rst7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("--exclude",
         help="amber-mask selection string. Atoms matching this selection will be excluded from the list of H-bonds",
         type=str,
         default="",
         required=False )

    parser.add_argument \
        ("--tol",
         help="if r < tol, then it is a h-bond (default: 2.3)",
         type=float,
         default=2.3,
         required=False )

    parser.add_argument \
        ("--rk",
         help="force constant (kcal/mol/A^2) used in NMR bond restraints",
         type=float,
         default=0,
         required=False )

    parser.add_argument \
        ("--angpen",
         help="penalty (kcal/mol) for bending an angle by 90 degrees",
         type=float,
         default=0,
         required=False )

    parser.add_argument \
        ("--r",
         help="upper-bound to the half-harmonic (default: 2.1)",
         type=float,
         default=2.1,
         required=False )


    args = parser.parse_args()

    parmfile = args.parm
    rstfile  = args.rst
    exclmask = args.exclude
    tol      = args.tol
    rk       = args.rk
    angpen   = args.angpen
    r        = args.r
    

    parm = parmutils.OpenParm( parmfile, xyz=rstfile )
    pairs = GetBasepairHBonds( parm, tol=tol )

    if len(exclmask) > 0:
        xs = parmutils.GetSelectedAtomIndices( parm, exclmask )
        tmp=[]
        for a,d in pairs:
            if not (a in xs or d in xs):
                tmp.append( (a,d) )
        pairs = tmp

    
    if rk > 0:
        fh = sys.stdout
        from collections import defaultdict as ddict
        rrp = ddict(lambda:ddict(list))
        for accep,donor in pairs:
            a = parm.atoms[accep]
            d = parm.atoms[donor]
            if a.residue.idx < d.residue.idx:
                rrp[a.residue.idx][d.residue.idx].append( (accep,donor) )
            else:
                rrp[d.residue.idx][a.residue.idx].append( (donor,accep) )

        angk = angpen/( (np.pi*0.5) * (np.pi*0.5) )
                
        for ra in sorted(rrp):
            for rd in sorted(rrp[ra]):
                pp = rrp[ra][rd]
                for a1,a2 in pp:
                    a = parm.atoms[a1]
                    d = parm.atoms[a2]
                    fh.write("# (%s) :%i@%s - (%s) :%i@%s\n"%
                             ( a.residue.name, a.residue.idx+1, a.name,
                               d.residue.name, d.residue.idx+1, d.name ))
                    fh.write("&rst iat = %i, %i, r1=-999,r2=0, r3=%.2f, r4=999, rk2=0, rk3=%.2f /\n"%( a.idx+1, d.idx+1, r, rk ) )
                    
                if angpen > 0:
                    for a1,a2 in pp:
                        a = parm.atoms[a1]
                        d = parm.atoms[a2]
                        if a.atomic_number == 1:
                            x = a.bond_partners[0]
                            h = a
                            o = d
                        else:
                            x = d.bond_partners[0]
                            h = d
                            o = a
                        
                        fh.write("# (%s) :%i@%s - (%s) :%i@%s - (%s) :%i@%s\n"%
                                 ( x.residue.name, x.residue.idx+1, x.name,
                                   h.residue.name, h.residue.idx+1, h.name,
                                   o.residue.name, o.residue.idx+1, o.name ))
                        fh.write("&rst iat = %i, %i, %i, r1=-999, r2=180, r3=180, r4=999, rk2=%.12f, rk3=%.12f /\n"%( x.idx+1, h.idx+1, o.idx+1, angk, angk ) )

                fh.write("\n")

        
    else:
        for accep,donor in pairs:
            print "label add Bonds 0/%i 0/%i"%(accep,donor)
        if angpen > 0:
            for accep,donor in pairs:
                d = parm.atoms[donor]
                x = d.bond_partners[0]
                print "label add Angles 0/%i 0/%i 0/%i"%(accep,donor,x.idx)
