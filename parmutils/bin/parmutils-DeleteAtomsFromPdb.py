#!/usr/bin/env python2.7

import sys
import parmed
import parmutils

pdb = parmed.load_file( sys.argv[1] )
pdb.strip( sys.argv[2] )
parmed.write_PDB( pdb, sys.stdout )
