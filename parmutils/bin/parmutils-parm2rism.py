#!/usr/bin/env python2.7

import parmed
import parmutils
import argparse



def WriteRismMdl( parm, maskstr, mdlfile ):
    import math
    sele = parmutils.GetSelectedAtomIndices( parm, maskstr )
    mdl = parmed.amber.AmberFormat()
    mdl.charge_flag="CHG"
    mdl.version = "VERSION_STAMP = V0001.000"
    mdl.add_flag('TITLE','20a4',data=parm.parm_data['TITLE'])

    usele = []
    msele = []
    for iat in sele:
        if len(usele) == 0:
            usele.append( iat )
            msele.append( 1 )
        else:
            p = parm.atoms[ usele[-1] ]
            a = parm.atoms[ iat ]
            if a.charge == p.charge and a.nb_idx == p.nb_idx:
                msele[-1] += 1
            else:
                usele.append( iat )
                msele.append( 1 )
                
    mdl.add_flag('POINTERS','10I8',data=[len(sele),len(usele)])

    uatoms = [ parm.atoms[iat] for iat in usele ]
    
    umap = {}
    utype = []
    for a in uatoms:
        if a.nb_idx in umap:
            utype.append( umap[ a.nb_idx ] )
        else:
            t = len(umap) + 1
            utype.append( t )
            umap[ a.nb_idx ] = t
            
    mdl.add_flag('ATMTYP','10I8',data=utype)
    mdl.add_flag('ATMNAME','20A4',data=[ a.name for a in uatoms ])
    mdl.add_flag('MASS','5e16.8',data=[ a.mass for a in uatoms ])
    mdl.add_flag('CHG','5e16.8',data=[ a.charge for a in uatoms ])

    eps = [ a.epsilon for a in uatoms ]
    for i in range(len(eps)):
        if eps[i] == 0:
            iat = usele[i]
            a = parm.atoms[iat]
            raise Exception("The epsilon value of atom %i (:%s@%s) is zero. The CoincidentRadius calculation has not been implemented, so we are aborting"%(a.idx+1,a.residue.name,a.name))
    sig = [ 0.5 * a.sigma * pow(2,1./6.) for a in uatoms ]
    for i in range(len(sig)):
        if sig[i] == 0:
            iat = usele[i]
            a = parm.atoms[iat]
            raise Exception("The rmin/2 value of atom %i (:%s@%s) is zero. The CoincidentRadius calculation has not been implemented, so we are aborting"%(a.idx+1,a.residue.name,a.name))
            
    
    mdl.add_flag('LJEPSILON','5e16.8',data=eps)
    # This is really rmin/2, but they call it "sigma" for an unknown reason
    mdl.add_flag('LJSIGMA','5e16.8',data=sig)
    mdl.add_flag('MULTI','10I8',data=msele)

    crds=[]
    for iat in sele:
        a = parm.atoms[iat]
        crds.extend( [ a.xx, a.xy, a.xz ] )

    mdl.add_flag('COORD','5e16.8',data=crds)

    mdl.write_parm( mdlfile )


    



if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""Write lib, pdb, and 2 frcmod files from a parm7.
The names of the residues from the mask selection will become lowercase,
and the atom-types of the selection will become lowercase.
The base.sele.frcmod file will contain any parameter that touches a selected atom.
The base.notsele.frcmod will contain all other parameters.
""" )
    

    parser.add_argument \
        ("-p","--parm",
         help="parm7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("-c","--rst",
         help="rst7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("-m","--mask",
         help="amber-mask selection string",
         type=str,
         required=True )

    parser.add_argument \
        ("-o","--mdl",
         help="mdl output file",
         type=str,
         required=True )

    args = parser.parse_args()
    
    parmfile = args.parm
    crd7file = args.rst
    maskstr  = args.mask
    mdlfile  = args.mdl

    if mdlfile == parmfile:
        raise Exception("mdl file cannot overwrite the parm7 file")
    
    parm = parmutils.OpenParm( parmfile, xyz=crd7file )

    WriteRismMdl( parm, maskstr, mdlfile )

    
    
