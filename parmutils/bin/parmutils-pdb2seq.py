#!/usr/bin/env python2.7

import parmed
import sys

pdb = parmed.load_file( sys.argv[1] )
seq = " ".join( [ res.name for res in pdb.residues ] )
print "mol = loadPdbUsingSeq %s { %s }"%(sys.argv[1],seq)

