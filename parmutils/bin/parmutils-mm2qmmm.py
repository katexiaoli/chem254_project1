#!/usr/bin/env python2.7

import os
import argparse
from parmutils import MM2QMMM

if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""
Converts MM parameter/restart files to versions appropriate for QM/MM. 
It removes connection atom charges, and redistributes MM charges so
that the QM region within each residue is an integer. It removes
M-sites from QM waters and removes dummy particles used in pKa
calculations. It adds LJ parameters to QM atoms that lack LJ
parameters in the MM representation.
""" )
    
    parser.add_argument \
        ("-p","--parm",
         help="MM parameter file",
         type=str,
         required=True )

    parser.add_argument \
        ("-c","--crd",
         help="MM restart file",
         type=str,
         required=True )
    
    parser.add_argument \
        ("-q","--qmcharge",
         help="QM charge",
         type=int,
         required=True )

    parser.add_argument \
        ("-m","--qmmask",
         help="QM mask based on the MM parameter file representation",
         type=str,
         required=True )

    parser.add_argument \
        ("-o","--oparm",
         help="QMMM output parameter file",
         type=str,
         required=True )

    parser.add_argument \
        ("-r","--orst",
         help="QMMM output restart file",
         type=str,
         required=True )

    parser.add_argument \
        ("-i","--mdin",
         help="QMMM output mdin file",
         type=str,
         required=True )

    parser.add_argument \
        ("-a","--am1",
         help="if present, write mdin for AM1/d-PhoT",
         action='store_true',
         default=False,
         required=False )

    parser.add_argument \
        ("-f","--frcmod",
         help="(optional) frcmod file containing LJ parameters for the QM atoms",
         type=str,
         required=False )

    parser.add_argument \
        ("-g","--group",
         help="(optional) one-or-more atom selection masks that group residues as a single residue when determining charge redistribution",
         nargs='*',
         required=False)
    

    parser.add_argument \
        ("-d","--dlfind",
         help="(optional) if >= 0, then use dlfind to optimize the qm region and all MM residues within X angstroms of the qm region",
         type=float,
         default=-1.,
         required=False )

    parser.add_argument \
        ("-R","--hmr",
         help="(optional) if present, then repartition H-masses in the QM region and set the timestep to 2 fs",
         action='store_true',
         required=False )

    parser.add_argument \
        ("-M","--hmass",
         help="(optional) Sets the target H-mass when using --hmr (default: 3.024)",
         type=float,
         default=3.024,
         required=False )

    
    args = parser.parse_args()

    
    p = MM2QMMM.MM2QMMM( args.parm, args.crd )
    p.set_qm( args.qmmask, args.qmcharge, charge_groups=args.group )

    frcmod=None
    if args.frcmod is not None:
        if len(args.frcmod) > 0:
            if os.path.exists(args.frcmod):
                frcmod=args.frcmod
            else:
                raise Exception("frcmod file does not exist: %s")
            
    p.new_qm_nbtypes( frcmod=frcmod, make_nonzero=True )
    
    qmmask = p.mmmask_to_qmmask( args.qmmask )

    dt = 0.001
    if args.hmr:
        dt = 0.002
        p.repartition_qm_hmass( args.hmass )
    
    p.save_qmparm( args.oparm, args.orst )


    basename = os.path.splitext( os.path.split( args.mdin )[-1] )[0]
    imin=0
    ntmin=1
    if args.dlfind >= 0:
        imin=1
        ntmin=5
    
    fh = open( args.mdin, "w" )
    fh.write("""title
&cntrl
! IO =======================================
      irest = 0       ! 0 = start, 1 = restart
        ntx = 1       ! 1 = start, 5 = restart
       ntxo = 1       ! read/write rst as formatted file
      iwrap = 1       ! wrap crds to unit cell
     ioutfm = 1       ! write mdcrd as netcdf
       imin = %i
      ntmin = %i
       ntpr = 50
       ntwr = 50
       ntwx = 50
       ntwf = 0
! DYNAMICS =================================
     nstlim = 10000   ! number of time steps
         dt = %.3f   ! ps/step
        ntb = 1       ! 1=NVT periodic, 2=NPT periodic, 0=no box
! TEMPERATURE ==============================
      temp0 = 298     ! target temp
   gamma_ln = 5.0     ! Langevin collision freq
        ntt = 3       ! thermostat (3=Langevin)
! PRESSURE  ================================
        ntp = 0       ! 0=no scaling, 1=isotropic, 2=anisotropic
! SHAKE ====================================
        ntc = 2       ! 1=no shake, 2=HX constrained, 3=all constrained
noshakemask = "%s"    ! do not shake these
        ntf = 1       ! 1=cpt all bond E, 2=ignore HX bond E, 3=ignore all bond E
! MISC =====================================
        cut = 10.0
      ifqnt = 1
         ig = -1
     nmropt = 1
/

&wt
type='DUMPFREQ', istep1=25
&end
&wt
  type='END',
&end
DISANG=TEMPLATE.disang
DUMPAVE=TEMPLATE.dumpave

&ewald
  dsum_tol = 1.e-6
/
"""%(imin,ntmin,dt,qmmask))

    if args.am1:
        fh.write("""
&qmmm
    qm_theory   = 'AM1/d'
        qmmask  = '%s'
      qmcharge  = %i
          spin  = 1
       qmshake  = 0
      qm_ewald  = 1
   qmmm_switch  = 1
       scfconv  = 1.e-10
     verbosity  = 0
  tight_p_conv  = 1
  diag_routine  = 0
   pseudo_diag  = 1
  dftb_maxiter  = 100
/

"""%(qmmask,args.qmcharge))
        
    else:
        
        fh.write("""
&qmmm
     qm_theory  = 'HFDF'
   hfdf_theory  = 'PBE0'
    hfdf_basis  = '6-31G*'
        qmmask  = '%s'
      qmcharge  = %i
          spin  = 1
        itrmax  = 50
       qmshake  = 0
       scfconv  = 1e-07
     verbosity  = 0
hfdf_mempercore = 2000
hfdf_restricted = T   
!hfdf_guess_mix = 0.5
!-------------------------------------------------------
! Ambient-Potential Composite Ewald
hfdf_ewald=T,qm_ewald=1
hfdf_mm_percent=0.0, hfdf_qmmm_wswitch=0.0, qmmm_switch=0
!-------------------------------------------------------
!
! Cutoff QM/MM electrostatics; set use_pme=0 in &ewald
!hfdf_ewald=F,qm_ewald=0
!hfdf_mm_percent=1.0,hfdf_qmmm_wswitch=0.0,qmmm_switch=0
!
! MM-charge Ewald w/ short-range cutoff QM/MM correction
!hfdf_ewald=F,qm_ewald=1
!hfdf_mm_percent=1.0,hfdf_qmmm_wswitch=0.0,qmmm_switch=1
!
! MM-charge Ewald w/ short-range switched-cutoff QM/MM correction
!hfdf_ewald=F,qm_ewald=1
!hfdf_mm_percent=1.0,hfdf_qmmm_wswitch=2.0,qmmm_switch=1
!
! Nam-style Mulliken-charge PME w/ short-range cutoff QM/MM correction
!hfdf_ewald=F,qm_ewald=1
!hfdf_mm_percent=0.0,hfdf_qmmm_wswitch=0.0,qmmm_switch=1
!
! Nam-style Mulliken-charge PME w/ switched QM/MM correction
!hfdf_ewald=F,qm_ewald=1
!hfdf_mm_percent=0.0,hfdf_qmmm_wswitch=2.0,qmmm_switch=1
!
/

"""%(qmmask,args.qmcharge))


if imin > 0:

    ats = MM2QMMM.GetSelectedAtomIndices( p.qmparm, "(%s)<:%.2f"%(qmmask,args.dlfind) )
    active = MM2QMMM.AtomListToSelection( p.qmparm, ats )
    
    fh.write("""

&dlfind
        active = "%s"
        prefix = '%s'
        crdrep = 'HDLC'
        optalg = 'LBFGS'
       hessupd = 'BFGS'
      trustrad = 'GRAD'
      lbfgsmem = -1
      maxcycle = 1200
        maxene = 2400
           tol = 1e-06
          tole = 0.0001
       maxstep = -1.0
       minstep = 1e-08
     scalestep = 0.5
     maxupdate = -1
         dimer = ''
   dimer_delta = 0.01
  dimer_maxrot = 25
dimer_mmpercent = 0.0
  dimer_tolrot = 2.0
         nhess = -1
         ihess = -1
       hessdel = 0.005
       hessini = 'IDENTITY'
       hessout = ''
           neb = ''
        wtmask = ''
/

"""%(active,basename))

