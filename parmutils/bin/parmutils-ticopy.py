#!/usr/bin/env python2.7

import parmed
import parmutils
import argparse


def GetResSeq( parm ):
    rtc=parmed.modeller.residue.ResidueTemplateContainer.from_structure( parm )
    return [r.name for r in rtc]



def ticopy( parm, molmask, base="tisplit" ):
    import copy
    import os
    import subprocess

    fh = file("%s.sh"%(base),"w")
    fh.write("""#!/bin/bash

cat << 'EOF' > %s.sh.cmds

"""%(base))
    
    parmed.tools.writeOFF( parm, "%s.lib"%(base) ).execute()
    parmutils.WriteFrcmod( parm, "%s.frcmod"%(base), uniqueparams=False )

    fh.write("loadOff %s.lib\n"%(base))
    fh.write("loadAmberParams %s.frcmod\n"%(base))

    
    p = parmutils.CopyParm( parm )
    p.strip( "!(%s)"%( molmask ) )
    parmed.tools.writeCoordinates(p, "%s.0.pdb"%(base)).execute()
    parmed.tools.writeCoordinates(p, "%s.1.pdb"%(base)).execute()

    seq = GetResSeq( p )
    fh.write("mol0 = loadPdbUsingSeq %s.0.pdb { %s }\n"%(base," ".join(seq)))
    fh.write("mol1 = loadPdbUsingSeq %s.1.pdb { %s }\n"%(base," ".join(seq)))

    p = parmutils.CopyParm( parm )
    p.strip( "(%s)|:WAT"%( molmask ) )
    nonwat_len = len(p.atoms)
    if nonwat_len > 0:
        parmed.tools.writeCoordinates(p, "%s.nonwater.pdb"%(base)).execute()
        seq = GetResSeq( p )
        fh.write("nonwater = loadPdbUsingSeq %s.nonwater.pdb { %s }\n"%(base," ".join(seq)))
        
    p = parmutils.CopyParm( parm )
    p.strip( "(%s)|(!:WAT)"%( molmask ) )
    wat_len = len(p.atoms)
    if wat_len > 0:
        parmed.tools.writeCoordinates(p, "%s.solvent.pdb"%(base)).execute()
        fh.write("solvent = loadPdb %s.solvent.pdb\n"%(base))
        
    fh.write("x = combine { mol0 mol1")
    if nonwat_len > 0:
        fh.write(" nonwater")
    if wat_len > 0:
        fh.write(" solvent")
    fh.write(" }\n")

    fh.write("""
setbox x centers
saveAmberParm x %s.parm7 %s.rst7
quit
EOF

tleap -s -f %s.sh.cmds | grep -v "+---" | grep -v "+Currently" > %s.sh.out
cat %s.sh.out

"""%(base,base,base,base,base))

    if parm.box is not None:
        fh.write("""
# Set the box dimensions
ChBox -X %.12f -Y %.12f -Z %.12f -al %.12f -bt %.12f -gm %.12f -c %s.rst7 -o %s.rst7.tmp; mv %s.rst7.tmp %s.rst7
"""%(parm.box[0],parm.box[1],parm.box[2],parm.box[3],parm.box[4],parm.box[5],base,base,base,base))

        if abs(parm.box[-1]-109.471219000000) < 1.e-4:
            fh.write("""
# Reset the ifbox flag
sed -i 's|0       0       1|0       0       2|' %s.parm7

"""%(base))

    


    
if __name__ == "__main__":
    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""
Write a leapsh script will insert two copies of a molecule.
""" )
    

    parser.add_argument \
        ("-p","--parm",
         help="parm7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("-c","--rst",
         help="rst7 file",
         type=str,
         required=True )

    parser.add_argument \
        ("--molmask",
         help="amber-mask selection string",
         type=str,
         required=True )

    parser.add_argument \
        ("-o","--base",
         help="basename of output files (defaults to parm7-file's basename)",
         type=str,
         default=None,
         required=False )

    args = parser.parse_args()

    molmask  = args.molmask
    parmfile = args.parm
    rstfile  = args.rst
    if args.base is not None:
        base = args.base
    else:
        base = parmfile.replace(".parm7","")
        if base == parmfile:
            base = "base"
    

parm = parmutils.OpenParm( parmfile, xyz=rstfile )
ticopy( parm, molmask, base=base )
