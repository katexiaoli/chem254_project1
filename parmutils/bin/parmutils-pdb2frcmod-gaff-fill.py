#!/usr/bin/env python2.7

def pdb_to_mol2_atomtypes(pdb,mol2,attype):
    import subprocess as subp
    import os
    import glob
    if ".pdb" in pdb:
        args = "antechamber -i %s -fi pdb -o %s -fo mol2 -at %s -pf -an n"%(pdb,mol2,attype)
    elif ".mol2" in pdb:
        args = "antechamber -i %s -fi mol2 -o %s -fo mol2 -at %s -pf -an n"%(pdb,mol2,attype)

    subp.call(args,shell=True)
    for f in glob.glob("ANTECH*"):
        os.remove(f)
    for f in glob.glob("ATOMT*"):
        os.remove(f)

def pdb_to_mol2_gaff(pdb,mol2):
    pdb_to_mol2_atomtypes(pdb,mol2,"gaff")

def pdb_to_mol2_amber(pdb,mol2):
    pdb_to_mol2_atomtypes(pdb,mol2,"amber")


def mol2_to_parm_gaff(mol2):
    import subprocess as subp
    import parmutils
    base = mol2.replace(".mol2","")
    fh = file( "%s.tleap.in"%(base), "w" )
    fh.write("""
source leaprc.gaff
%s = loadmol2 %s
saveamberparm %s %s.parm7 %s.rst7
quit
"""%(base,mol2,base,base,base))
    fh.close()
    args = "tleap -s -f %s.tleap.in"%(base)
    subp.call(args,shell=True)
    return parmutils.OpenParm( "%s.parm7"%(base), xyz="%s.rst7"%(base) )


def mol2_to_missing_params(mol2):
    import subprocess as subp
    from subprocess import Popen, PIPE
    from collections import defaultdict as ddict
    
    base = mol2.replace(".mol2","")
    fh = file( "%s.tleap.in"%(base), "w" )
    fh.write("""
source leaprc.protein.ff14SB
source leaprc.water.tip4pew
source leaprc.constph
set default PBradii mbondi3
%s = loadmol2 %s
saveamberparm %s %s.parm7 %s.rst7
quit
"""%(base,mol2,base,base,base))
    fh.close()
    args = "tleap -s -f %s.tleap.in"%(base)
    #subp.call(args,shell=True)
    proc = Popen( args.split(), stdout=PIPE, stderr=PIPE )
    stdout,stderr = proc.communicate()
    proc.wait()

    missing = ddict( list )
    for i,line in enumerate(stdout.split("\n")):
        if "Could not find bond parameter for:" in line:
            cs = line.strip().split()
            a = (cs[-3],cs[-1])
            b = (cs[-1],cs[-3])
            if not (a in missing["bonds"] or b in missing["bonds"]):
                missing["bonds"].append( a )
        elif "Could not find angle parameter:" in line:
            cs = line.strip().split()
            a = (cs[-5],cs[-3],cs[-1])
            b = (cs[-1],cs[-3],cs[-5])
            if not (a in missing["angles"] or b in missing["angles"]):
                missing["angles"].append( a )
        elif " ** No torsion terms for" in line:
            cs = line.replace(" ** No torsion terms for","").strip().split("-")
            a = (cs[0],cs[1],cs[2],cs[3])
            b = (cs[3],cs[2],cs[1],cs[0])
            if not (a in missing["torsions"] or b in missing["torsions"]):
                missing["torsions"].append( a )
    return missing
    

if __name__ == "__main__":
    import parmed
    import parmutils
    from collections import defaultdict as ddict
    import glob
    import os
    import sys
    import argparse
    import shutil


    parser = argparse.ArgumentParser \
    ( formatter_class=argparse.RawDescriptionHelpFormatter,
      description="""Reads a mol2 or pdb file and writes a mol2 and frcmod file.
The new mol2 file resets the AMBER atom-types by calling antechamber.
The frcmod file contains GAFF parameters for any missing parameter values.
If --mask is set, then the output mol2 file will only contain the atoms within
the mask. Typically, the input mol2 file will contain extra residues on the
head and tail of the middle residue, so that antechamber properly assigns
the atom-types, but one only wants a final mol2 file containing the middle
residue.
""" )
    parser.add_argument('filename',
                        metavar='filename',
                        type=str,
                        nargs=1,
                        help='input mol2 (or pdb) file')


    
    parser.add_argument \
        ("-n","--noreassign",
         help="if present, do not reassign amber atom-types (requires mol2 as input)",
         action='store_true',
         required=False )
    
    
    parser.add_argument \
        ("-m","--mask",
         help="amber mask selecting the atoms to keep in the output mol2 file",
         type=str,
         default='',
         required=False )
    
    args = parser.parse_args()

        
    pdb = args.filename[0]
    if not os.path.exists(pdb):
        raise Exception("File not found '%s'"%(pdb))
    

    if ".pdb" in pdb:
        base = pdb.replace(".pdb","")
    elif ".mol2" in pdb:
        base = pdb.replace(".mol2","")

    gaff_mol2  = base + ".gaff.mol2"
    amber_mol2 = base + ".amber.mol2"
    pdb_to_mol2_gaff(pdb,gaff_mol2)
    if args.noreassign:
        if ".mol2" in pdb:
            shutil.copy( pdb, amber_mol2 )
        else:
            raise Exception("Option '-n' can only be used with mol2 files")
    else:
        pdb_to_mol2_amber( pdb, amber_mol2 )
        
    gaff = mol2_to_parm_gaff(gaff_mol2)
    missing = mol2_to_missing_params(amber_mol2)
    for key in missing:
        print key
        for x in missing[key]:
            print x
        print ""

    amber = parmed.load_file( amber_mol2, structure=True )
    for a in gaff.atoms:
        a.type = amber.atoms[a.idx].type

    for x in gaff.bonds:
        x.used=False
        a = (x.atom1.type,x.atom2.type)
        b = (x.atom2.type,x.atom1.type)
        if a in missing["bonds"] or b in missing["bonds"]:
            x.used=True
    gaff.bonds.prune_unused()

    for x in gaff.angles:
        x.used=False
        a = (x.atom1.type,x.atom2.type,x.atom3.type)
        b = (x.atom3.type,x.atom2.type,x.atom1.type)
        if a in missing["angles"] or b in missing["angles"]:
            x.used=True
    gaff.angles.prune_unused()

        
    for x in gaff.dihedrals:
        x.used=False
        a = (x.atom1.type,x.atom2.type,x.atom3.type,x.atom4.type)
        b = (x.atom4.type,x.atom3.type,x.atom2.type,x.atom1.type)
        if a in missing["torsions"] or b in missing["torsions"]:
            x.used=True
    gaff.dihedrals.prune_unused()

    parmutils.WriteFrcmod( gaff, "%s.frcmod"%(base),
                           uniqueparams=False,
                           with_mass=False,
                           with_nonb=False )

    for f in glob.glob("%s.gaff.*"%(base)):
        os.remove(f)

    oname = amber_mol2.replace(".amber.mol2",".mol2")
    if os.path.exists(oname):
        os.remove(oname)

    if len(args.mask) > 0:
        amber.strip( "!(%s)"%(args.mask) )
    amber.save( oname )

    #os.rename(amber_mol2,oname)
