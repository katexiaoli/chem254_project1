#!/bin/bash
set -e
set -u

source ${HOME}/devel/git/DataBase/CustomResidues/leap/bashrc

function RunRespCalcs
{
    local mol2="$1"
    local base=${mol2%.mol2}
    if [ ! -e ${base}.log ]; then
	parmutils-mol22g09.py --run ${base}.mol2
    fi
    for alp in 0 30 60 90 120 150 180; do
	for bet in 0 30 60 90; do
	    f=$(printf "%s_%.2f_%.2f_%.2f.log" ${base} ${alp} ${bet} 0)
	    if [ ! -e ${f} ]; then
		parmutils-rotate-g09-resp.py --alp ${alp} --bet ${bet} --run ${base}.log
	    fi
	done
    done
}



function add_dna_OH
{
    local selection="$1"
    cat <<EOF

H = createAtom "H" "H*"   0.3929705
O = createAtom "O" "OH"  -0.7008705

add ${selection} O
bond ${selection}.P ${selection}.O
select ${selection}.O
relax ${selection}
deselect ${selection}.O

add ${selection} H
bond ${selection}.O ${selection}.H
select ${selection}.H
relax ${selection}
deselect ${selection}.H

select ${selection}.O
select ${selection}.H
relax ${selection}
deselect ${selection}.O
deselect ${selection}.H

EOF
    
}


function add_rna_OH
{
    local selection="$1"
    cat <<EOF

H = createAtom "H" "H*"   0.3928705
O = createAtom "O" "OH"  -0.7009705

add ${selection} O
bond ${selection}.P ${selection}.O
select ${selection}.O
relax ${selection}
deselect ${selection}.O

add ${selection} H
bond ${selection}.O ${selection}.H
select ${selection}.H
relax ${selection}
deselect ${selection}.H

select ${selection}.O
select ${selection}.H
relax ${selection}
deselect ${selection}.O
deselect ${selection}.H

EOF
    
}




function extract_dna_nucleobase
{
    local selection="$1"
    cat <<EOF
remove ${selection} ${selection}.P
remove ${selection} ${selection}.OP1
remove ${selection} ${selection}.OP2
remove ${selection} ${selection}.O5'
remove ${selection} ${selection}.C5'
remove ${selection} ${selection}.H5'
remove ${selection} ${selection}.H5''
remove ${selection} ${selection}.C4'
remove ${selection} ${selection}.H4'
remove ${selection} ${selection}.O4'
remove ${selection} ${selection}.C1'
remove ${selection} ${selection}.H1'
remove ${selection} ${selection}.C3'
remove ${selection} ${selection}.H3'
remove ${selection} ${selection}.C2'
remove ${selection} ${selection}.H2'
remove ${selection} ${selection}.O3'
##########################
# DNA
remove ${selection} ${selection}.H2''
# RNA
#remove ${selection} ${selection}.O2'
#remove ${selection} ${selection}.HO2'
##########################
EOF
}


function extract_rna_nucleobase
{
    local selection="$1"
    cat <<EOF
remove ${selection} ${selection}.P
remove ${selection} ${selection}.OP1
remove ${selection} ${selection}.OP2
remove ${selection} ${selection}.O5'
remove ${selection} ${selection}.C5'
remove ${selection} ${selection}.H5'
remove ${selection} ${selection}.H5''
remove ${selection} ${selection}.C4'
remove ${selection} ${selection}.H4'
remove ${selection} ${selection}.O4'
remove ${selection} ${selection}.C1'
remove ${selection} ${selection}.H1'
remove ${selection} ${selection}.C3'
remove ${selection} ${selection}.H3'
remove ${selection} ${selection}.C2'
remove ${selection} ${selection}.H2'
remove ${selection} ${selection}.O3'
##########################
# DNA
#remove ${selection} ${selection}.H2''
# RNA
remove ${selection} ${selection}.O2'
remove ${selection} ${selection}.HO2'
##########################
EOF
}


function methylate_DC_N1
{
    local selection="$1"
cat <<EOF
C1 = createAtom "C1" "CA" -0.266021
add ${selection} C1
bond ${selection}.N1 ${selection}.C1
select ${selection}.C1
relax ${selection}
deselect ${selection}.C1

H11 = createAtom "H11" "HA" 0.109707
add ${selection} H11
bond ${selection}.C1 ${selection}.H11
select ${selection}.H11
relax ${selection}
deselect ${selection}.H11

H12 = createAtom "H12" "HA" 0.109707
add ${selection} H12
bond ${selection}.C1 ${selection}.H12
select ${selection}.H12
relax ${selection}
deselect ${selection}.H12

H13 = createAtom "H13" "HA" 0.109707
add ${selection} H13
bond ${selection}.C1 ${selection}.H13
select ${selection}.H13
relax ${selection}
deselect ${selection}.H13

select ${selection}.H11
select ${selection}.H12
select ${selection}.H13
relax ${selection}
deselect ${selection}.H11
deselect ${selection}.H12
deselect ${selection}.H13
EOF
}




function methylate_C_N1
{
    local selection="$1"
cat <<EOF
C1 = createAtom "C1" "CA" -0.189079
add ${selection} C1
bond ${selection}.N1 ${selection}.C1
select ${selection}.C1
relax ${selection}
deselect ${selection}.C1

H11 = createAtom "H11" "HA" 0.094193
add ${selection} H11
bond ${selection}.C1 ${selection}.H11
select ${selection}.H11
relax ${selection}
deselect ${selection}.H11

H12 = createAtom "H12" "HA" 0.094193
add ${selection} H12
bond ${selection}.C1 ${selection}.H12
select ${selection}.H12
relax ${selection}
deselect ${selection}.H12

H13 = createAtom "H13" "HA" 0.094193
add ${selection} H13
bond ${selection}.C1 ${selection}.H13
select ${selection}.H13
relax ${selection}
deselect ${selection}.H13


select ${selection}.H11
select ${selection}.H12
select ${selection}.H13
relax ${selection}
deselect ${selection}.H11
deselect ${selection}.H12
deselect ${selection}.H13
EOF
}






###################################################
# DNA adenine nucleobase (methylated @ N1)
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
DC = sequence { DC }
set DC   name "DC"
set DC.1 name "DC"
EOF

extract_dna_nucleobase "DC.1" >> tleap.in
methylate_DC_N1 "DC.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 DC DC.mol2 1
#saveamberparm DC foo.parm7 foo.rst7
quit
EOF

tleap -s -f tleap.in


#
# make DC.log from DC.mol2
#
RunRespCalcs DC.mol2

	       



###################################################
# RNA adenine nucleobase (methylated @ N1)
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
C = sequence { C }
set C name "C"
set C.1 name "C"
EOF

extract_rna_nucleobase "C.1" >> tleap.in
methylate_C_N1 "C.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 C C.mol2 1
#saveamberparm C foo.parm7 foo.rst7
quit
EOF

tleap -s -f tleap.in

#
# make C.log from C.mol2
#
RunRespCalcs C.mol2






###################################################
# protonated-N3 DNA adenine nucleobase
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
DCF = sequence { DC }
set DCF name "DCF"
set DCF.1 name "DCF"

H3 = createAtom "H3" "HD" 1.0
add DCF.1 H3
bond DCF.1.N3 DCF.1.H3
select DCF.1.H3
relax DCF
deselect DCF.1.H3
EOF

extract_dna_nucleobase "DCF.1" >> tleap.in

#
# The mol2 format chops charges at 0.0001, I make a parm7 file
# for the sole purpose of extracting the native MM charges
#
cat <<EOF >> tleap.in
set DCF.1.H3 charge 0
saveamberparm DCF DCF_base.parm7 DCF_base.rst7
set DCF.1.H3 charge 1
EOF

methylate_DC_N1 "DCF.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 DCF DCF.mol2 1
quit
EOF

tleap -s -f tleap.in


#
# make DCF.log from DCF.mol2
#

mol2=DCF.mol2
base=${mol2%.mol2}
if [ ! -e ${base}.log ]; then
    parmutils-mol22g09.py --run ${base}.mol2
fi
for alp in 0 30 60 90 120 150 180; do
    for bet in 0 30 60 90; do
	f=$(printf "%s_%.2f_%.2f_%.2f.log" ${base} ${alp} ${bet} 0)
	if [ ! -e ${f} ]; then
	    parmutils-rotate-g09-resp.py --alp ${alp} --bet ${bet} --run ${base}.log
	fi
    done
done
	



###################################################
# protonated-N3 RNA adenine nucleobase
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
CF = sequence { C }
set CF name "CF"
set CF.1 name "CF"

H3 = createAtom "H3" "HD" 1.0
add CF.1 H3
bond CF.1.N3 CF.1.H3
select CF.1.H3
relax CF
deselect CF.1.H3
EOF

extract_rna_nucleobase "CF.1" >> tleap.in

#
# The mol2 format chops charges at 0.0001, I make a parm7 file
# for the sole purpose of extracting the native MM charges
#
cat <<EOF >> tleap.in
set CF.1.H3 charge 0
saveamberparm CF CF_base.parm7 CF_base.rst7
set CF.1.H3 charge 1
EOF

methylate_C_N1 "CF.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 CF CF.mol2 1
quit
EOF

tleap -s -f tleap.in


#
# make CF.log from CF.mol2
#
RunRespCalcs CF.mol2




##################################################
# CHARGE FITS
##################################################


parmutils-GetAtomChargeCmds.py  CF_base.parm7 ':1' | sed 's|FOO|CF_native|'   > charge_fit_functions.sh
parmutils-GetAtomChargeCmds.py DCF_base.parm7 ':1' | sed 's|FOO|DCF_native|' >> charge_fit_functions.sh

rm -f CF_base*
rm -f DCF_base*

cat <<'EOF' | python2.7 | sed 's|_1|_protN3|' >> charge_fit_functions.sh
import parmutils
import parmutils.respfit as rf

def GetLogFiles(mol2):
    import glob
    base=mol2.replace(".mol2","")
    return glob.glob("%s_*.log"%(base))

if __name__ == "__main__":
    comp = parmutils.BASH( 12 )
    
    ###############################################################
    # RNA
    ###############################################################

    native = rf.ResidueResp( comp, 1 )
    native.add_state( "C", "C.mol2", GetLogFiles("C.mol2") )
    #  ** AUXILIARY CAP **
    native.states[-1].add_group_restraint( "@C1,H11,H12,H13" )
    #  ** DELETED ATOMS **
    # native.states[-1].add_group_restraint( "" )

    native.multimolecule_fit(True) 
    native.perform_fit("@*",unique_residues=False)
    native.preserve_residue_charges_by_shifting()
    #native.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #native.print_resp()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    model = rf.ResidueResp( comp, 1 )
    model.add_state( "CF", "CF.mol2", GetLogFiles("CF.mol2") )
    #  ** AUXILIARY CAP **
    model.states[-1].add_group_restraint( "@C1,H11,H12,H13" )

    model.multimolecule_fit(True) 
    model.perform_fit("@*",unique_residues=False)
    model.preserve_residue_charges_by_shifting()
    #model.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #model.print_resp()
    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    rf.PrintPerturbedCharges(native,model,extra=["H3"],exclude=["C1","H11","H12","H13"])

    ###############################################################
    # DNA
    ###############################################################

    native = rf.ResidueResp( comp, 1 )
    native.add_state( "DC", "DC.mol2", GetLogFiles("DC.mol2") )
    #  ** AUXILIARY CAP **
    native.states[-1].add_group_restraint( "@C1,H11,H12,H13" )
    #  ** DELETED ATOMS **
    # native.states[-1].add_group_restraint( "" )

    native.multimolecule_fit(True) 
    native.perform_fit("@*",unique_residues=False)
    native.preserve_residue_charges_by_shifting()
    #native.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #native.print_resp()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    model = rf.ResidueResp( comp, 1 )
    model.add_state( "DCF", "DCF.mol2", GetLogFiles("DCF.mol2") )
    #  ** AUXILIARY CAP **
    model.states[-1].add_group_restraint( "@C1,H11,H12,H13" )

    model.multimolecule_fit(True) 
    model.perform_fit("@*",unique_residues=False)
    model.preserve_residue_charges_by_shifting()
    #model.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #model.print_resp()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    rf.PrintPerturbedCharges(native,model,extra=["H3"],exclude=["C1","H11","H12","H13"])

EOF




source charge_fit_functions.sh

start_leaprc > tleap.in
cat <<EOF >> tleap.in

DCF = sequence { DC }

set DCF name "DCF"
set DCF.1 name "DCF"

H3 = createAtom "H3" "HD" 0.0000
add DCF.1 H3
bond DCF.1.N3 DCF.1.H3
select DCF.1.H3
relax DCF
deselect DCF.1.H3

savepdb  DCF DCF.pdb
savemol2 DCF DCF.mol2 1
saveoff  DCF DCF.lib

saveamberparm DCF DCF.parm7 DCF.rst7
EOF
#set_charge_DCF_protN3 "DCF.1" >> tleap.in
cat <<EOF >> tleap.in
#saveamberparm DCF DCF.parm7 DCF.rst7


CF = sequence { C }

set CF   name "CF"
set CF.1 name "CF"

H3 = createAtom "H3" "HD" 0.0000
add CF.1 H3
bond CF.1.N3 CF.1.H3
select CF.1.H3
relax CF
deselect CF.1.H3

savepdb  CF CF.pdb
savemol2 CF CF.mol2 1
saveoff  CF CF.lib


saveamberparm CF CF.parm7 CF.rst7
EOF
#set_charge_CF_protN3 "CF.1" >> tleap.in
cat <<EOF >> tleap.in
#saveamberparm CF CF.parm7 CF.rst7

quit
EOF


tleap -s -f tleap.in

for f in leap.log tleap.in *.com *.chk *.resp.* C.mol2 DC.mol2 CF.rst7 DCF.rst7; do
    if [ -e ${f} ]; then
	rm ${f}
    fi
done


###################################################
###################################################
# Make the reference mononucleotide molecules
###################################################
###################################################

start_leaprc > tleap.in
cat <<EOF >> tleap.in

DCM = sequence { DC }

set DCM name "DCM"
set DCM.1 name "DCM"

EOF

add_dna_OH "DCM.1" >> tleap.in

cat <<EOF >> tleap.in

H3 = createAtom "H3" "HD" 0.0000
add DCM.1 H3
bond DCM.1.N3 DCM.1.H3
select DCM.1.H3
relax DCM
deselect DCM.1.H3

set DCM.1.O3' type "OH"
set DCM.1.O3' pertType "OH"
HO3p = createAtom "HO3'" "HO" 0.307900
add DCM.1 HO3p
bond DCM.1.O3' DCM.1.HO3'
select DCM.1.HO3'
relax DCM
deselect DCM.1.HO3'

set DCM head null
set DCM tail null
set DCM.1 connect0 null
set DCM.1 connect1 null

savepdb  DCM DCM.pdb
savemol2 DCM DCM.mol2 1
saveoff  DCM DCM.lib
saveamberparm DCM DCM.parm7 DCM.rst7




CM = sequence { C }

set CM   name "CM"
set CM.1 name "CM"

EOF

add_rna_OH "CM.1" >> tleap.in

cat <<EOF >> tleap.in

H3 = createAtom "H3" "HD" 0.0000
add CM.1 H3
bond CM.1.N3 CM.1.H3
select CM.1.H3
relax CM
deselect CM.1.H3

set CM.1.O3' type "OH"
set CM.1.O3' pertType "OH"
HO3p = createAtom "HO3'" "HO" 0.308100
add CM.1 HO3p
bond CM.1.O3' CM.1.HO3'
select CM.1.HO3'
relax CM
deselect CM.1.HO3'


set CM head null
set CM tail null
set CM.1 connect0 null
set CM.1 connect1 null

savepdb  CM CM.pdb
savemol2 CM CM.mol2 1
saveoff  CM CM.lib
saveamberparm CM CM.parm7 CM.rst7

quit
EOF

tleap -s -f tleap.in
rm tleap.in
rm leap.log
rm *.parm7

