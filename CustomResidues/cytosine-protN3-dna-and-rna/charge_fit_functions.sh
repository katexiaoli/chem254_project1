
function set_charge_CF_native()
{
    selection="$1"
    cat <<EOF

set ${selection}.N1    charge -0.048400
set ${selection}.C6    charge  0.005300
set ${selection}.H6    charge  0.195800
set ${selection}.C5    charge -0.521500
set ${selection}.H5    charge  0.192800
set ${selection}.C4    charge  0.818500
set ${selection}.N4    charge -0.953000
set ${selection}.H41   charge  0.423400
set ${selection}.H42   charge  0.423400
set ${selection}.N3    charge -0.758400
set ${selection}.C2    charge  0.753800
set ${selection}.O2    charge -0.625200
set ${selection}.H3    charge  0.000000
EOF
}


function set_charge_DCF_native()
{
    selection="$1"
    cat <<EOF

set ${selection}.N1    charge -0.033900
set ${selection}.C6    charge -0.018300
set ${selection}.H6    charge  0.229300
set ${selection}.C5    charge -0.522200
set ${selection}.H5    charge  0.186300
set ${selection}.C4    charge  0.843900
set ${selection}.N4    charge -0.977300
set ${selection}.H41   charge  0.431400
set ${selection}.H42   charge  0.431400
set ${selection}.N3    charge -0.774800
set ${selection}.C2    charge  0.795900
set ${selection}.O2    charge -0.654800
set ${selection}.H3    charge  0.000000
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N1       0.214976    0.263376    0.218329   -0.045047   -0.048400
# 1    C6      -0.022653   -0.027953   -0.064487   -0.036534    0.005300
# 1    H6       0.240499    0.044699    0.237436    0.192737    0.195800
# 1    C5      -0.371804    0.149696   -0.359081   -0.508777   -0.521500
# 1    H5       0.218713    0.025913    0.224362    0.198449    0.192800
# 1    C4       0.554194   -0.264306    0.520107    0.784413    0.818500
# 1    N4      -0.883495    0.069505   -0.841595   -0.911100   -0.953000
# 1    H41      0.481019    0.057619    0.459223    0.401604    0.423400
# 1    H42      0.481019    0.057619    0.459223    0.401604    0.423400
# 1    N3      -0.303040    0.455360   -0.304863   -0.760223   -0.758400
# 1    C2       0.400755   -0.353045    0.460689    0.813734    0.753800
# 1    O2      -0.450730    0.174470   -0.449891   -0.624361   -0.625200
# 1    H3       0.347049    0.347049    0.347049                        
# 1    C1      -0.516223   -0.327123   -0.522440   -0.195317   -0.189100
# 1    H11      0.203241    0.109041    0.205313    0.096272    0.094200
# 1    H12      0.203241    0.109041    0.205313    0.096272    0.094200
# 1    H13      0.203241    0.109041    0.205313    0.096272    0.094200
# 1    SUM      1.000000    1.000000    1.000000   -0.000000    0.000000


function set_charge_CF_protN3
{
   selection="$1"
   cat <<EOF
set ${selection}.N1    charge   0.214976
set ${selection}.C6    charge  -0.022653
set ${selection}.H6    charge   0.240499
set ${selection}.C5    charge  -0.371804
set ${selection}.H5    charge   0.218713
set ${selection}.C4    charge   0.554194
set ${selection}.N4    charge  -0.883495
set ${selection}.H41   charge   0.481019
set ${selection}.H42   charge   0.481019
set ${selection}.N3    charge  -0.303040
set ${selection}.C2    charge   0.400755
set ${selection}.O2    charge  -0.450730
set ${selection}.H3    charge   0.347049
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N1       0.243482    0.277382    0.279727    0.002345   -0.033900
# 1    C6      -0.062289   -0.043989   -0.079829   -0.035840   -0.018300
# 1    H6       0.276417    0.047117    0.238263    0.191146    0.229300
# 1    C5      -0.358959    0.163241   -0.354800   -0.518041   -0.522200
# 1    H5       0.210166    0.023866    0.225775    0.201909    0.186300
# 1    C4       0.566954   -0.276946    0.504218    0.781164    0.843900
# 1    N4      -0.901342    0.075958   -0.831936   -0.907894   -0.977300
# 1    H41      0.487355    0.055955    0.456784    0.400829    0.431400
# 1    H42      0.487355    0.055955    0.456784    0.400829    0.431400
# 1    N3      -0.309987    0.464813   -0.289916   -0.754729   -0.774800
# 1    C2       0.429030   -0.366870    0.427409    0.794279    0.795900
# 1    O2      -0.477593    0.177207   -0.441892   -0.619099   -0.654800
# 1    H3       0.346313    0.346313    0.346313                        
# 1    C1      -0.589160   -0.323160   -0.626373   -0.303213   -0.266000
# 1    H11      0.217420    0.107720    0.229824    0.122104    0.109700
# 1    H12      0.217420    0.107720    0.229824    0.122104    0.109700
# 1    H13      0.217420    0.107720    0.229824    0.122104    0.109700
# 1    SUM      1.000000    1.000000    1.000000   -0.000000    0.000000


function set_charge_DCF_protN3
{
   selection="$1"
   cat <<EOF
set ${selection}.N1    charge   0.243482
set ${selection}.C6    charge  -0.062289
set ${selection}.H6    charge   0.276417
set ${selection}.C5    charge  -0.358959
set ${selection}.H5    charge   0.210166
set ${selection}.C4    charge   0.566954
set ${selection}.N4    charge  -0.901342
set ${selection}.H41   charge   0.487355
set ${selection}.H42   charge   0.487355
set ${selection}.N3    charge  -0.309987
set ${selection}.C2    charge   0.429030
set ${selection}.O2    charge  -0.477593
set ${selection}.H3    charge   0.346313
EOF
}

