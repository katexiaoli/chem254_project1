#!/usr/bin/env python2.7

import os
import sys
import glob
import parmed
import parmutils
import parmutils.respfit as rf

def mmopt(comp,parmfile,rstfile,qmmask=None):
    basename = rstfile.replace(".rst7","")
    
    pre = parmutils.Mdin( base = "mmpre."+basename, inpcrd = rstfile )
    pre.PARM7 = parmfile
    pre.Set_Restart(False)
    pre.Set_NVT(150)
    pre.Set_PrintFreq(10)
    pre.cntrl["imin"]=1
    pre.cntrl["maxcyc"]=500
    pre.cntrl["drms"]=1.e-4
    pre.WriteMdin()
    
    nvt = parmutils.Mdin( base = "mmnvt."+basename, inpcrd = pre.RST7 )
    nvt.PARM7 = parmfile
    nvt.Set_Restart(False)
    nvt.Set_NPT(298)
    nvt.cntrl["barostat"]=1
    nvt.Set_PrintFreq(100000)
    nvt.cntrl["nstlim"]=400000
    nvt.WriteMdin()
    
    pos = parmutils.Mdin( base = "mmpos."+basename, inpcrd = nvt.RST7 )
    pos.PARM7 = parmfile
    pos.Set_Restart(False)
    pos.Set_NVT(150)
    pos.Set_PrintFreq(10)
    pos.cntrl["imin"]=1
    pos.cntrl["maxcyc"]=200
    pos.cntrl["drms"]=1.e-4
    pos.WriteMdin()
    
    opt = parmutils.Mdin( base = "mmopt."+basename, inpcrd = pos.RST7 )
    opt.PARM7 = parmfile
    opt.Set_Restart(False)
    opt.Set_NVT(150)
    opt.Set_PrintFreq(10)
    opt.cntrl["imin"]   = 1  # minimize
    opt.cntrl["ntmin"]  = 5  # read &dlfind
    opt.Set_DLFIND_Minimize()
    if qmmask is not None:
        opt.dlfind["active"] = "'%s'"%(qmmask)
    opt.WriteMdin()
    
    fh = comp.open( "mmopt." + basename + ".slurm" )
    fh.write( "%s %s %s\n"%( comp.mpirun, pre.exe, pre.CmdString() ) )
    fh.write( "%s %s %s\n"%( comp.mpirun, nvt.exe, nvt.CmdString() ) )
    fh.write( """
cat <<'EOF' | cpptraj -p %s -y %s
trajout %s notime keepext offset 1
go
quit
EOF

"""%(parmfile,nvt.NC,rstfile))
    fh.write("for rst in %s.*.rst7; do\n"%(rstfile.replace(".rst7","")))
    pos.SetBaseName( "mmpos.${rst%.rst7}" )
    pos.MDIN = "mmpos."+basename+".mdin"
    pos.CRD7 = "${rst}"
    fh.write( "%s %s %s\n"%( comp.mpirun, pos.exe, pos.CmdString() ) )
    opt.SetBaseName( "mmopt.${rst%.rst7}" )
    opt.MDIN = "mmopt."+basename+".mdin"
    opt.CRD7 = pos.RST7
    fh.write( "%s %s %s\n"%( comp.mpirun, opt.exe, opt.CmdString() ) )
    fh.write("done\n\n")

    
class StateData(object):
    def __init__(self,label,parm,rst,mask):
        self.label=label
        self.parm=parm
        self.rst=rst
        self.mask=mask
        

if __name__ == "__main__":
    #comp = parmutils.BASH(numnodes=12)
    comp = parmutils.ELF(numnodes=1)
    comp.set_exclude("e1c067")
    
    comp.amberhome="${AMBERHOME}"

    bases = [ "A","C","G","U" ]
    if False:
        bases = [ "DA","DG","DT","DC" ]
    
    states = []
    for base in bases:
        for pref in ["1a","2a","3a"]:
            parm= "%s%s.parm7"%(pref,base)
            rst = "%s%s.rst7"%(pref,base)
            states.append( StateData(pref+base,parm,rst,"!:WAT") )
            for f in [rst,parm]:
                if not os.path.exists(f):
                    print "missing",f
                    exit(1)

    for state in states:
        mmopt( comp, state.parm, state.rst, qmmask=state.mask )
  
    
    fit = rf.ResidueResp( comp,1,
                          theory="HF",
                          basis="6-31G*",
                          fitgasphase=True,
                          maxgrad=1.e-3,
                          etol=1.e-2 )

    #
    # If you have a trajectory file, you can break-up the frames
    # into rst7 files and include each into the array below
    #
    
    for state in states:
        rsts = glob.glob( "mmopt.%s.*.rst7"%(state.rst.replace(".rst7","")) )
        if len(rsts) == 0:
            rst = "mmopt.%s"%(state.rst)
            if not os.path.exists(rst):
                raise Exception("no mmopt restart files generated for %s"%(state.rst))
            rsts = [rst]
        state.frames = rsts
        fit.add_state( state.label, state.parm, rsts, qmmask=state.mask )

        

        if True:
            if "3" in state.label:
                fit.states[-1].add_group_restraint( ":1" )
                fit.states[-1].add_group_restraint( ":2" )
                fit.states[-1].add_group_restraint( ":2@P,OP*" )
                fit.states[-1].add_group_restraint( ":3@P,OP*" )
            else:
                fit.states[-1].add_group_restraint( ":1" )
                fit.states[-1].add_group_restraint( ":2@P,OP*" )

    
    
    #
    # This creates a script (mod.1.1a.mmopt.1a.slurm)
    # that will generate mod.1.1a.mmopt.1a.mdout
    #
    # The naming format is:
    #    mod.1.${parm%.parm7}.${rst%.rst7}.slurm
    #
    fit.write_mdin()

    for state in states:
        for frame in state.frames:
            mdout = "mod.1.%s.%s"%(state.parm.replace(".parm7",""),frame.replace(".rst7",".mdout"))
            if not os.path.exists(mdout):
                print "missing",mdout
                exit(1)

            
    if True:

        sys.stdout.write("""
def fitted_charges():
    from collections import defaultdict as ddict

    QQS = ddict( lambda: ddict( lambda: ddict(float) ) )
""")

        #
        # if unique_residues=True and the mask is "@*", then
        # the atomic charges are shared between states for those
        # residues that have the same residue-name and residue-index.
        #
        # if unique_residues=False and the mask is "@*", then
        # the atomic charges are shared between states for all
        # residues with the same residue-name, even if they have
        # different residue-index's.
        #
        # if the mask is "", then the atomic charges are not
        # shared between states at all
        #
        # The mask is useful when doing pKa charge fits, in which
        # case different states will likely have the same residue
        # names, but different charge vectors -- you may still
        # want to average the phosphate and/or sugar charges
        # between states though.
        #
        fit.multimolecule_fit(True) # 1 fit or many?
        fit.perform_fit("@*",unique_residues=False)
        #fit.preserve_residue_charges_by_shifting()
        fit.preserve_mm_charges_by_shifting("@P,OP1,OP2")
        fit.print_resp()

        sys.stdout.write("""    return QQS

""")


#    for s,fit_state in zip(states,fit.states):
#        fit_state.perform_fit(unique_residues=False)
#        fit_state.read_respfile()
#        fit_state.preserve_residue_charges_by_shifting()
#        fit_state.print_resp(prefix=s.label)

