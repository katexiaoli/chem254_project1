#!/bin/bash
set -e
set -u

source ${HOME}/devel/git/DataBase/FrankensteinResidues/leap/bashrc



####################################################################################

if [ -e DE3.lib ]; then
    rm -f DE3.lib
fi

start_leaprc > tleap.in
cat <<EOF >> tleap.in


DE3 = sequence { DC3 }
remove DE3.1 DE3.1.H41
remove DE3.1 DE3.1.H42
remove DE3.1 DE3.1.N4
remove DE3.1 DE3.1.C4
remove DE3.1 DE3.1.N3
remove DE3.1 DE3.1.O2
remove DE3.1 DE3.1.C2
remove DE3.1 DE3.1.H5
remove DE3.1 DE3.1.C5
remove DE3.1 DE3.1.H6
remove DE3.1 DE3.1.C6
remove DE3.1 DE3.1.N1
remove DE3.1 DE3.1.H1'
remove DE3.1 DE3.1.C1'
remove DE3.1 DE3.1.O4'
remove DE3.1 DE3.1.HO3'
remove DE3.1 DE3.1.O3'
remove DE3.1 DE3.1.H3'
remove DE3.1 DE3.1.C3'
remove DE3.1 DE3.1.H2'
remove DE3.1 DE3.1.H2''
remove DE3.1 DE3.1.C2'

at = createAtom "H41'" "H1" 0.1176
add DE3.1 at
bond DE3.1.H41' DE3.1.C4'
select DE3.1.H41'
relax DE3
deselect DE3.1.H41'


at = createAtom "H42'" "H1" 0.1176
add DE3.1 at
bond DE3.1.H42' DE3.1.C4'
select DE3.1.H42'
relax DE3
deselect DE3.1.H42'

select DE3.1.C4'
select DE3.1.H4'
select DE3.1.H41'
select DE3.1.H42'
relax DE3
deselect DE3.1.C4'
deselect DE3.1.H4'
deselect DE3.1.H41'
deselect DE3.1.H42'

set DE3.1.C4'  charge 0.0
set DE3.1.H4'  charge 0.0152333
set DE3.1.H41' charge 0.0152333
set DE3.1.H42' charge 0.0152333

############################################

set DE3   name "DE3"
set DE3.1 name "DE3"
set DE3.1 restype  nucleic
set DE3.1 connect0 DE3.1.P
set DE3   head     DE3.1.P

set DE3.1.P    charge   1.165900 
set DE3.1.OP1  charge  -0.776100 
set DE3.1.OP2  charge  -0.776100 
set DE3.1.O5'  charge  -0.533010
set DE3.1.C5'  charge   0.370392
set DE3.1.H5'  charge  -0.037415
set DE3.1.H5'' charge  -0.037415
set DE3.1.C4'  charge  -0.097430
set DE3.1.H4'  charge   0.009693
set DE3.1.H41' charge   0.009693
set DE3.1.H42' charge   0.009693


savepdb  DE3 DE3.pdb
saveoff  DE3 DE3.lib
savemol2 DE3 DE3.mol2 1
saveamberparm DE3 DE3.parm7 DE3.rst7

quit

EOF

tleap -s -f tleap.in
rm leap.log



####################################################################################

if [ -e E3.lib ]; then
    rm -f E3.lib
fi

start_leaprc > tleap.in
cat <<EOF >> tleap.in


E3 = sequence { C3 }
remove E3.1 E3.1.H41
remove E3.1 E3.1.H42
remove E3.1 E3.1.N4
remove E3.1 E3.1.C4
remove E3.1 E3.1.N3
remove E3.1 E3.1.O2
remove E3.1 E3.1.C2
remove E3.1 E3.1.H5
remove E3.1 E3.1.C5
remove E3.1 E3.1.H6
remove E3.1 E3.1.C6
remove E3.1 E3.1.N1
remove E3.1 E3.1.H1'
remove E3.1 E3.1.C1'
remove E3.1 E3.1.O4'
remove E3.1 E3.1.HO3'
remove E3.1 E3.1.O3'
remove E3.1 E3.1.H3'
remove E3.1 E3.1.C3'
remove E3.1 E3.1.H2'
remove E3.1 E3.1.O2'
remove E3.1 E3.1.HO2'
remove E3.1 E3.1.C2'

at = createAtom "H41'" "H1" 0.1176
add E3.1 at
bond E3.1.H41' E3.1.C4'
select E3.1.H41'
relax E3
deselect E3.1.H41'


at = createAtom "H42'" "H1" 0.1176
add E3.1 at
bond E3.1.H42' E3.1.C4'
select E3.1.H42'
relax E3
deselect E3.1.H42'

select E3.1.C4'
select E3.1.H4'
select E3.1.H41'
select E3.1.H42'
relax E3
deselect E3.1.C4'
deselect E3.1.H4'
deselect E3.1.H41'
deselect E3.1.H42'

set E3.1.C4'  charge 0.0
set E3.1.H4'  charge 0.0152333
set E3.1.H41' charge 0.0152333
set E3.1.H42' charge 0.0152333

############################################

set E3   name "E3"
set E3.1 name "E3"
set E3.1 restype  nucleic
set E3.1 connect0 E3.1.P
set E3   head     E3.1.P

set E3.1.P    charge   1.166200
set E3.1.OP1  charge  -0.776000
set E3.1.OP2  charge  -0.776000
set E3.1.O5'  charge  -0.546662
set E3.1.C5'  charge   0.393554
set E3.1.H5'  charge  -0.044721
set E3.1.H5'' charge  -0.044721
set E3.1.C4'  charge  -0.093954
set E3.1.H4'  charge   0.010135
set E3.1.H41' charge   0.010135
set E3.1.H42' charge   0.010135


savepdb  E3 E3.pdb
saveoff  E3 E3.lib
savemol2 E3 E3.mol2 1
saveamberparm E3 E3.parm7 E3.rst7

quit

EOF

tleap -s -f tleap.in
rm leap.log




####################################################################################


if [ -e DE5.lib ]; then
    rm -f DE5.lib
fi

start_leaprc > tleap.in
cat <<EOF >> tleap.in

DE5 = sequence { DC5 }

remove DE5.1 DE5.1.H41
remove DE5.1 DE5.1.H42
remove DE5.1 DE5.1.N4
remove DE5.1 DE5.1.C4
remove DE5.1 DE5.1.N3
remove DE5.1 DE5.1.O2
remove DE5.1 DE5.1.C2
remove DE5.1 DE5.1.H5
remove DE5.1 DE5.1.C5
remove DE5.1 DE5.1.H6
remove DE5.1 DE5.1.C6
remove DE5.1 DE5.1.N1
remove DE5.1 DE5.1.H1'
remove DE5.1 DE5.1.C1'
remove DE5.1 DE5.1.HO5'
remove DE5.1 DE5.1.O5'
remove DE5.1 DE5.1.H5'
remove DE5.1 DE5.1.H5''
remove DE5.1 DE5.1.C5'
remove DE5.1 DE5.1.H4'
remove DE5.1 DE5.1.C4'
remove DE5.1 DE5.1.H2'
remove DE5.1 DE5.1.H2''
remove DE5.1 DE5.1.O4'
remove DE5.1 DE5.1.H3'

at = createAtom "H31'" "H1" 0.1176
add DE5.1 at
bond DE5.1.H31' DE5.1.C3'
select DE5.1.H31'
relax DE5
deselect DE5.1.H31'

at = createAtom "H32'" "H1" 0.1176
add DE5.1 at
bond DE5.1.H32' DE5.1.C3'
select DE5.1.H32'
relax DE5
deselect DE5.1.H32'

at = createAtom "H21'" "H1" 0.1176
add DE5.1 at
bond DE5.1.H21' DE5.1.C2'
select DE5.1.H21'
relax DE5
deselect DE5.1.H21'

at = createAtom "H22'" "H1" 0.1176
add DE5.1 at
bond DE5.1.H22' DE5.1.C2'
select DE5.1.H22'
relax DE5
deselect DE5.1.H22'

at = createAtom "H23'" "H1" 0.1176
add DE5.1 at
bond DE5.1.H23' DE5.1.C2'
select DE5.1.H23'
relax DE5
deselect DE5.1.H23'


select DE5.1.C3'
select DE5.1.H31'
select DE5.1.H32'
select DE5.1.C2'
select DE5.1.H21'
select DE5.1.H22'
select DE5.1.H23'
relax DE5
deselect DE5.1.C3'
deselect DE5.1.H31'
deselect DE5.1.H32'
deselect DE5.1.C2'
deselect DE5.1.H21'
deselect DE5.1.H22'
deselect DE5.1.H23'

############################################

set DE5   name "DE5"
set DE5.1 name "DE5"
set DE5.1 restype  nucleic
set DE5.1 connect1 DE5.1.O3'
set DE5   tail     DE5.1.O3'

set DE5.1.C3'  charge   0.356495
set DE5.1.C2'  charge  -0.140679
set DE5.1.O3'  charge  -0.528284
set DE5.1.H31' charge  -0.029196
set DE5.1.H32' charge  -0.029196
set DE5.1.H21' charge   0.020986
set DE5.1.H22' charge   0.020986
set DE5.1.H23' charge   0.020986

savepdb  DE5 DE5.pdb
saveoff  DE5 DE5.lib
savemol2 DE5 DE5.mol2 1
saveamberparm DE5 DE5.parm7 DE5.rst7

quit

EOF

tleap -s -f tleap.in
rm leap.log



####################################################################################



if [ -e E5.lib ]; then
    rm -f E5.lib
fi

start_leaprc > tleap.in
cat <<EOF >> tleap.in

E5 = sequence { C5 }

remove E5.1 E5.1.H41
remove E5.1 E5.1.H42
remove E5.1 E5.1.N4
remove E5.1 E5.1.C4
remove E5.1 E5.1.N3
remove E5.1 E5.1.O2
remove E5.1 E5.1.C2
remove E5.1 E5.1.H5
remove E5.1 E5.1.C5
remove E5.1 E5.1.H6
remove E5.1 E5.1.C6
remove E5.1 E5.1.N1
remove E5.1 E5.1.H1'
remove E5.1 E5.1.C1'
remove E5.1 E5.1.HO5'
remove E5.1 E5.1.O5'
remove E5.1 E5.1.H5'
remove E5.1 E5.1.H5''
remove E5.1 E5.1.C5'
remove E5.1 E5.1.H4'
remove E5.1 E5.1.C4'
remove E5.1 E5.1.H2'
remove E5.1 E5.1.HO2'
remove E5.1 E5.1.O2'
remove E5.1 E5.1.O4'
remove E5.1 E5.1.H3'

at = createAtom "H31'" "H1" 0.1176
add E5.1 at
bond E5.1.H31' E5.1.C3'
select E5.1.H31'
relax E5
deselect E5.1.H31'

at = createAtom "H32'" "H1" 0.1176
add E5.1 at
bond E5.1.H32' E5.1.C3'
select E5.1.H32'
relax E5
deselect E5.1.H32'

at = createAtom "H21'" "H1" 0.1176
add E5.1 at
bond E5.1.H21' E5.1.C2'
select E5.1.H21'
relax E5
deselect E5.1.H21'

at = createAtom "H22'" "H1" 0.1176
add E5.1 at
bond E5.1.H22' E5.1.C2'
select E5.1.H22'
relax E5
deselect E5.1.H22'

at = createAtom "H23'" "H1" 0.1176
add E5.1 at
bond E5.1.H23' E5.1.C2'
select E5.1.H23'
relax E5
deselect E5.1.H23'


select E5.1.C3'
select E5.1.H31'
select E5.1.H32'
select E5.1.C2'
select E5.1.H21'
select E5.1.H22'
select E5.1.H23'
relax E5
deselect E5.1.C3'
deselect E5.1.H31'
deselect E5.1.H32'
deselect E5.1.C2'
deselect E5.1.H21'
deselect E5.1.H22'
deselect E5.1.H23'

############################################

set E5   name "E5"
set E5.1 name "E5"
set E5.1 restype  nucleic
set E5.1 connect1 E5.1.O3'
set E5   tail     E5.1.O3'

set E5.1.C3'  charge   0.423683
set E5.1.C2'  charge  -0.127484
set E5.1.O3'  charge  -0.550521
set E5.1.H31' charge  -0.050802
set E5.1.H32' charge  -0.050802
set E5.1.H21' charge   0.015942
set E5.1.H22' charge   0.015942
set E5.1.H23' charge   0.015942

savepdb  E5 E5.pdb
saveoff  E5 E5.lib
savemol2 E5 E5.mol2 1
saveamberparm E5 E5.parm7 E5.rst7

quit

EOF

tleap -s -f tleap.in
rm leap.log


####################################################################################

rm tleap.in
exit

####################################################################################



# -0.3078
start_leaprc > tleap.in
cat <<EOF >> tleap.in

loadoff DE5.lib

mol = sequence { DE5 DC3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 2aDC.parm7 2aDC.rst7

mol = sequence { DE5 DG3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 2aDG.parm7 2aDG.rst7

mol = sequence { DE5 DA3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 2aDA.parm7 2aDA.rst7

mol = sequence { DE5 DT3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 2aDT.parm7 2aDT.rst7



loadoff E5.lib

mol = sequence { E5 C3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 2aC.parm7 2aC.rst7

mol = sequence { E5 G3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 2aG.parm7 2aG.rst7

mol = sequence { E5 A3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 2aA.parm7 2aA.rst7

mol = sequence { E5 U3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 2aU.parm7 2aU.rst7


loadoff DE3.lib

mol = sequence { DC5 DE3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 1aDC.parm7 1aDC.rst7

mol = sequence { DG5 DE3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 1aDG.parm7 1aDG.rst7

mol = sequence { DA5 DE3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 1aDA.parm7 1aDA.rst7

mol = sequence { DT5 DE3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 1aDT.parm7 1aDT.rst7



loadoff E3.lib

mol = sequence { C5 E3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 1aC.parm7 1aC.rst7

mol = sequence { G5 E3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 1aG.parm7 1aG.rst7

mol = sequence { A5 E3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 1aA.parm7 1aA.rst7

mol = sequence { U5 E3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 1aU.parm7 1aU.rst7



mol = sequence { DE5 DC DE3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 3aDC.parm7 3aDC.rst7

mol = sequence { DE5 DG DE3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 3aDG.parm7 3aDG.rst7

mol = sequence { DE5 DA DE3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 3aDA.parm7 3aDA.rst7

mol = sequence { DE5 DT DE3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 3aDT.parm7 3aDT.rst7



mol = sequence { E5 C E3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 3aC.parm7 3aC.rst7

mol = sequence { E5 G E3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 3aG.parm7 3aG.rst7

mol = sequence { E5 A E3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 3aA.parm7 3aA.rst7

mol = sequence { E5 U E3 }
solvateOct mol TIP4PEWBOX 12
saveamberparm mol 3aU.parm7 3aU.rst7

quit
EOF

tleap -s -f tleap.in
rm leap.log


