
function set_charge_GF_native()
{
    selection="$1"
    cat <<EOF

set ${selection}.N9    charge  0.049200
set ${selection}.C8    charge  0.137400
set ${selection}.H8    charge  0.164000
set ${selection}.N7    charge -0.570900
set ${selection}.C5    charge  0.174400
set ${selection}.C6    charge  0.477000
set ${selection}.O6    charge -0.559700
set ${selection}.N1    charge -0.478700
set ${selection}.H1    charge  0.342400
set ${selection}.C2    charge  0.765700
set ${selection}.N2    charge -0.967200
set ${selection}.H21   charge  0.436400
set ${selection}.H22   charge  0.436400
set ${selection}.N3    charge -0.632300
set ${selection}.C4    charge  0.122200
set ${selection}.O2'   charge -0.613900
set ${selection}.HO2'  charge  0.418600
set ${selection}.H7    charge  0.000000
EOF
}


function set_charge_DGF_native()
{
    selection="$1"
    cat <<EOF

set ${selection}.N9    charge  0.057700
set ${selection}.C8    charge  0.073600
set ${selection}.H8    charge  0.199700
set ${selection}.N7    charge -0.572500
set ${selection}.C5    charge  0.199100
set ${selection}.C6    charge  0.491800
set ${selection}.O6    charge -0.569900
set ${selection}.N1    charge -0.505300
set ${selection}.H1    charge  0.352000
set ${selection}.C2    charge  0.743200
set ${selection}.N2    charge -0.923000
set ${selection}.H21   charge  0.423500
set ${selection}.H22   charge  0.423500
set ${selection}.N3    charge -0.663600
set ${selection}.C4    charge  0.181400
set ${selection}.H7    charge  0.000000
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N9      -0.083055   -0.132255   -0.080538    0.051717    0.049200
# 1    C8       0.158700    0.021300    0.049546    0.028246    0.137400
# 1    H8       0.105675   -0.058325    0.112988    0.171313    0.164000
# 1    N7      -0.618647   -0.047747   -0.548391   -0.500644   -0.570900
# 1    C5       0.070000   -0.104400    0.024518    0.128918    0.174400
# 1    C6       0.705319    0.228319    0.726310    0.497991    0.477000
# 1    O6      -0.718235   -0.158535   -0.702537   -0.544002   -0.559700
# 1    N1      -0.837562   -0.358862   -0.818621   -0.459759   -0.478700
# 1    C2       0.969192    0.203492    0.812989    0.609497    0.765700
# 1    N2      -1.042107   -0.074907   -0.917204   -0.842297   -0.967200
# 1    H21      0.396521   -0.039879    0.343333    0.383212    0.436400
# 1    H22      0.396521   -0.039879    0.343333    0.383212    0.436400
# 1    N3      -0.789592   -0.157292   -0.698050   -0.540758   -0.632300
# 1    C4       0.183569    0.061369    0.248624    0.187255    0.122200
# 1    C9       0.034785    0.213685    0.030399   -0.183286   -0.178900
# 1    H91      0.022972   -0.071228    0.024434    0.095662    0.094200
# 1    H92      0.022972   -0.071228    0.024434    0.095662    0.094200
# 1    H93      0.022972   -0.071228    0.024434    0.095662    0.094200
# 1    H1                  -0.342400                0.342400    0.342400
# 1    SUM     -1.000000   -1.000000   -1.000000   -0.000000    0.000000


function set_charge_GF_deprotN1
{
   selection="$1"
   cat <<EOF
set ${selection}.N9    charge  -0.083055
set ${selection}.C8    charge   0.158700
set ${selection}.H8    charge   0.105675
set ${selection}.N7    charge  -0.618647
set ${selection}.C5    charge   0.070000
set ${selection}.C6    charge   0.705319
set ${selection}.O6    charge  -0.718235
set ${selection}.N1    charge  -0.837562
set ${selection}.C2    charge   0.969192
set ${selection}.N2    charge  -1.042107
set ${selection}.H21   charge   0.396521
set ${selection}.H22   charge   0.396521
set ${selection}.N3    charge  -0.789592
set ${selection}.C4    charge   0.183569
set ${selection}.H1    charge   0.0
set ${selection}.H7    charge   0.0
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N9       0.263931    0.214731    0.266448    0.051717    0.049200
# 1    C8       0.085253   -0.052147   -0.023901    0.028246    0.137400
# 1    H8       0.253106    0.089106    0.260419    0.171313    0.164000
# 1    N7      -0.394573    0.176327   -0.324317   -0.500644   -0.570900
# 1    C5       0.028266   -0.146134   -0.017216    0.128918    0.174400
# 1    C6       0.465262   -0.011738    0.486253    0.497991    0.477000
# 1    O6      -0.497175    0.062525   -0.481477   -0.544002   -0.559700
# 1    N1      -0.442585    0.036115   -0.423644   -0.459759   -0.478700
# 1    H1       0.353615    0.011215    0.353615    0.342400    0.342400
# 1    C2       0.886566    0.120866    0.730363    0.609497    0.765700
# 1    N2      -1.052155   -0.084955   -0.927252   -0.842297   -0.967200
# 1    H21      0.508569    0.072169    0.455381    0.383212    0.436400
# 1    H22      0.508569    0.072169    0.455381    0.383212    0.436400
# 1    N3      -0.674117   -0.041817   -0.582575   -0.540758   -0.632300
# 1    C4       0.172481    0.050281    0.237536    0.187255    0.122200
# 1    H7       0.431284    0.431284    0.431284                        
# 1    C9      -0.558651   -0.379751   -0.563037   -0.183286   -0.178900
# 1    H91      0.220784    0.126584    0.222246    0.095662    0.094200
# 1    H92      0.220784    0.126584    0.222246    0.095662    0.094200
# 1    H93      0.220784    0.126584    0.222246    0.095662    0.094200
# 1    SUM      1.000000    1.000000    1.000000   -0.000000    0.000000


function set_charge_GF_protN7
{
   selection="$1"
   cat <<EOF
set ${selection}.N9    charge   0.263931
set ${selection}.C8    charge   0.085253
set ${selection}.H8    charge   0.253106
set ${selection}.N7    charge  -0.394573
set ${selection}.C5    charge   0.028266
set ${selection}.C6    charge   0.465262
set ${selection}.O6    charge  -0.497175
set ${selection}.N1    charge  -0.442585
set ${selection}.H1    charge   0.353615
set ${selection}.C2    charge   0.886566
set ${selection}.N2    charge  -1.052155
set ${selection}.H21   charge   0.508569
set ${selection}.H22   charge   0.508569
set ${selection}.N3    charge  -0.674117
set ${selection}.C4    charge   0.172481
set ${selection}.H7    charge   0.431284
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N9      -0.067732   -0.125432   -0.051166    0.074266    0.057700
# 1    C8       0.089695    0.016095    0.036377    0.020282    0.073600
# 1    H8       0.142289   -0.057411    0.116227    0.173638    0.199700
# 1    N7      -0.619437   -0.046937   -0.544679   -0.497742   -0.572500
# 1    C5       0.105819   -0.093281    0.021278    0.114559    0.199100
# 1    C6       0.700738    0.208938    0.729268    0.520330    0.491800
# 1    O6      -0.724137   -0.154237   -0.703317   -0.549080   -0.569900
# 1    N1      -0.833463   -0.328163   -0.819198   -0.491035   -0.505300
# 1    C2       0.927054    0.183854    0.813783    0.629929    0.743200
# 1    N2      -0.993500   -0.070500   -0.917534   -0.847034   -0.923000
# 1    H21      0.383213   -0.040287    0.343333    0.383620    0.423500
# 1    H22      0.383213   -0.040287    0.343333    0.383620    0.423500
# 1    N3      -0.810817   -0.147217   -0.696701   -0.549484   -0.663600
# 1    C4       0.228266    0.046866    0.240197    0.193331    0.181400
# 1    C9      -0.015257    0.225043   -0.011902   -0.236945   -0.240300
# 1    H91      0.034685   -0.075015    0.033567    0.108582    0.109700
# 1    H92      0.034685   -0.075015    0.033567    0.108582    0.109700
# 1    H93      0.034685   -0.075015    0.033567    0.108582    0.109700
# 1    H1                  -0.352000                0.352000    0.352000
# 1    SUM     -1.000000   -1.000000   -1.000000    0.000000    0.000000


function set_charge_DGF_deprotN1
{
   selection="$1"
   cat <<EOF
set ${selection}.N9    charge  -0.067732
set ${selection}.C8    charge   0.089695
set ${selection}.H8    charge   0.142289
set ${selection}.N7    charge  -0.619437
set ${selection}.C5    charge   0.105819
set ${selection}.C6    charge   0.700738
set ${selection}.O6    charge  -0.724137
set ${selection}.N1    charge  -0.833463
set ${selection}.C2    charge   0.927054
set ${selection}.N2    charge  -0.993500
set ${selection}.H21   charge   0.383213
set ${selection}.H22   charge   0.383213
set ${selection}.N3    charge  -0.810817
set ${selection}.C4    charge   0.228266
set ${selection}.H1    charge   0.0
set ${selection}.H7    charge   0.0
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N9       0.278019    0.220319    0.294585    0.074266    0.057700
# 1    C8       0.025119   -0.048481   -0.028199    0.020282    0.073600
# 1    H8       0.286354    0.086654    0.260292    0.173638    0.199700
# 1    N7      -0.400945    0.171555   -0.326187   -0.497742   -0.572500
# 1    C5       0.068622   -0.130478   -0.015919    0.114559    0.199100
# 1    C6       0.459359   -0.032441    0.487889    0.520330    0.491800
# 1    O6      -0.503012    0.066888   -0.482192   -0.549080   -0.569900
# 1    N1      -0.437288    0.068012   -0.423023   -0.491035   -0.505300
# 1    H1       0.353053    0.001053    0.353053    0.352000    0.352000
# 1    C2       0.845886    0.102686    0.732615    0.629929    0.743200
# 1    N2      -1.005751   -0.082751   -0.929785   -0.847034   -0.923000
# 1    H21      0.495879    0.072379    0.455999    0.383620    0.423500
# 1    H22      0.495879    0.072379    0.455999    0.383620    0.423500
# 1    N3      -0.693953   -0.030353   -0.579837   -0.549484   -0.663600
# 1    C4       0.211646    0.030246    0.223577    0.193331    0.181400
# 1    H7       0.432333    0.432333    0.432333                        
# 1    C9      -0.617387   -0.377087   -0.614032   -0.236945   -0.240300
# 1    H91      0.235395    0.125695    0.234277    0.108582    0.109700
# 1    H92      0.235395    0.125695    0.234277    0.108582    0.109700
# 1    H93      0.235395    0.125695    0.234277    0.108582    0.109700
# 1    SUM      1.000000    1.000000    1.000000    0.000000    0.000000


function set_charge_DGF_protN7
{
   selection="$1"
   cat <<EOF
set ${selection}.N9    charge   0.278019
set ${selection}.C8    charge   0.025119
set ${selection}.H8    charge   0.286354
set ${selection}.N7    charge  -0.400945
set ${selection}.C5    charge   0.068622
set ${selection}.C6    charge   0.459359
set ${selection}.O6    charge  -0.503012
set ${selection}.N1    charge  -0.437288
set ${selection}.H1    charge   0.353053
set ${selection}.C2    charge   0.845886
set ${selection}.N2    charge  -1.005751
set ${selection}.H21   charge   0.495879
set ${selection}.H22   charge   0.495879
set ${selection}.N3    charge  -0.693953
set ${selection}.C4    charge   0.211646
set ${selection}.H7    charge   0.432333
EOF
}

