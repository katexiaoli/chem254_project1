#!/bin/bash


function resetbox
{
    local refrst="$1"
    local modrst="$2"
    
    local c=($(tail -n 1 ${refrst}))
    ChBox -X ${c[0]} -Y ${c[1]} -Z ${c[2]} -al ${c[3]} -bt ${c[4]} -gm ${c[5]} -c ${modrst} -o ${modrst}.tmp
    mv ${modrst}.tmp ${modrst}
}

function setifbox
{
    local parm="$1"
    sed -i 's|0       0       1|0       0       2|' ${parm}
}


function start_leaprc()
{
    if [ -e "${AMBERHOME}/dat/leap/cmd/leaprc.protein.ff14SB" ]; then
	# amber 16 #########################
	cat <<EOF 
source leaprc.constph
set default PBradii mbondi3

source leaprc.protein.ff14SB
source leaprc.water.tip4pew
EOF
    elif [ -e "${AMBERHOME}/dat/leap/cmd/leaprc.ff14SB" ]; then
	# amber 14 #########################
	cat <<EOF 
source leaprc.constph
set default PBradii mbondi3

source leaprc.ff14SB
loadOff atomic_ions.lib
loadOff solvents.lib
HOH = T4E
WAT = T4E
loadAmberParams frcmod.tip4pew
loadAmberParams frcmod.ionsjc_tip4pew

#loadamberparams frcmod.tip4pew
#WAT=T4E
#loadamberparams frcmod.ionsjc_tip4pew
EOF
	if [ -e "${AMBERHOME}/dat/leap/parm/frcmod.ions234lm_126_tip4pew" ]; then	    
	    echo "loadamberparams frcmod.ions234lm_126_tip4pew"
	elif [ -e "${AMBERHOME}/dat/leap/parm/frcmod.ionslrcm_cm_tip4pew" ]; then
	    echo "loadamberparams frcmod.ionslrcm_cm_tip4pew"
	fi 
    else
	echo "# ERROR IN function start_leaprc()"
    fi
    
    leap="${GIT_HOME}/DataBase/CustomResidues"

    cat <<EOF 

WAT=T4E


# defines the HD atom-type
loadamberparams ${leap}/leap/CustomResidues.frcmod


# RNA 2-aminopurine
loadamberparams ${leap}/2-aminopurine-dna-and-rna/AP.frcmod
loadoff ${leap}/2-aminopurine-dna-and-rna/AP.lib

# DNA 2-aminopurine
loadamberparams ${leap}/2-aminopurine-dna-and-rna/DAP.frcmod
loadoff ${leap}/2-aminopurine-dna-and-rna/DAP.lib



# RNA protonated-N1 adenine
loadoff ${leap}/adenine-dna-and-rna/AF.lib

# DNA protonated-N1 adenine
loadoff ${leap}/adenine-dna-and-rna/DAF.lib

# RNA protonated-N1 adenine mononucleotide molecule
loadoff ${leap}/adenine-dna-and-rna/AM.lib

# DNA protonated-N1 adenine mononucleotide molecule
loadoff ${leap}/adenine-dna-and-rna/DAM.lib




# RNA protonated-N3 cytosine
loadoff ${leap}/cytosine-protN3-dna-and-rna/CF.lib

# DNA protonated-N3 cytosine
loadoff ${leap}/cytosine-protN3-dna-and-rna/DCF.lib

# RNA protonated-N3 cytosine mononucleotide molecule
loadoff ${leap}/cytosine-protN3-dna-and-rna/CM.lib

# DNA protonated-N3 cytosine mononucleotide molecule
loadoff ${leap}/cytosine-protN3-dna-and-rna/DCM.lib





# RNA deprotonated-N1 guanine
loadoff ${leap}/guanine-deprotN1-dna-and-rna/GF.lib

# DNA deprotonated-N1 guanine
loadoff ${leap}/guanine-deprotN1-dna-and-rna/DGF.lib

# RNA deprotonated-N1 guanine mononucleotide molecule
loadoff ${leap}/guanine-deprotN1-dna-and-rna/GM.lib

# DNA deprotonated-N1 guanine mononucleotide molecule
loadoff ${leap}/guanine-deprotN1-dna-and-rna/DGM.lib





# RNA O3'-ethoxide cap end-group
loadoff ${leap}/ethoxide-caps-dna-and-rna/E3.lib

# RNA O3'-ethoxide cap end-group
loadoff ${leap}/ethoxide-caps-dna-and-rna/E5.lib

# DNA O3'-ethoxide cap end-group
loadoff ${leap}/ethoxide-caps-dna-and-rna/DE3.lib

# DNA O5'-ethoxide cap end-group
loadoff ${leap}/ethoxide-caps-dna-and-rna/DE5.lib





# RNA O5'-hydroxyl cap end-group
loadoff ${leap}/hydroxide5prime-caps-dna-and-rna/O5.lib

# DNA O5'-hydroxyl cap end-group
loadoff ${leap}/hydroxide5prime-caps-dna-and-rna/DO5.lib


EOF

}


leap="${GIT_HOME}/DataBase/CustomResidues"

source ${leap}/adenine-dna-and-rna/charge_fit_functions.sh
source ${leap}/cytosine-protN3-dna-and-rna/charge_fit_functions.sh
source ${leap}/guanine-deprotN1-dna-and-rna/charge_fit_functions.sh

#######################################################################


if [ $# -gt 0 ]; then
    
    echo "==============================================="
    echo "Available functions:"
    echo "- - - - - - - - - - - - - - - - - - - - - - - -"

    declare -F | grep -v '\-fx' | sed -e 's|declare -f ||'
    echo "==============================================="
    
fi
