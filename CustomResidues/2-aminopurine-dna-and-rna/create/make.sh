#!/bin/bash
set -e
set -u

source ${HOME}/devel/git/DataBase/CustomResidues/leap/bashrc




function RunRespCalcs
{
    local mol2="$1"
    local base=${mol2%.mol2}
    if [ ! -e ${base}.log ]; then
	parmutils-mol22g09.py --run ${base}.mol2
    fi
    for alp in 0 30 60 90 120 150 180; do
	for bet in 0 30 60 90; do
	    f=$(printf "%s_%.2f_%.2f_%.2f.log" ${base} ${alp} ${bet} 0)
	    if [ ! -e ${f} ]; then
		parmutils-rotate-g09-resp.py --alp ${alp} --bet ${bet} --run ${base}.log
	    fi
	done
    done
}




function extract_dna_nucleobase
{
    local selection="$1"
    cat <<EOF
remove ${selection} ${selection}.P
remove ${selection} ${selection}.OP1
remove ${selection} ${selection}.OP2
remove ${selection} ${selection}.O5'
remove ${selection} ${selection}.C5'
remove ${selection} ${selection}.H5'
remove ${selection} ${selection}.H5''
remove ${selection} ${selection}.C4'
remove ${selection} ${selection}.H4'
remove ${selection} ${selection}.O4'
remove ${selection} ${selection}.C1'
remove ${selection} ${selection}.H1'
remove ${selection} ${selection}.C3'
remove ${selection} ${selection}.H3'
remove ${selection} ${selection}.C2'
remove ${selection} ${selection}.H2'
remove ${selection} ${selection}.O3'
##########################
# DNA
remove ${selection} ${selection}.H2''
# RNA
#remove ${selection} ${selection}.O2'
#remove ${selection} ${selection}.HO2'
##########################
EOF
}


function extract_rna_nucleobase
{
    local selection="$1"
    cat <<EOF
remove ${selection} ${selection}.P
remove ${selection} ${selection}.OP1
remove ${selection} ${selection}.OP2
remove ${selection} ${selection}.O5'
remove ${selection} ${selection}.C5'
remove ${selection} ${selection}.H5'
remove ${selection} ${selection}.H5''
remove ${selection} ${selection}.C4'
remove ${selection} ${selection}.H4'
remove ${selection} ${selection}.O4'
remove ${selection} ${selection}.C1'
remove ${selection} ${selection}.H1'
remove ${selection} ${selection}.C3'
remove ${selection} ${selection}.H3'
remove ${selection} ${selection}.C2'
remove ${selection} ${selection}.H2'
remove ${selection} ${selection}.O3'
##########################
# DNA
#remove ${selection} ${selection}.H2''
# RNA
remove ${selection} ${selection}.O2'
remove ${selection} ${selection}.HO2'
##########################
EOF
}


function methylate_DG_N9
{
    local selection="$1"
cat <<EOF
C9 = createAtom "C9" "CA" -0.2403
add ${selection} C9
bond ${selection}.N9 ${selection}.C9
select ${selection}.C9
relax ${selection}
deselect ${selection}.C9

H91 = createAtom "H91" "HA" 0.109707
add ${selection} H91
bond ${selection}.C9 ${selection}.H91
select ${selection}.H91
relax ${selection}
deselect ${selection}.H91

H92 = createAtom "H92" "HA" 0.109707
add ${selection} H92
bond ${selection}.C9 ${selection}.H92
select ${selection}.H92
relax ${selection}
deselect ${selection}.H92

H93 = createAtom "H93" "HA" 0.109707
add ${selection} H93
bond ${selection}.C9 ${selection}.H93
select ${selection}.H93
relax ${selection}
deselect ${selection}.H93

select ${selection}.H91
select ${selection}.H92
select ${selection}.H93
relax ${selection}
deselect ${selection}.H91
deselect ${selection}.H92
deselect ${selection}.H93
EOF
}




function methylate_G_N9
{
    local selection="$1"
cat <<EOF
C9 = createAtom "C9" "CA" -0.1789
add ${selection} C9
bond ${selection}.N9 ${selection}.C9
select ${selection}.C9
relax ${selection}
deselect ${selection}.C9

H91 = createAtom "H91" "HA" 0.094193
add ${selection} H91
bond ${selection}.C9 ${selection}.H91
select ${selection}.H91
relax ${selection}
deselect ${selection}.H91

H92 = createAtom "H92" "HA" 0.094193
add ${selection} H92
bond ${selection}.C9 ${selection}.H92
select ${selection}.H92
relax ${selection}
deselect ${selection}.H92

H93 = createAtom "H93" "HA" 0.094193
add ${selection} H93
bond ${selection}.C9 ${selection}.H93
select ${selection}.H93
relax ${selection}
deselect ${selection}.H93


select ${selection}.H91
select ${selection}.H92
select ${selection}.H93
relax ${selection}
deselect ${selection}.H91
deselect ${selection}.H92
deselect ${selection}.H93
EOF
}




###################################################
# DNA guanine nucleobase (methylated @ N9)
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
DG = sequence { DG }
set DG name "DG"
set DG.1 name "DG"
EOF

extract_dna_nucleobase "DG.1" >> tleap.in
methylate_DG_N9 "DG.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 DG DG.mol2 1
savepdb DG DG.pdb
quit
EOF

tleap -s -f tleap.in


#
# make DG.log from DG.mol2
#
RunRespCalcs DG.mol2




###################################################
# RNA guanine nucleobase (methylated @ N9)
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
G = sequence { G }
set G name "G"
set G.1 name "G"
EOF

extract_rna_nucleobase "G.1" >> tleap.in
methylate_G_N9 "G.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 G G.mol2 1
savepdb G G.pdb
quit
EOF

tleap -s -f tleap.in


#
# make G.log from G.mol2
#
RunRespCalcs G.mol2


###################################################
# DNA 2-aminopurine nucleobase (methylated @ N9)
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in

DAP = sequence { DG }

set DAP name "DAP"
set DAP.1 name "DAP"

remove DAP.1 DAP.1.O6
remove DAP.1 DAP.1.H1

# The H6 charge simply preserves the nucleobase net charge
H6 = createAtom "H6" "H5" -0.2179
add DAP.1 H6
bond DAP.1.C6 DAP.1.H6
select DAP.1.H6
relax DAP
deselect DAP.1.H6

EOF

extract_dna_nucleobase "DAP.1" >> tleap.in
methylate_DG_N9 "DAP.1" >> tleap.in

cat<<EOF >> tleap.in
savemol2 DAP DAP.mol2 1
savepdb DAP DAP.pdb
quit
EOF
tleap -s -f tleap.in

#
# make DAP.log from DAP.mol2
#
RunRespCalcs DAP.mol2



###################################################
# RNA 2-aminopurine nucleobase (methylated @ N9)
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in

AP = sequence { G }

set AP name "AP"
set AP.1 name "AP"

remove AP.1 AP.1.O6
remove AP.1 AP.1.H1

# The H6 charge merely preserves the nucleobase net charge
H6 = createAtom "H6" "H5" -0.2173
add AP.1 H6
bond AP.1.C6 AP.1.H6
select AP.1.H6
relax AP
deselect AP.1.H6

EOF

extract_rna_nucleobase "AP.1" >> tleap.in
methylate_G_N9 "AP.1" >> tleap.in

cat<<EOF >> tleap.in
savemol2 AP AP.mol2 1
savepdb AP AP.pdb
quit
EOF
tleap -s -f tleap.in

#
# make AP.log from AP.mol2
#
RunRespCalcs AP.mol2





##################################################
# CHARGE FITS
##################################################

cat <<'EOF' | python2.7 | sed -e 's|_1||' > charge_fit_functions.sh
import parmutils
import parmutils.respfit as rf

def GetLogFiles(mol2):
    import glob
    base=mol2.replace(".mol2","")
    return glob.glob("%s_*.log"%(base))

if __name__ == "__main__":
    comp = parmutils.BASH( 12 )
    
    ###############################################################
    # RNA
    ###############################################################

    native = rf.ResidueResp( comp, 1 )
    native.add_state( "G", "G.mol2", GetLogFiles("G.mol2") )
    #  ** AUXILIARY CAP **
    native.states[-1].add_group_restraint( "@C9,H91,H92,H93" )
    #  ** DELETED ATOMS **
    native.states[-1].add_group_restraint( "@H1,O6" )

    native.multimolecule_fit(True) 
    native.perform_fit("@*",unique_residues=False)
    native.preserve_residue_charges_by_shifting()
    #native.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #native.print_resp()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    model = rf.ResidueResp( comp, 1 )
    model.add_state( "AP", "AP.mol2", GetLogFiles("AP.mol2") )
    #   ** AUXILIARY CAP **
    model.states[-1].add_group_restraint( "@C9,H91,H92,H93" )

    model.multimolecule_fit(True) 
    model.perform_fit("@*",unique_residues=False)
    model.preserve_residue_charges_by_shifting()
    #model.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #model.print_resp()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    rf.PrintPerturbedCharges(native,model,exclude=["H1","O6","C9","H91","H92","H93"])

    ###############################################################
    # DNA
    ###############################################################

    native = rf.ResidueResp( comp, 1 )
    native.add_state( "DG", "DG.mol2", GetLogFiles("DG.mol2") )
    #  ** AUXILIARY CAP **
    native.states[-1].add_group_restraint( "@C9,H91,H92,H93" )
    #  ** DELETED ATOMS **
    native.states[-1].add_group_restraint( "@H1,O6" )

    native.multimolecule_fit(True) 
    native.perform_fit("@*",unique_residues=False)
    native.preserve_residue_charges_by_shifting()
    #native.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #native.print_resp()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    model = rf.ResidueResp( comp, 1 )
    model.add_state( "DAP", "DAP.mol2", GetLogFiles("DAP.mol2") )
    # ** AUXILIARY CAP **
    model.states[-1].add_group_restraint( "@C9,H91,H92,H93" )

    model.multimolecule_fit(True) 
    model.perform_fit("@*",unique_residues=False)
    model.preserve_residue_charges_by_shifting()
    #model.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #model.print_resp()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    rf.PrintPerturbedCharges(native,model,exclude=["H1","O6","C9","H91","H92","H93"])

EOF




source charge_fit_functions.sh





###################################################
# DNA
###################################################



###################################################
#
# We need to do amber atom-type assignments and
# make a frcmod file.  In order to get proper
# assignments and needed parameters, we stick
# the purine in a polymer
#

start_leaprc > tleap.in
cat <<EOF >> tleap.in

DAP = sequence { DG5 DG DG3 }

set DAP name "DAP"
set DAP.2 name "DAP"

remove DAP.2 DAP.2.O6
remove DAP.2 DAP.2.H1

H6 = createAtom "H6" "H5" 0.0000
add DAP.2 H6
bond DAP.2.C6 DAP.2.H6
select DAP.2.H6
relax DAP
deselect DAP.2.H6

savemol2 DAP DAP.mol2 1
quit

EOF

tleap -s -f tleap.in



###################################################
#
# Assign amber atomtypes and create a frcmod for
# the missing parameter values
#

parmutils-pdb2frcmod-gaff-fill.py --mask=":2" DAP.mol2

#
# The above command gave us a frcmod file, however, I need
# to adjust some of the parameter values by hand.
# Specifically, the amine group should be planar, so
# the angles around the N should add up to 360
# ... grabbing the missing GAFF parameters doesn't yield
# the correct sum.
#
cat <<EOF > DAP.frcmod
manually set NC-CA-H4, CQ-N2-H, NC-CQ-N2-H to match DA amine group
MASS

BOND
CQ-N2    417.89999999999998   1.38590000

ANGLE
NC-CQ-N2     70.00000000000000            115.45   # 0.5*(360-(NC-CQ-NC))
NC-CA-H4     50.00000000000000            120.000
CQ-N2-H      50.00000000000000            120.000

DIHE
NC-CQ-N2-H     1     2.40000000000000       180.000   2.0    SCEE=1.2 SCNB=2.0

IMPROPER

NONB


EOF

start_leaprc > tleap.in
cat <<EOF >> tleap.in

loadamberparams DAP.frcmod
DAP = loadmol2 DAP.mol2
set DAP.1 restype  nucleic
set DAP.1 connect0 DAP.1.P
set DAP.1 connect1 DAP.1.O3'
set DAP   head     DAP.1.P
set DAP   tail     DAP.1.O3'
EOF

#
# Our RESP-fit charges
#
set_charge_DAP "DAP.1" >> tleap.in

cat <<EOF >> tleap.in

savepdb  DAP DAP.pdb
saveoff  DAP DAP.lib
savemol2 DAP DAP.mol2 1
saveamberparm DAP DAP.parm7 DAP.rst7

quit
EOF

tleap -s -f tleap.in





###################################################
# RNA
###################################################



###################################################
#
# We need to do amber atom-type assignments and
# make a frcmod file.  In order to get proper
# assignments and needed parameters, we stick
# the purine in a polymer
#

start_leaprc > tleap.in
cat <<EOF >> tleap.in

AP = sequence { G5 G G3 }

set AP name "AP"
set AP.2 name "AP"

remove AP.2 AP.2.O6
remove AP.2 AP.2.H1

H6 = createAtom "H6" "H5" 0.0000
add AP.2 H6
bond AP.2.C6 AP.2.H6
select AP.2.H6
relax AP
deselect AP.2.H6

savemol2 AP AP.mol2 1
quit

EOF

tleap -s -f tleap.in


#
# This gives us the same atom-type assignments and
# frcmod as the DNA residue, so just copy the DNA frcmod
#
parmutils-pdb2frcmod-gaff-fill.py --mask=":2" AP.mol2
cp DAP.frcmod AP.frcmod



start_leaprc > tleap.in
cat <<EOF >> tleap.in

loadamberparams AP.frcmod
AP = loadmol2 AP.mol2
set AP.1 restype  nucleic
set AP.1 connect0 AP.1.P
set AP.1 connect1 AP.1.O3'
set AP   head     AP.1.P
set AP   tail     AP.1.O3'
EOF

#
# Our RESP-fit charges
#
set_charge_AP "AP.1" >> tleap.in

cat <<EOF >> tleap.in

savepdb  AP AP.pdb
saveoff  AP AP.lib
savemol2 AP AP.mol2 1
saveamberparm AP AP.parm7 AP.rst7

quit
EOF

tleap -s -f tleap.in


for f in *.amber.* *.chk multistate.* leap.log tleap.in *.com G.mol2 G.pdb DG.mol2 DG.pdb *.parm7 *.rst7; do
    if [ -e ${f} ]; then
	rm ${f}
    fi
done

