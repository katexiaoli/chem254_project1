#!/bin/bash
set -e
set -u

source ${HOME}/devel/git/DataBase/FrankensteinResidues/leap/bashrc

function add_dna_OH
{
    local selection="$1"
    cat <<EOF

H = createAtom "H" "H*"  0.3131
O = createAtom "O" "OH" -0.6210

add ${selection} O
bond ${selection}.P ${selection}.O
select ${selection}.O
relax ${selection}
deselect ${selection}.O

add ${selection} H
bond ${selection}.O ${selection}.H
select ${selection}.H
relax ${selection}
deselect ${selection}.H

select ${selection}.O
select ${selection}.H
relax ${selection}
deselect ${selection}.O
deselect ${selection}.H

EOF
    
}


function add_rna_OH
{
    local selection="$1"
    cat <<EOF

H = createAtom "H" "H*"  0.3129
O = createAtom "O" "OH" -0.6210

add ${selection} O
bond ${selection}.P ${selection}.O
select ${selection}.O
relax ${selection}
deselect ${selection}.O

add ${selection} H
bond ${selection}.O ${selection}.H
select ${selection}.H
relax ${selection}
deselect ${selection}.H

select ${selection}.O
select ${selection}.H
relax ${selection}
deselect ${selection}.O
deselect ${selection}.H

EOF
}


####################################################################################

if [ -e DO5.lib ]; then
    rm -f DO5.lib
fi


if [ -e O5.lib ]; then
    rm -f O5.lib
fi


start_leaprc > tleap.in
cat <<EOF >> tleap.in

DO5 = sequence { OHE }
set DO5   name "DO5"
set DO5.1 name "DO5"

set DO5.1.H type     "H*"
set DO5.1.H pertType "H*"
#set DO5.1.H charge  0.388950
#set DO5.1.O charge -0.696850
set DO5.1.H charge  0.3929705
set DO5.1.O charge -0.7008705


set DO5.1 restype  nucleic
set DO5.1 connect1 DO5.1.O
set DO5   tail     DO5.1.O

savepdb  DO5 DO5.pdb
saveoff  DO5 DO5.lib
savemol2 DO5 DO5.mol2 1


O5 = sequence { OHE }
set O5   name "O5"
set O5.1 name "O5"

set O5.1.H type     "H*"
set O5.1.H pertType "H*"
#set O5.1.H charge  0.396891
#set O5.1.O charge -0.704991
set O5.1.H charge  0.3928705
set O5.1.O charge -0.7009705


set O5.1 restype  nucleic
set O5.1 connect1 O5.1.O
set O5   tail     O5.1.O

savepdb  O5 O5.pdb
saveoff  O5 O5.lib
savemol2 O5 O5.mol2 1


quit
EOF

tleap -s -f tleap.in
rm leap.log



#############
exit
#############


start_leaprc > tleap.in
cat <<EOF >> tleap.in

DO3 = sequence { OHE }
set DO3   name "DO3"
set DO3.1 name "DO3"
remove DO3.1 DO3.1.O
set DO3.1.H name        "HO3'"
set DO3.1.HO3' type     "HO"
set DO3.1.HO3' pertType "HO"
set DO3.1.HO3' charge  0.307900
set DO3.1 restype  nucleic
set DO3 head DO3.1.HO3'
set DO3 tail null
set DO3.1 connect0 DO3.1.HO3'
set DO3.1 connect1 null
savepdb  DO3 DO3.pdb
saveoff  DO3 DO3.lib
savemol2 DO3 DO3.mol2 1

#mol = sequence { DC }
#HO3p = createAtom "HO3'" "HO" 0.307900
#add      mol.1 HO3p
#bond     mol.1.O3' mol.1.HO3'
#select   mol.1.HO3'
#relax    mol.1
#deselect mol.1.HO3'
#savemol2 mol mol.mol2 1

quit
EOF

tleap -s -f tleap.in
rm leap.log


start_leaprc > tleap.in
cat <<EOF >> tleap.in
loadoff DO5.lib
loadoff DO3.lib

mol = sequence { DC DO3 }
savemol2 mol mol.mol2 1
#saveamberparm mol mol.parm7 mol.rst7
quit
EOF

tleap -s -f tleap.in
rm leap.log

parmutils-pdb2frcmod-gaff-fill.py -n mol.mol2


cat <<EOF > DO3.frcmod
modified parameters
MASS

BOND

ANGLE
CT-OS-HO     55.0000   108.5000  copied from CT-OH-HO

DIHE

IMPROPER

NONB

EOF



#############
exit
#############


start_leaprc > tleap.in

for base in A C G T; do

    echo "mol = sequence { OHE D${base}3 }" >> tleap.in
    #add_dna_OH "mol.1" >> tleap.in
    cat <<EOF >> tleap.in
set mol.1.H type    "H*"
set mol.1.H charge  0.388950
set mol.1.O charge -0.696850
EOF
    echo "solvateOct mol TIP4PEWBOX 13" >> tleap.in
    echo "saveamberparm mol D${base}.parm7 D${base}.rst7" >> tleap.in

done


for base in A C G U; do

    echo "mol = sequence { OHE ${base}3 }" >> tleap.in
    #add_rna_OH "mol.1" >> tleap.in
        cat <<EOF >> tleap.in
set mol.1.H type    "H*"
set mol.1.H charge  0.396891
set mol.1.O charge -0.704991
EOF
    echo "solvateOct mol TIP4PEWBOX 13" >> tleap.in
    echo "saveamberparm mol ${base}.parm7 ${base}.rst7" >> tleap.in

done



echo "quit" >> tleap.in
tleap -s -f tleap.in
rm leap.log


rm tleap.in
exit

####################################################################################

