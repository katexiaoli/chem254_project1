source leaprc.constph
set default PBradii mbondi3

source leaprc.protein.ff14SB
source leaprc.water.tip4pew

WAT=T4E

#####################################################
# two-sided frankenstein nucleotide fragments
#####################################################
# These are junjie's parametrization, which
# involved RESP fitting methylated nucleobases
# under the constraint that the non-methyl-part
# of the base sum to the fractional charge used by amber
#
# You can switch the charge states with the following functions in the bashrc
#
# set_charge_AFK_deprotonated_N6_protonated_N1_anti
# set_charge_AFK_deprotonated_N6_protonated_N1_syn
# set_charge_AFK_native
# set_charge_AFK_protonated_N1
# set_charge_AFK_protonated_N3
# set_charge_AFK_protonated_N7
#
# set_charge_CFK_deprotonated_N4_protonated_N3_anti
# set_charge_CFK_deprotonated_N4_protonated_N3_syn
# set_charge_CFK_native
# set_charge_CFK_protonated_N3
#
# set_charge_GFK_deprotonated_N1
# set_charge_GFK_deprotonated_N1_protonated_O6
# set_charge_GFK_native
#
# set_charge_UFK_deprotonated_N3
# set_charge_UFK_deprotonated_N3_protonated_O4
# set_charge_UFK_native
#
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/GFK/GFK.off
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/AFK/AFK.off
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/CFK/CFK.off
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/UFK/UFK.off

#####################################################
# one-sided frankenstein nucleotide fragments
#####################################################
# (same charge differences as the two-sided)
#
# You can switch the charge states with the following functions in the bashrc
#
# set_charge_AF3_deprotonated_N6_protonated_N1_anti
# set_charge_AF3_deprotonated_N6_protonated_N1_syn
# set_charge_AF3_native
# set_charge_AF3_protonated_N1
# set_charge_AF3_protonated_N3
# set_charge_AF3_protonated_N7
#
# set_charge_CF3_deprotonated_N4_protonated_N3_anti
# set_charge_CF3_deprotonated_N4_protonated_N3_syn
# set_charge_CF3_native
# set_charge_CF3_protonated_N3
#
# set_charge_GF3_deprotonated_N1
# set_charge_GF3_deprotonated_N1_protonated_O6
# set_charge_GF3_native
#
# set_charge_UF3_deprotonated_N3
# set_charge_UF3_deprotonated_N3_protonated_O4
# set_charge_UF3_native
#
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/XF3/GF3.off
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/XF3/AF3.off
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/XF3/CF3.off
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/XF3/UF3.off

#####################################################
# methylated frankenstein nucleobases
#####################################################
# (same charge differences as the two-sided and one-sided)
# (the methyl charges are am1-bcc -- uniformly scaled to neutralize the molecule)
#
# You can switch the charge states with the following functions in the bashrc
#
# set_charge_AFK_deprotonated_N6_protonated_N1_anti
# set_charge_AFK_deprotonated_N6_protonated_N1_syn
# set_charge_AFK_native
# set_charge_AFK_protonated_N1
# set_charge_AFK_protonated_N3
# set_charge_AFK_protonated_N7
#
# set_charge_CFK_deprotonated_N4_protonated_N3_anti
# set_charge_CFK_deprotonated_N4_protonated_N3_syn
# set_charge_CFK_native
# set_charge_CFK_protonated_N3
#
# set_charge_GFK_deprotonated_N1
# set_charge_GFK_deprotonated_N1_protonated_O6
# set_charge_GFK_native
#
# set_charge_UFK_deprotonated_N3
# set_charge_UFK_deprotonated_N3_protonated_O4
# set_charge_UFK_native
#
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/XFE/GFE.off
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/XFE/AFE.off
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/XFE/CFE.off
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/XFE/UFE.off

#####################################################
# hydrogenated frankenstein nucleobases
#####################################################
# The charge schemes are based on RESP fitting
# The charges are NOT consistent with the above fragments
#
# You can switch the charge states with the following functions in the bashrc
#
# set_charge_AFB_deprotonated_N6_protonated_N1_anti
# set_charge_AFB_deprotonated_N6_protonated_N1_syn
# set_charge_AFB_native
# set_charge_AFB_protonated_N1
# set_charge_AFB_protonated_N3
# set_charge_AFB_protonated_N7
#
# set_charge_CFB_deprotonated_N4_protonated_N3_anti
# set_charge_CFB_deprotonated_N4_protonated_N3_syn
# set_charge_CFB_native
# set_charge_CFB_protonated_N3
# set_charge_GFB_deprotonated_N1
# set_charge_GFB_deprotonated_N1_protonated_O6
# set_charge_GFB_native
#
# set_charge_UFB_deprotonated_N3
# set_charge_UFB_deprotonated_N3_protonated_O4
# set_charge_UFB_native

loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/XFB/GFB.off
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/XFB/AFB.off
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/XFB/CFB.off
loadoff /home/giese/devel/git/DataBase/FrankensteinResidues/leap/XFB/UFB.off

loadamberparams /home/giese/devel/git/DataBase/FrankensteinResidues/leap/Changeatomtype.frcmod


DO5 = sequence { OHE }
set DO5   name "DO5"
set DO5.1 name "DO5"

set DO5.1.H type     "H*"
set DO5.1.H pertType "H*"
#set DO5.1.H charge  0.388950
#set DO5.1.O charge -0.696850
set DO5.1.H charge  0.3929705
set DO5.1.O charge -0.7008705


set DO5.1 restype  nucleic
set DO5.1 connect1 DO5.1.O
set DO5   tail     DO5.1.O

savepdb  DO5 DO5.pdb
saveoff  DO5 DO5.lib
savemol2 DO5 DO5.mol2 1


O5 = sequence { OHE }
set O5   name "O5"
set O5.1 name "O5"

set O5.1.H type     "H*"
set O5.1.H pertType "H*"
#set O5.1.H charge  0.396891
#set O5.1.O charge -0.704991
set O5.1.H charge  0.3928705
set O5.1.O charge -0.7009705


set O5.1 restype  nucleic
set O5.1 connect1 O5.1.O
set O5   tail     O5.1.O

savepdb  O5 O5.pdb
saveoff  O5 O5.lib
savemol2 O5 O5.mol2 1


x = sequence { DC5 }
desc x

quit
