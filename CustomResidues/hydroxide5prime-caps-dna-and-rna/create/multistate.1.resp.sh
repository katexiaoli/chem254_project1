#!/bin/bash
out=multistate.1.resp
cat <<EOF > ${out}.qwt
1
1.

EOF

resp -O -i ${out}.inp -o ${out}.out -p ${out}.punch -t ${out}.qout -w ${out}.qwt -e ${out}.esp

rm -f ${out}.punch ${out}.qout ${out}.qwt ${out}.esp

