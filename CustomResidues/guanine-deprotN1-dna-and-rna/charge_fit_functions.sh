
function set_charge_GF_native()
{
    selection="$1"
    cat <<EOF

set ${selection}.N9    charge  0.049200
set ${selection}.C8    charge  0.137400
set ${selection}.H8    charge  0.164000
set ${selection}.N7    charge -0.570900
set ${selection}.C5    charge  0.174400
set ${selection}.C6    charge  0.477000
set ${selection}.O6    charge -0.559700
set ${selection}.N1    charge -0.478700
set ${selection}.H1    charge  0.342400
set ${selection}.C2    charge  0.765700
set ${selection}.N2    charge -0.967200
set ${selection}.H21   charge  0.436400
set ${selection}.H22   charge  0.436400
set ${selection}.N3    charge -0.632300
set ${selection}.C4    charge  0.122200
EOF
}


function set_charge_DGF_native()
{
    selection="$1"
    cat <<EOF

set ${selection}.N9    charge  0.057700
set ${selection}.C8    charge  0.073600
set ${selection}.H8    charge  0.199700
set ${selection}.N7    charge -0.572500
set ${selection}.C5    charge  0.199100
set ${selection}.C6    charge  0.491800
set ${selection}.O6    charge -0.569900
set ${selection}.N1    charge -0.505300
set ${selection}.H1    charge  0.352000
set ${selection}.C2    charge  0.743200
set ${selection}.N2    charge -0.923000
set ${selection}.H21   charge  0.423500
set ${selection}.H22   charge  0.423500
set ${selection}.N3    charge -0.663600
set ${selection}.C4    charge  0.181400
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N9      -0.083055   -0.132255   -0.080538    0.051717    0.049200
# 1    C8       0.158700    0.021300    0.049546    0.028246    0.137400
# 1    H8       0.105675   -0.058325    0.112988    0.171313    0.164000
# 1    N7      -0.618647   -0.047747   -0.548391   -0.500644   -0.570900
# 1    C5       0.070000   -0.104400    0.024518    0.128918    0.174400
# 1    C6       0.705319    0.228319    0.726310    0.497991    0.477000
# 1    O6      -0.718235   -0.158535   -0.702537   -0.544002   -0.559700
# 1    N1      -0.837561   -0.358861   -0.818620   -0.459759   -0.478700
# 1    C2       0.969192    0.203492    0.812989    0.609497    0.765700
# 1    N2      -1.042107   -0.074907   -0.917204   -0.842297   -0.967200
# 1    H21      0.396521   -0.039879    0.343333    0.383212    0.436400
# 1    H22      0.396521   -0.039879    0.343333    0.383212    0.436400
# 1    N3      -0.789592   -0.157292   -0.698050   -0.540758   -0.632300
# 1    C4       0.183569    0.061369    0.248624    0.187255    0.122200
# 1    C9       0.034785    0.213685    0.030399   -0.183286   -0.178900
# 1    H91      0.022972   -0.071228    0.024434    0.095662    0.094200
# 1    H92      0.022972   -0.071228    0.024434    0.095662    0.094200
# 1    H93      0.022972   -0.071228    0.024434    0.095662    0.094200
# 1    H1                  -0.342400                0.342400    0.342400
# 1    SUM     -1.000000   -1.000000   -1.000000   -0.000000    0.000000


function set_charge_GF_deprotN1
{
   selection="$1"
   cat <<EOF
set ${selection}.N9    charge  -0.083055
set ${selection}.C8    charge   0.158700
set ${selection}.H8    charge   0.105675
set ${selection}.N7    charge  -0.618647
set ${selection}.C5    charge   0.070000
set ${selection}.C6    charge   0.705319
set ${selection}.O6    charge  -0.718235
set ${selection}.N1    charge  -0.837561
set ${selection}.C2    charge   0.969192
set ${selection}.N2    charge  -1.042107
set ${selection}.H21   charge   0.396521
set ${selection}.H22   charge   0.396521
set ${selection}.N3    charge  -0.789592
set ${selection}.C4    charge   0.183569
set ${selection}.H1    charge   0.0
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N9      -0.067732   -0.125432   -0.051166    0.074266    0.057700
# 1    C8       0.089695    0.016095    0.036377    0.020282    0.073600
# 1    H8       0.142289   -0.057411    0.116227    0.173638    0.199700
# 1    N7      -0.619437   -0.046937   -0.544679   -0.497742   -0.572500
# 1    C5       0.105819   -0.093281    0.021278    0.114559    0.199100
# 1    C6       0.700737    0.208937    0.729267    0.520330    0.491800
# 1    O6      -0.724137   -0.154237   -0.703317   -0.549080   -0.569900
# 1    N1      -0.833463   -0.328163   -0.819198   -0.491035   -0.505300
# 1    C2       0.927054    0.183854    0.813783    0.629929    0.743200
# 1    N2      -0.993500   -0.070500   -0.917534   -0.847034   -0.923000
# 1    H21      0.383213   -0.040287    0.343333    0.383620    0.423500
# 1    H22      0.383213   -0.040287    0.343333    0.383620    0.423500
# 1    N3      -0.810817   -0.147217   -0.696701   -0.549484   -0.663600
# 1    C4       0.228265    0.046865    0.240196    0.193331    0.181400
# 1    C9      -0.015257    0.225043   -0.011902   -0.236945   -0.240300
# 1    H91      0.034685   -0.075015    0.033567    0.108582    0.109700
# 1    H92      0.034685   -0.075015    0.033567    0.108582    0.109700
# 1    H93      0.034685   -0.075015    0.033567    0.108582    0.109700
# 1    H1                  -0.352000                0.352000    0.352000
# 1    SUM     -1.000000   -1.000000   -1.000000    0.000000    0.000000


function set_charge_DGF_deprotN1
{
   selection="$1"
   cat <<EOF
set ${selection}.N9    charge  -0.067732
set ${selection}.C8    charge   0.089695
set ${selection}.H8    charge   0.142289
set ${selection}.N7    charge  -0.619437
set ${selection}.C5    charge   0.105819
set ${selection}.C6    charge   0.700737
set ${selection}.O6    charge  -0.724137
set ${selection}.N1    charge  -0.833463
set ${selection}.C2    charge   0.927054
set ${selection}.N2    charge  -0.993500
set ${selection}.H21   charge   0.383213
set ${selection}.H22   charge   0.383213
set ${selection}.N3    charge  -0.810817
set ${selection}.C4    charge   0.228265
set ${selection}.H1    charge   0.0
EOF
}

