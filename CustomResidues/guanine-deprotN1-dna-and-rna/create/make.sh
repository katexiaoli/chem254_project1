#!/bin/bash
set -e
set -u

source ${HOME}/devel/git/DataBase/CustomResidues/leap/bashrc



function RunRespCalcs
{
    local mol2="$1"
    local base=${mol2%.mol2}
    if [ ! -e ${base}.log ]; then
	parmutils-mol22g09.py --run ${base}.mol2
    fi
    for alp in 0 30 60 90 120 150 180; do
	for bet in 0 30 60 90; do
	    f=$(printf "%s_%.2f_%.2f_%.2f.log" ${base} ${alp} ${bet} 0)
	    if [ ! -e ${f} ]; then
		parmutils-rotate-g09-resp.py --alp ${alp} --bet ${bet} --run ${base}.log
	    fi
	done
    done
}



function add_dna_OH
{
    local selection="$1"
    cat <<EOF

H = createAtom "H" "H*"   0.3929705
O = createAtom "O" "OH"  -0.7008705

add ${selection} O
bond ${selection}.P ${selection}.O
select ${selection}.O
relax ${selection}
deselect ${selection}.O

add ${selection} H
bond ${selection}.O ${selection}.H
select ${selection}.H
relax ${selection}
deselect ${selection}.H

select ${selection}.O
select ${selection}.H
relax ${selection}
deselect ${selection}.O
deselect ${selection}.H

EOF
    
}


function add_rna_OH
{
    local selection="$1"
    cat <<EOF

H = createAtom "H" "H*"   0.3928705
O = createAtom "O" "OH"  -0.7009705

add ${selection} O
bond ${selection}.P ${selection}.O
select ${selection}.O
relax ${selection}
deselect ${selection}.O

add ${selection} H
bond ${selection}.O ${selection}.H
select ${selection}.H
relax ${selection}
deselect ${selection}.H

select ${selection}.O
select ${selection}.H
relax ${selection}
deselect ${selection}.O
deselect ${selection}.H

EOF
    
}



function extract_dna_nucleobase
{
    local selection="$1"
    cat <<EOF
remove ${selection} ${selection}.P
remove ${selection} ${selection}.OP1
remove ${selection} ${selection}.OP2
remove ${selection} ${selection}.O5'
remove ${selection} ${selection}.C5'
remove ${selection} ${selection}.H5'
remove ${selection} ${selection}.H5''
remove ${selection} ${selection}.C4'
remove ${selection} ${selection}.H4'
remove ${selection} ${selection}.O4'
remove ${selection} ${selection}.C1'
remove ${selection} ${selection}.H1'
remove ${selection} ${selection}.C3'
remove ${selection} ${selection}.H3'
remove ${selection} ${selection}.C2'
remove ${selection} ${selection}.H2'
remove ${selection} ${selection}.O3'
##########################
# DNA
remove ${selection} ${selection}.H2''
# RNA
#remove ${selection} ${selection}.O2'
#remove ${selection} ${selection}.HO2'
##########################
EOF
}


function extract_rna_nucleobase
{
    local selection="$1"
    cat <<EOF
remove ${selection} ${selection}.P
remove ${selection} ${selection}.OP1
remove ${selection} ${selection}.OP2
remove ${selection} ${selection}.O5'
remove ${selection} ${selection}.C5'
remove ${selection} ${selection}.H5'
remove ${selection} ${selection}.H5''
remove ${selection} ${selection}.C4'
remove ${selection} ${selection}.H4'
remove ${selection} ${selection}.O4'
remove ${selection} ${selection}.C1'
remove ${selection} ${selection}.H1'
remove ${selection} ${selection}.C3'
remove ${selection} ${selection}.H3'
remove ${selection} ${selection}.C2'
remove ${selection} ${selection}.H2'
remove ${selection} ${selection}.O3'
##########################
# DNA
#remove ${selection} ${selection}.H2''
# RNA
remove ${selection} ${selection}.O2'
remove ${selection} ${selection}.HO2'
##########################
EOF
}


function methylate_DG_N9
{
    local selection="$1"
cat <<EOF
C9 = createAtom "C9" "CA" -0.2403
add ${selection} C9
bond ${selection}.N9 ${selection}.C9
select ${selection}.C9
relax ${selection}
deselect ${selection}.C9

H91 = createAtom "H91" "HA" 0.109707
add ${selection} H91
bond ${selection}.C9 ${selection}.H91
select ${selection}.H91
relax ${selection}
deselect ${selection}.H91

H92 = createAtom "H92" "HA" 0.109707
add ${selection} H92
bond ${selection}.C9 ${selection}.H92
select ${selection}.H92
relax ${selection}
deselect ${selection}.H92

H93 = createAtom "H93" "HA" 0.109707
add ${selection} H93
bond ${selection}.C9 ${selection}.H93
select ${selection}.H93
relax ${selection}
deselect ${selection}.H93

select ${selection}.H91
select ${selection}.H92
select ${selection}.H93
relax ${selection}
deselect ${selection}.H91
deselect ${selection}.H92
deselect ${selection}.H93
EOF
}




function methylate_G_N9
{
    local selection="$1"
cat <<EOF
C9 = createAtom "C9" "CA" -0.1789
add ${selection} C9
bond ${selection}.N9 ${selection}.C9
select ${selection}.C9
relax ${selection}
deselect ${selection}.C9

H91 = createAtom "H91" "HA" 0.094193
add ${selection} H91
bond ${selection}.C9 ${selection}.H91
select ${selection}.H91
relax ${selection}
deselect ${selection}.H91

H92 = createAtom "H92" "HA" 0.094193
add ${selection} H92
bond ${selection}.C9 ${selection}.H92
select ${selection}.H92
relax ${selection}
deselect ${selection}.H92

H93 = createAtom "H93" "HA" 0.094193
add ${selection} H93
bond ${selection}.C9 ${selection}.H93
select ${selection}.H93
relax ${selection}
deselect ${selection}.H93


select ${selection}.H91
select ${selection}.H92
select ${selection}.H93
relax ${selection}
deselect ${selection}.H91
deselect ${selection}.H92
deselect ${selection}.H93
EOF
}




###################################################
# DNA guanine nucleobase (methylated @ N9)
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
DG = sequence { DG }
set DG   name "DG"
set DG.1 name "DG"
EOF

extract_dna_nucleobase "DG.1" >> tleap.in

cat <<EOF >> tleap.in
saveamberparm DG DG.parm7 DG.rst7
EOF

methylate_DG_N9 "DG.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 DG DG.mol2 1
savepdb  DG DG.pdb
quit
EOF

tleap -s -f tleap.in


#
# make DG.log from DG.mol2
#
RunRespCalcs DG.mol2



###################################################
# RNA guanine nucleobase (methylated @ N9)
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
G = sequence { G }
set G   name "G"
set G.1 name "G"
EOF

extract_rna_nucleobase "G.1" >> tleap.in


cat <<EOF >> tleap.in
saveamberparm G G.parm7 G.rst7
EOF

methylate_G_N9 "G.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 G G.mol2 1
savepdb  G G.pdb
quit
EOF

tleap -s -f tleap.in


#
# make G.log from G.mol2
#
RunRespCalcs G.mol2



###################################################
# DNA guanine deporotonated N1
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in

DGF = sequence { DG }

set DGF   name "DGF"
set DGF.1 name "DGF"

remove DGF.1 DGF.1.H1
set DGF.1.N1 charge -1.1533

EOF

extract_dna_nucleobase "DGF.1" >> tleap.in
methylate_DG_N9 "DGF.1" >> tleap.in

cat<<EOF >> tleap.in
#saveamberparm DGF DGF.parm7 DGF.rst7
savemol2 DGF DGF.mol2 1
savepdb  DGF DGF.pdb
quit
EOF
tleap -s -f tleap.in

#
# make DGF.log from DGF.mol2
#
RunRespCalcs DGF.mol2



###################################################
# RNA guanine deprotonated N1 (methylated @ N9)
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in

GF = sequence { G }

set GF   name "GF"
set GF.1 name "GF"

remove GF.1 GF.1.H1
set GF.1.N1 charge -1.1363

EOF

extract_rna_nucleobase "GF.1" >> tleap.in
methylate_G_N9 "GF.1" >> tleap.in

cat<<EOF >> tleap.in
#saveamberparm GF GF.parm7 GF.rst7
savemol2 GF GF.mol2 1
savepdb GF GF.pdb
quit
EOF
tleap -s -f tleap.in



#
# make GF.log from GF.mol2
#
RunRespCalcs GF.mol2





##################################################
# CHARGE FITS
##################################################

parmutils-GetAtomChargeCmds.py  G.parm7 ':1' | sed -e 's|FOO|GF_native|'  >  charge_fit_functions.sh
parmutils-GetAtomChargeCmds.py DG.parm7 ':1' | sed -e 's|FOO|DGF_native|' >> charge_fit_functions.sh

rm  G.parm7  G.rst7
rm DG.parm7 DG.rst7

cat <<'EOF' | python2.7 | sed -e 's|_1|_deprotN1|' >> charge_fit_functions.sh
import parmutils
import parmutils.respfit as rf

def GetLogFiles(mol2):
    import glob
    base=mol2.replace(".mol2","")
    return glob.glob("%s_*.log"%(base))

if __name__ == "__main__":
    comp = parmutils.BASH( 12 )
    
    ###############################################################
    # RNA
    ###############################################################

    native = rf.ResidueResp( comp, 1 )
    native.add_state( "G", "G.mol2", GetLogFiles("G.mol2") )
    #  ** AUXILIARY CAP **
    native.states[-1].add_group_restraint( "@C9,H91,H92,H93" )
    #  ** DELETED ATOMS **
    native.states[-1].add_group_restraint( "@H1" )

    native.multimolecule_fit(True) 
    native.perform_fit("@*",unique_residues=False)
    native.preserve_residue_charges_by_shifting()
    #native.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #native.print_resp()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    model = rf.ResidueResp( comp, 1 )
    model.add_state( "GF", "GF.mol2", GetLogFiles("GF.mol2") )
    #   ** AUXILIARY CAP **
    model.states[-1].add_group_restraint( "@C9,H91,H92,H93" )

    model.multimolecule_fit(True) 
    model.perform_fit("@*",unique_residues=False)
    model.preserve_residue_charges_by_shifting()
    #model.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #model.print_resp()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    rf.PrintPerturbedCharges(native,model,extra=["H1"],exclude=["C9","H91","H92","H93"])

    ###############################################################
    # DNA
    ###############################################################

    native = rf.ResidueResp( comp, 1 )
    native.add_state( "DG", "DG.mol2", GetLogFiles("DG.mol2") )
    #  ** AUXILIARY CAP **
    native.states[-1].add_group_restraint( "@C9,H91,H92,H93" )
    #  ** DELETED ATOMS **
    native.states[-1].add_group_restraint( "@H1" )

    native.multimolecule_fit(True) 
    native.perform_fit("@*",unique_residues=False)
    native.preserve_residue_charges_by_shifting()
    #native.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #native.print_resp()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    model = rf.ResidueResp( comp, 1 )
    model.add_state( "DGF", "DGF.mol2", GetLogFiles("DGF.mol2") )
    # ** AUXILIARY CAP **
    model.states[-1].add_group_restraint( "@C9,H91,H92,H93" )

    model.multimolecule_fit(True) 
    model.perform_fit("@*",unique_residues=False)
    model.preserve_residue_charges_by_shifting()
    #model.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #model.print_resp()

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    rf.PrintPerturbedCharges(native,model,extra=["H1"],exclude=["C9","H91","H92","H93"])

EOF




source charge_fit_functions.sh




start_leaprc > tleap.in
cat <<EOF >> tleap.in

DGF = sequence { DG }
set DGF   name "DGF"
set DGF.1 name "DGF"
set DGF.1 restype  nucleic
set DGF.1 connect0 DGF.1.P
set DGF.1 connect1 DGF.1.O3'
set DGF   head     DGF.1.P
set DGF   tail     DGF.1.O3'
set DGF.1.H1 type "HD"
EOF

#
# Our RESP-fit charges
#
set_charge_DGF_native "DGF.1" >> tleap.in
#set_charge_DGF_deprotN1 "DGF.1" >> tleap.in

cat <<EOF >> tleap.in

savepdb  DGF DGF.pdb
saveoff  DGF DGF.lib
savemol2 DGF DGF.mol2 1
saveamberparm DGF DGF.parm7 DGF.rst7



## RNA

GF = sequence { G }
set GF   name "GF"
set GF.1 name "GF"
set GF.1 restype  nucleic
set GF.1 connect0 GF.1.P
set GF.1 connect1 GF.1.O3'
set GF   head     GF.1.P
set GF   tail     GF.1.O3'
set GF.1.H1 type "HD"
EOF

#
# Our RESP-fit charges
#
set_charge_GF_native "GF.1" >> tleap.in
#set_charge_GF_deprotN1 "GF.1" >> tleap.in

cat <<EOF >> tleap.in

savepdb  GF GF.pdb
saveoff  GF GF.lib
savemol2 GF GF.mol2 1
saveamberparm GF GF.parm7 GF.rst7


quit
EOF

tleap -s -f tleap.in



for f in *.amber.* *.chk multistate.* leap.log tleap.in *.com G.mol2 G.pdb DG.mol2 DG.pdb *.parm7 *.rst7; do
    if [ -e ${f} ]; then
	rm ${f}
    fi
done





###################################################
###################################################
# Make the reference mononucleotide molecules
###################################################
###################################################

start_leaprc > tleap.in
cat <<EOF >> tleap.in

DGM = sequence { DG }

set DGM name "DGM"
set DGM.1 name "DGM"

EOF

add_dna_OH "DGM.1" >> tleap.in

cat <<EOF >> tleap.in

set DGM.1.H1 type "HD"

set DGM.1.O3' type "OH"
set DGM.1.O3' pertType "OH"
HO3p = createAtom "HO3'" "HO" 0.307900
add DGM.1 HO3p
bond DGM.1.O3' DGM.1.HO3'
select DGM.1.HO3'
relax DGM
deselect DGM.1.HO3'

set DGM head null
set DGM tail null
set DGM.1 connect0 null
set DGM.1 connect1 null

savepdb  DGM DGM.pdb
savemol2 DGM DGM.mol2 1
saveoff  DGM DGM.lib
saveamberparm DGM DGM.parm7 DGM.rst7




GM = sequence { G }

set GM   name "GM"
set GM.1 name "GM"

EOF

add_rna_OH "GM.1" >> tleap.in

cat <<EOF >> tleap.in

set GM.1.H1 type "HD"

set GM.1.O3' type "OH"
set GM.1.O3' pertType "OH"
HO3p = createAtom "HO3'" "HO" 0.308100
add GM.1 HO3p
bond GM.1.O3' GM.1.HO3'
select GM.1.HO3'
relax GM
deselect GM.1.HO3'


set GM head null
set GM tail null
set GM.1 connect0 null
set GM.1 connect1 null

savepdb  GM GM.pdb
savemol2 GM GM.mol2 1
saveoff  GM GM.lib
saveamberparm GM GM.parm7 GM.rst7

quit
EOF

tleap -s -f tleap.in
rm tleap.in
rm leap.log
rm *.parm7

