#!/bin/bash
set -e
set -u

source ${HOME}/devel/git/DataBase/CustomResidues/leap/bashrc



function RunRespCalcs
{
    local mol2="$1"
    local base=${mol2%.mol2}
    if [ ! -e ${base}.log ]; then
	parmutils-mol22g09.py --run ${base}.mol2
    fi
    for alp in 0 30 60 90 120 150 180; do
	for bet in 0 30 60 90; do
	    f=$(printf "%s_%.2f_%.2f_%.2f.log" ${base} ${alp} ${bet} 0)
	    if [ ! -e ${f} ]; then
		parmutils-rotate-g09-resp.py --alp ${alp} --bet ${bet} --run ${base}.log
	    fi
	done
    done
}



function add_dna_OH
{
    local selection="$1"
    cat <<EOF

H = createAtom "H" "H*"   0.3929705
O = createAtom "O" "OH"  -0.7008705

add ${selection} O
bond ${selection}.P ${selection}.O
select ${selection}.O
relax ${selection}
deselect ${selection}.O

add ${selection} H
bond ${selection}.O ${selection}.H
select ${selection}.H
relax ${selection}
deselect ${selection}.H

select ${selection}.O
select ${selection}.H
relax ${selection}
deselect ${selection}.O
deselect ${selection}.H

EOF
    
}


function add_rna_OH
{
    local selection="$1"
    cat <<EOF

H = createAtom "H" "H*"   0.3928705
O = createAtom "O" "OH"  -0.7009705

add ${selection} O
bond ${selection}.P ${selection}.O
select ${selection}.O
relax ${selection}
deselect ${selection}.O

add ${selection} H
bond ${selection}.O ${selection}.H
select ${selection}.H
relax ${selection}
deselect ${selection}.H

select ${selection}.O
select ${selection}.H
relax ${selection}
deselect ${selection}.O
deselect ${selection}.H

EOF
    
}





function extract_dna_nucleobase
{
    local selection="$1"
    cat <<EOF
remove ${selection} ${selection}.P
remove ${selection} ${selection}.OP1
remove ${selection} ${selection}.OP2
remove ${selection} ${selection}.O5'
remove ${selection} ${selection}.C5'
remove ${selection} ${selection}.H5'
remove ${selection} ${selection}.H5''
remove ${selection} ${selection}.C4'
remove ${selection} ${selection}.H4'
remove ${selection} ${selection}.O4'
remove ${selection} ${selection}.C1'
remove ${selection} ${selection}.H1'
remove ${selection} ${selection}.C3'
remove ${selection} ${selection}.H3'
remove ${selection} ${selection}.C2'
remove ${selection} ${selection}.H2'
remove ${selection} ${selection}.O3'
##########################
# DNA
remove ${selection} ${selection}.H2''
# RNA
#remove ${selection} ${selection}.O2'
#remove ${selection} ${selection}.HO2'
##########################
EOF
}


function extract_rna_nucleobase
{
    local selection="$1"
    cat <<EOF
remove ${selection} ${selection}.P
remove ${selection} ${selection}.OP1
remove ${selection} ${selection}.OP2
remove ${selection} ${selection}.O5'
remove ${selection} ${selection}.C5'
remove ${selection} ${selection}.H5'
remove ${selection} ${selection}.H5''
remove ${selection} ${selection}.C4'
remove ${selection} ${selection}.H4'
remove ${selection} ${selection}.O4'
remove ${selection} ${selection}.C1'
remove ${selection} ${selection}.H1'
remove ${selection} ${selection}.C3'
remove ${selection} ${selection}.H3'
remove ${selection} ${selection}.C2'
remove ${selection} ${selection}.H2'
remove ${selection} ${selection}.O3'
##########################
# DNA
#remove ${selection} ${selection}.H2''
# RNA
remove ${selection} ${selection}.O2'
remove ${selection} ${selection}.HO2'
##########################
EOF
}


function methylate_DA_N9
{
    local selection="$1"
cat <<EOF
C9 = createAtom "C9" "CA" -0.223821
add ${selection} C9
bond ${selection}.N9 ${selection}.C9
select ${selection}.C9
relax ${selection}
deselect ${selection}.C9

H91 = createAtom "H91" "HA" 0.109707
add ${selection} H91
bond ${selection}.C9 ${selection}.H91
select ${selection}.H91
relax ${selection}
deselect ${selection}.H91

H92 = createAtom "H92" "HA" 0.109707
add ${selection} H92
bond ${selection}.C9 ${selection}.H92
select ${selection}.H92
relax ${selection}
deselect ${selection}.H92

H93 = createAtom "H93" "HA" 0.109707
add ${selection} H93
bond ${selection}.C9 ${selection}.H93
select ${selection}.H93
relax ${selection}
deselect ${selection}.H93

select ${selection}.H91
select ${selection}.H92
select ${selection}.H93
relax ${selection}
deselect ${selection}.H91
deselect ${selection}.H92
deselect ${selection}.H93
EOF
}




function methylate_A_N9
{
    local selection="$1"
cat <<EOF
C9 = createAtom "C9" "CA" -0.158480
add ${selection} C9
bond ${selection}.N9 ${selection}.C9
select ${selection}.C9
relax ${selection}
deselect ${selection}.C9

H91 = createAtom "H91" "HA" 0.094193
add ${selection} H91
bond ${selection}.C9 ${selection}.H91
select ${selection}.H91
relax ${selection}
deselect ${selection}.H91

H92 = createAtom "H92" "HA" 0.094193
add ${selection} H92
bond ${selection}.C9 ${selection}.H92
select ${selection}.H92
relax ${selection}
deselect ${selection}.H92

H93 = createAtom "H93" "HA" 0.094193
add ${selection} H93
bond ${selection}.C9 ${selection}.H93
select ${selection}.H93
relax ${selection}
deselect ${selection}.H93


select ${selection}.H91
select ${selection}.H92
select ${selection}.H93
relax ${selection}
deselect ${selection}.H91
deselect ${selection}.H92
deselect ${selection}.H93
EOF
}






###################################################
# DNA adenine nucleobase (methylated @ N9)
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
DA = sequence { DA }
set DA name "DA"
set DA.1 name "DA"
EOF

extract_dna_nucleobase "DA.1" >> tleap.in
methylate_DA_N9 "DA.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 DA DA.mol2 1
quit
EOF

tleap -s -f tleap.in


#
# make DA.log from DA.mol2
#
RunRespCalcs DA.mol2



###################################################
# RNA adenine nucleobase (methylated @ N9)
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
A = sequence { A }
set A name "A"
set A.1 name "A"
EOF

extract_rna_nucleobase "A.1" >> tleap.in
methylate_A_N9 "A.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 A A.mol2 1
quit
EOF

tleap -s -f tleap.in


#
# make A.log from A.mol2
#
RunRespCalcs A.mol2







###################################################
# protonated-N1 DNA adenine nucleobase
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
DAF = sequence { DA }
set DAF name "DAF"
set DAF.1 name "DAF"

H1 = createAtom "H1" "HD" 1.0
add DAF.1 H1
bond DAF.1.N1 DAF.1.H1
select DAF.1.H1
relax DAF
deselect DAF.1.H1

EOF

extract_dna_nucleobase "DAF.1" >> tleap.in
methylate_DA_N9 "DAF.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 DAF DAF.mol2 1
quit
EOF

tleap -s -f tleap.in

#
# make DAF.log from DAF.mol2
#
RunRespCalcs DAF.mol2




#
# The mol2 format chops charges at 0.0001, I make a parm7 file
# for the sole purpose of extracting the native MM charges
#
start_leaprc > tleap.in
cat <<EOF >> tleap.in
DAF = sequence { DA }
set DAF name "DAF"
set DAF.1 name "DAF"

H1 = createAtom "H1" "HD" 0
add DAF.1 H1
bond DAF.1.N1 DAF.1.H1
select DAF.1.H1
relax DAF
deselect DAF.1.H1

H3 = createAtom "H3" "HD" 0
add DAF.1 H3
bond DAF.1.N3 DAF.1.H3
select DAF.1.H3
relax DAF
deselect DAF.1.H3

H7 = createAtom "H7" "HD" 0
add DAF.1 H7
bond DAF.1.N7 DAF.1.H7
select DAF.1.H7
relax DAF
deselect DAF.1.H7

EOF

extract_dna_nucleobase "DAF.1" >> tleap.in

cat <<EOF >> tleap.in
saveamberparm DAF DAF_base.parm7 DAF_base.rst7
quit
EOF

tleap -s -f tleap.in








###################################################
# protonated-N3 DNA adenine nucleobase
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
DA3 = sequence { DA }
set DA3 name "DA3"
set DA3.1 name "DA3"

H3 = createAtom "H3" "HD" 1.0
add DA3.1 H3
bond DA3.1.N3 DA3.1.H3
select DA3.1.H3
relax DA3
deselect DA3.1.H3
EOF

extract_dna_nucleobase "DA3.1" >> tleap.in
methylate_DA_N9 "DA3.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 DA3 DA3.mol2 1
quit
EOF

tleap -s -f tleap.in


#
# make DA3.log from DA3.mol2
#
RunRespCalcs DA3.mol2




###################################################
# protonated-N7 DNA adenine nucleobase
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
DA7 = sequence { DA }
set DA7 name "DA7"
set DA7.1 name "DA7"

H7 = createAtom "H7" "HD" 1.0
add DA7.1 H7
bond DA7.1.N7 DA7.1.H7
select DA7.1.H7
relax DA7
deselect DA7.1.H7
EOF

extract_dna_nucleobase "DA7.1" >> tleap.in
methylate_DA_N9 "DA7.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 DA7 DA7.mol2 1
quit
EOF

tleap -s -f tleap.in


#
# make DA7.log from DA7.mol2
#
RunRespCalcs DA7.mol2








###################################################
# protonated-N1 RNA adenine nucleobase
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
AF = sequence { A }
set AF name "AF"
set AF.1 name "AF"

H1 = createAtom "H1" "HD" 1.0
add AF.1 H1
bond AF.1.N1 AF.1.H1
select AF.1.H1
relax AF
deselect AF.1.H1
EOF

extract_rna_nucleobase "AF.1" >> tleap.in
methylate_A_N9 "AF.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 AF AF.mol2 1
quit
EOF

tleap -s -f tleap.in


#
# make AF.log from AF.mol2
#
RunRespCalcs AF.mol2





#
# The mol2 format chops charges at 0.0001, I make a parm7 file
# for the sole purpose of extracting the native MM charges
#
start_leaprc > tleap.in
cat <<EOF >> tleap.in
AF = sequence { A }
set AF name "AF"
set AF.1 name "AF"

H1 = createAtom "H1" "HD" 0
add AF.1 H1
bond AF.1.N1 AF.1.H1
select AF.1.H1
relax AF
deselect AF.1.H1

H3 = createAtom "H3" "HD" 0
add AF.1 H3
bond AF.1.N3 AF.1.H3
select AF.1.H3
relax AF
deselect AF.1.H3

H7 = createAtom "H7" "HD" 0
add AF.1 H7
bond AF.1.N7 AF.1.H7
select AF.1.H7
relax AF
deselect AF.1.H7

EOF

extract_dna_nucleobase "AF.1" >> tleap.in

cat <<EOF >> tleap.in
saveamberparm AF AF_base.parm7 AF_base.rst7
quit
EOF

tleap -s -f tleap.in



###################################################
# protonated-N3 RNA adenine nucleobase
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
A3 = sequence { A }
set A3 name "A3"
set A3.1 name "A3"

H3 = createAtom "H3" "HD" 1.0
add A3.1 H3
bond A3.1.N3 A3.1.H3
select A3.1.H3
relax A3
deselect A3.1.H3
EOF

extract_rna_nucleobase "A3.1" >> tleap.in
methylate_A_N9 "A3.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 A3 A3.mol2 1
quit
EOF

tleap -s -f tleap.in


#
# make A3.log from A3.mol2
#
RunRespCalcs A3.mol2





###################################################
# protonated-N7 RNA adenine nucleobase
###################################################


start_leaprc > tleap.in
cat <<EOF >> tleap.in
A7 = sequence { A }
set A7 name "A7"
set A7.1 name "A7"

H7 = createAtom "H7" "HD" 1.0
add A7.1 H7
bond A7.1.N7 A7.1.H7
select A7.1.H7
relax A7
deselect A7.1.H7
EOF

extract_rna_nucleobase "A7.1" >> tleap.in
methylate_A_N9 "A7.1" >> tleap.in

cat <<EOF >> tleap.in
savemol2 A7 A7.mol2 1
quit
EOF

tleap -s -f tleap.in


#
# make A7.log from A7.mol2
#
RunRespCalcs A7.mol2







##################################################
# CHARGE FITS
##################################################


parmutils-GetAtomChargeCmds.py  AF_base.parm7 ':1' | sed -e 's|FOO|AF_native|'  > charge_fit_functions.sh
parmutils-GetAtomChargeCmds.py DAF_base.parm7 ':1' | sed -e 's|FOO|DAF_native|' >> charge_fit_functions.sh

rm -f AF_base*
rm -f DAF_base*

cat <<'EOF' | python2.7 | sed -e 's|DAF_1|DAF_protN1|' -e 's|AF_1|AF_protN1|' -e 's|DA3_1|DAF_protN3|' -e 's|A3_1|AF_protN3|' -e 's|DA7_1|DAF_protN7|' -e 's|A7_1|AF_protN7|' >> charge_fit_functions.sh
import parmutils
import parmutils.respfit as rf

def GetLogFiles(mol2):
    import glob
    base=mol2.replace(".mol2","")
    return glob.glob("%s_*.log"%(base))

if __name__ == "__main__":
    comp = parmutils.BASH( 12 )
    
    ###############################################################
    # RNA
    ###############################################################

    native = rf.ResidueResp( comp, 1 )
    native.add_state( "A", "A.mol2", GetLogFiles("A.mol2") )
    #  ** AUXILIARY CAP **
    native.states[-1].add_group_restraint( "@C9,H91,H92,H93" )
    #  ** DELETED ATOMS **
    # native.states[-1].add_group_restraint( "" )

    native.multimolecule_fit(True) 
    native.perform_fit("@*",unique_residues=False)
    native.preserve_residue_charges_by_shifting()
    #native.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #native.print_resp()

    for res in [ "AF", "A3", "A7" ]:
       model = rf.ResidueResp( comp, 1 )
       model.add_state( res, res+".mol2", GetLogFiles(res+".mol2") )
       #  ** AUXILIARY CAP **
       model.states[-1].add_group_restraint( "@C9,H91,H92,H93" )

       model.multimolecule_fit(True) 
       model.perform_fit("@*",unique_residues=False)
       model.preserve_residue_charges_by_shifting()
       #model.preserve_mm_charges_by_shifting("@P,OP1,OP2")
       #model.print_resp()

       rf.PrintPerturbedCharges(native,model,extra=["H1","H3","H7"],exclude=["C9","H91","H92","H93"])


    ###############################################################
    # DNA
    ###############################################################

    native = rf.ResidueResp( comp, 1 )
    native.add_state( "DA", "DA.mol2", GetLogFiles("DA.mol2") )
    #  ** AUXILIARY CAP **
    native.states[-1].add_group_restraint( "@C9,H91,H92,H93" )
    #  ** DELETED ATOMS **
    # native.states[-1].add_group_restraint( "" )

    native.multimolecule_fit(True) 
    native.perform_fit("@*",unique_residues=False)
    native.preserve_residue_charges_by_shifting()
    #native.preserve_mm_charges_by_shifting("@P,OP1,OP2")
    #native.print_resp()

    for res in [ "DAF", "DA3", "DA7" ]:
       model = rf.ResidueResp( comp, 1 )
       model.add_state( res, res+".mol2", GetLogFiles(res+".mol2") )
       #  ** AUXILIARY CAP **
       model.states[-1].add_group_restraint( "@C9,H91,H92,H93" )

       model.multimolecule_fit(True) 
       model.perform_fit("@*",unique_residues=False)
       model.preserve_residue_charges_by_shifting()
       #model.preserve_mm_charges_by_shifting("@P,OP1,OP2")
       #model.print_resp()

       rf.PrintPerturbedCharges(native,model,extra=["H1","H3","H7"],exclude=["C9","H91","H92","H93"])



EOF




source charge_fit_functions.sh

start_leaprc > tleap.in
cat <<EOF >> tleap.in

DAF = sequence { DA }

set DAF name "DAF"
set DAF.1 name "DAF"

H1 = createAtom "H1" "HD" 0.0000
add DAF.1 H1
bond DAF.1.N1 DAF.1.H1
select DAF.1.H1
relax DAF
deselect DAF.1.H1

H3 = createAtom "H3" "HD" 0.0000
add DAF.1 H3
bond DAF.1.N3 DAF.1.H3
select DAF.1.H3
relax DAF
deselect DAF.1.H3

H7 = createAtom "H7" "HD" 0.0000
add DAF.1 H7
bond DAF.1.N7 DAF.1.H7
select DAF.1.H7
relax DAF
deselect DAF.1.H7

savepdb  DAF DAF.pdb
savemol2 DAF DAF.mol2 1
saveoff  DAF DAF.lib



AF = sequence { A }

set AF   name "AF"
set AF.1 name "AF"

H1 = createAtom "H1" "HD" 0.0000
add AF.1 H1
bond AF.1.N1 AF.1.H1
select AF.1.H1
relax AF
deselect AF.1.H1

H3 = createAtom "H3" "HD" 0.0000
add AF.1 H3
bond AF.1.N3 AF.1.H3
select AF.1.H3
relax AF
deselect AF.1.H3

H7 = createAtom "H7" "HD" 0.0000
add AF.1 H7
bond AF.1.N7 AF.1.H7
select AF.1.H7
relax AF
deselect AF.1.H7

savepdb  AF AF.pdb
savemol2 AF AF.mol2 1
saveoff  AF AF.lib


quit
EOF


tleap -s -f tleap.in

for f in leap.log tleap.in *.com *.chk *.resp.* A.mol2 DA.mol2; do
    if [ -e ${f} ]; then
	rm ${f}
    fi
done




###################################################
###################################################
# Make the reference mononucleotide molecules
###################################################
###################################################

start_leaprc > tleap.in
cat <<EOF >> tleap.in

DAM = sequence { DA }

set DAM name "DAM"
set DAM.1 name "DAM"

EOF

add_dna_OH "DAM.1" >> tleap.in

cat <<EOF >> tleap.in

set DAM.1.O3' type "OH"
set DAM.1.O3' pertType "OH"
HO3p = createAtom "HO3'" "HO" 0.307900
add DAM.1 HO3p
bond DAM.1.O3' DAM.1.HO3'
select DAM.1.HO3'
relax DAM
deselect DAM.1.HO3'

H1 = createAtom "H1" "HD" 0.0000
add DAM.1 H1
bond DAM.1.N1 DAM.1.H1
select DAM.1.H1
relax DAM
deselect DAM.1.H1

H3 = createAtom "H3" "HD" 0.0000
add DAM.1 H3
bond DAM.1.N3 DAM.1.H3
select DAM.1.H3
relax DAM
deselect DAM.1.H3

H7 = createAtom "H7" "HD" 0.0000
add DAM.1 H7
bond DAM.1.N7 DAM.1.H7
select DAM.1.H7
relax DAM
deselect DAM.1.H7

set DAM head null
set DAM tail null
set DAM.1 connect0 null
set DAM.1 connect1 null

savepdb  DAM DAM.pdb
savemol2 DAM DAM.mol2 1
saveoff  DAM DAM.lib
saveamberparm DAM DAM.parm7 DAM.rst7




AM = sequence { A }

set AM   name "AM"
set AM.1 name "AM"

EOF

add_rna_OH "AM.1" >> tleap.in

cat <<EOF >> tleap.in

set AM.1.O3' type "OH"
set AM.1.O3' pertType "OH"
HO3p = createAtom "HO3'" "HO" 0.308100
add AM.1 HO3p
bond AM.1.O3' AM.1.HO3'
select AM.1.HO3'
relax AM
deselect AM.1.HO3'


H1 = createAtom "H1" "HD" 0.0000
add AM.1 H1
bond AM.1.N1 AM.1.H1
select AM.1.H1
relax AM
deselect AM.1.H1

H3 = createAtom "H3" "HD" 0.0000
add AM.1 H3
bond AM.1.N3 AM.1.H3
select AM.1.H3
relax AM
deselect AM.1.H3

H7 = createAtom "H7" "HD" 0.0000
add AM.1 H7
bond AM.1.N7 AM.1.H7
select AM.1.H7
relax AM
deselect AM.1.H7

set AM head null
set AM tail null
set AM.1 connect0 null
set AM.1 connect1 null

savepdb  AM AM.pdb
savemol2 AM AM.mol2 1
saveoff  AM AM.lib
saveamberparm AM AM.parm7 AM.rst7

quit
EOF

tleap -s -f tleap.in
rm tleap.in
rm leap.log
rm *.parm7

rm A3.mol2
rm A7.mol2

