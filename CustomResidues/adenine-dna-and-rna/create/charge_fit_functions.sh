
function set_charge_AF_native()
{
    selection="$1"
    cat <<EOF

set ${selection}.N9    charge -0.025100
set ${selection}.C8    charge  0.200600
set ${selection}.H8    charge  0.155300
set ${selection}.N7    charge -0.607300
set ${selection}.C5    charge  0.051500
set ${selection}.C6    charge  0.700900
set ${selection}.N6    charge -0.901900
set ${selection}.H61   charge  0.411500
set ${selection}.H62   charge  0.411500
set ${selection}.N1    charge -0.761500
set ${selection}.C2    charge  0.587500
set ${selection}.H2    charge  0.047300
set ${selection}.N3    charge -0.699700
set ${selection}.C4    charge  0.305300
set ${selection}.O2'   charge -0.613900
set ${selection}.HO2'  charge  0.418600
set ${selection}.H1    charge  0.000000
set ${selection}.H3    charge  0.000000
set ${selection}.H7    charge  0.000000
EOF
}


function set_charge_DAF_native()
{
    selection="$1"
    cat <<EOF

set ${selection}.N9    charge -0.026800
set ${selection}.C8    charge  0.160700
set ${selection}.H8    charge  0.187700
set ${selection}.N7    charge -0.617500
set ${selection}.C5    charge  0.072500
set ${selection}.C6    charge  0.689700
set ${selection}.N6    charge -0.912300
set ${selection}.H61   charge  0.416700
set ${selection}.H62   charge  0.416700
set ${selection}.N1    charge -0.762400
set ${selection}.C2    charge  0.571600
set ${selection}.H2    charge  0.059800
set ${selection}.N3    charge -0.741700
set ${selection}.C4    charge  0.380000
set ${selection}.H1    charge  0.000000
set ${selection}.H3    charge  0.000000
set ${selection}.H7    charge  0.000000
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N9       0.093652    0.118752    0.082586   -0.036166   -0.025100
# 1    C8       0.189679   -0.010921    0.068021    0.078942    0.200600
# 1    H8       0.208560    0.053260    0.222744    0.169484    0.155300
# 1    N7      -0.561602    0.045698   -0.486342   -0.532040   -0.607300
# 1    C5       0.167119    0.115619    0.121009    0.005390    0.051500
# 1    C6       0.435997   -0.264903    0.399232    0.664135    0.700900
# 1    N6      -0.795135    0.106765   -0.766987   -0.873752   -0.901900
# 1    H61      0.447580    0.036080    0.433457    0.397377    0.411500
# 1    H62      0.447580    0.036080    0.433457    0.397377    0.411500
# 1    N1      -0.345364    0.416136   -0.293878   -0.710014   -0.761500
# 1    C2       0.304387   -0.283113    0.201274    0.484387    0.587500
# 1    H2       0.166244    0.118944    0.191452    0.072508    0.047300
# 1    N3      -0.498842    0.200858   -0.456506   -0.657364   -0.699700
# 1    C4       0.261509   -0.043791    0.371844    0.415635    0.305300
# 1    H1       0.354541    0.354541    0.354541                        
# 1    C9      -0.411484   -0.252984   -0.402435   -0.149451   -0.158500
# 1    H91      0.178528    0.084328    0.175512    0.091184    0.094200
# 1    H92      0.178528    0.084328    0.175512    0.091184    0.094200
# 1    H93      0.178528    0.084328    0.175512    0.091184    0.094200
# 1    SUM      1.000000    1.000000    1.000000    0.000000    0.000000


function set_charge_AF_protN1
{
   selection="$1"
   cat <<EOF
set ${selection}.N9    charge   0.093652
set ${selection}.C8    charge   0.189679
set ${selection}.H8    charge   0.208560
set ${selection}.N7    charge  -0.561602
set ${selection}.C5    charge   0.167119
set ${selection}.C6    charge   0.435997
set ${selection}.N6    charge  -0.795135
set ${selection}.H61   charge   0.447580
set ${selection}.H62   charge   0.447580
set ${selection}.N1    charge  -0.345364
set ${selection}.C2    charge   0.304387
set ${selection}.H2    charge   0.166244
set ${selection}.N3    charge  -0.498842
set ${selection}.C4    charge   0.261509
set ${selection}.H1    charge   0.354541
set ${selection}.H3    charge   0.0
set ${selection}.H7    charge   0.0
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N9       0.105912    0.131012    0.094846   -0.036166   -0.025100
# 1    C8       0.258198    0.057598    0.136540    0.078942    0.200600
# 1    H8       0.192579    0.037279    0.206763    0.169484    0.155300
# 1    N7      -0.594826    0.012474   -0.519566   -0.532040   -0.607300
# 1    C5       0.177271    0.125771    0.131161    0.005390    0.051500
# 1    C6       0.711494    0.010594    0.674729    0.664135    0.700900
# 1    N6      -0.827133    0.074767   -0.798985   -0.873752   -0.901900
# 1    H61      0.444160    0.032660    0.430037    0.397377    0.411500
# 1    H62      0.444160    0.032660    0.430037    0.397377    0.411500
# 1    N1      -0.617501    0.143999   -0.566015   -0.710014   -0.761500
# 1    C2       0.429032   -0.158468    0.325919    0.484387    0.587500
# 1    H2       0.144360    0.097060    0.169568    0.072508    0.047300
# 1    N3      -0.363060    0.336640   -0.320724   -0.657364   -0.699700
# 1    C4       0.012353   -0.292947    0.122688    0.415635    0.305300
# 1    H3       0.358902    0.358902    0.358902                        
# 1    C9      -0.460793   -0.302293   -0.451744   -0.149451   -0.158500
# 1    H91      0.194964    0.100764    0.191948    0.091184    0.094200
# 1    H92      0.194964    0.100764    0.191948    0.091184    0.094200
# 1    H93      0.194964    0.100764    0.191948    0.091184    0.094200
# 1    SUM      1.000000    1.000000    1.000000    0.000000    0.000000


function set_charge_AF_protN3
{
   selection="$1"
   cat <<EOF
set ${selection}.N9    charge   0.105912
set ${selection}.C8    charge   0.258198
set ${selection}.H8    charge   0.192579
set ${selection}.N7    charge  -0.594826
set ${selection}.C5    charge   0.177271
set ${selection}.C6    charge   0.711494
set ${selection}.N6    charge  -0.827133
set ${selection}.H61   charge   0.444160
set ${selection}.H62   charge   0.444160
set ${selection}.N1    charge  -0.617501
set ${selection}.C2    charge   0.429032
set ${selection}.H2    charge   0.144360
set ${selection}.N3    charge  -0.363060
set ${selection}.C4    charge   0.012353
set ${selection}.H3    charge   0.358902
set ${selection}.H1    charge   0.0
set ${selection}.H7    charge   0.0
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N9       0.214772    0.239872    0.203706   -0.036166   -0.025100
# 1    C8       0.139419   -0.061181    0.017761    0.078942    0.200600
# 1    H8       0.242632    0.087332    0.256816    0.169484    0.155300
# 1    N7      -0.413360    0.193940   -0.338100   -0.532040   -0.607300
# 1    C5      -0.127931   -0.179431   -0.174041    0.005390    0.051500
# 1    C6       0.801136    0.100236    0.764371    0.664135    0.700900
# 1    N6      -0.978684   -0.076784   -0.950536   -0.873752   -0.901900
# 1    H61      0.464594    0.053094    0.450471    0.397377    0.411500
# 1    H62      0.464594    0.053094    0.450471    0.397377    0.411500
# 1    N1      -0.735634    0.025866   -0.684148   -0.710014   -0.761500
# 1    C2       0.680654    0.093154    0.577541    0.484387    0.587500
# 1    H2       0.075856    0.028556    0.101064    0.072508    0.047300
# 1    N3      -0.691928    0.007772   -0.649592   -0.657364   -0.699700
# 1    C4       0.319701    0.014401    0.430036    0.415635    0.305300
# 1    H7       0.420081    0.420081    0.420081                        
# 1    C9      -0.547175   -0.388675   -0.538126   -0.149451   -0.158500
# 1    H91      0.223758    0.129558    0.220742    0.091184    0.094200
# 1    H92      0.223758    0.129558    0.220742    0.091184    0.094200
# 1    H93      0.223758    0.129558    0.220742    0.091184    0.094200
# 1    SUM      1.000000    1.000000    1.000000    0.000000    0.000000


function set_charge_AF_protN7
{
   selection="$1"
   cat <<EOF
set ${selection}.N9    charge   0.214772
set ${selection}.C8    charge   0.139419
set ${selection}.H8    charge   0.242632
set ${selection}.N7    charge  -0.413360
set ${selection}.C5    charge  -0.127931
set ${selection}.C6    charge   0.801136
set ${selection}.N6    charge  -0.978684
set ${selection}.H61   charge   0.464594
set ${selection}.H62   charge   0.464594
set ${selection}.N1    charge  -0.735634
set ${selection}.C2    charge   0.680654
set ${selection}.H2    charge   0.075856
set ${selection}.N3    charge  -0.691928
set ${selection}.C4    charge   0.319701
set ${selection}.H7    charge   0.420081
set ${selection}.H1    charge   0.0
set ${selection}.H3    charge   0.0
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N9       0.093956    0.120756    0.115252   -0.005504   -0.026800
# 1    C8       0.147957   -0.012743    0.053837    0.066580    0.160700
# 1    H8       0.241129    0.053429    0.226414    0.172985    0.187700
# 1    N7      -0.570558    0.046942   -0.481623   -0.528565   -0.617500
# 1    C5       0.186543    0.114043    0.113909   -0.000134    0.072500
# 1    C6       0.426879   -0.262821    0.404184    0.667005    0.689700
# 1    N6      -0.806708    0.105592   -0.768380   -0.873972   -0.912300
# 1    H61      0.452987    0.036287    0.433589    0.397302    0.416700
# 1    H62      0.452987    0.036287    0.433589    0.397302    0.416700
# 1    N1      -0.347120    0.415280   -0.295626   -0.710906   -0.762400
# 1    C2       0.289680   -0.281920    0.203468    0.485388    0.571600
# 1    H2       0.178467    0.118667    0.191008    0.072341    0.059800
# 1    N3      -0.541316    0.200384   -0.458031   -0.658415   -0.741700
# 1    C4       0.335260   -0.044740    0.368552    0.413292    0.380000
# 1    H1       0.354560    0.354560    0.354560                        
# 1    C9      -0.477283   -0.253483   -0.468424   -0.214941   -0.223800
# 1    H91      0.194194    0.084494    0.191241    0.106747    0.109700
# 1    H92      0.194194    0.084494    0.191241    0.106747    0.109700
# 1    H93      0.194194    0.084494    0.191241    0.106747    0.109700
# 1    SUM      1.000000    1.000000    1.000000    0.000000    0.000000


function set_charge_DAF_protN1
{
   selection="$1"
   cat <<EOF
set ${selection}.N9    charge   0.093956
set ${selection}.C8    charge   0.147957
set ${selection}.H8    charge   0.241129
set ${selection}.N7    charge  -0.570558
set ${selection}.C5    charge   0.186543
set ${selection}.C6    charge   0.426879
set ${selection}.N6    charge  -0.806708
set ${selection}.H61   charge   0.452987
set ${selection}.H62   charge   0.452987
set ${selection}.N1    charge  -0.347120
set ${selection}.C2    charge   0.289680
set ${selection}.H2    charge   0.178467
set ${selection}.N3    charge  -0.541316
set ${selection}.C4    charge   0.335260
set ${selection}.H1    charge   0.354560
set ${selection}.H3    charge   0.0
set ${selection}.H7    charge   0.0
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N9       0.106182    0.132982    0.127478   -0.005504   -0.026800
# 1    C8       0.224553    0.063853    0.130433    0.066580    0.160700
# 1    H8       0.221544    0.033844    0.206829    0.172985    0.187700
# 1    N7      -0.606675    0.010825   -0.517740   -0.528565   -0.617500
# 1    C5       0.199436    0.126936    0.126802   -0.000134    0.072500
# 1    C6       0.703817    0.014117    0.681122    0.667005    0.689700
# 1    N6      -0.840601    0.071699   -0.802273   -0.873972   -0.912300
# 1    H61      0.450125    0.033425    0.430727    0.397302    0.416700
# 1    H62      0.450125    0.033425    0.430727    0.397302    0.416700
# 1    N1      -0.620003    0.142397   -0.568509   -0.710906   -0.762400
# 1    C2       0.416053   -0.155547    0.329841    0.485388    0.571600
# 1    H2       0.155901    0.096101    0.168442    0.072341    0.059800
# 1    N3      -0.405325    0.336375   -0.322040   -0.658415   -0.741700
# 1    C4       0.079179   -0.300821    0.112471    0.413292    0.380000
# 1    H3       0.360391    0.360391    0.360391                        
# 1    C9      -0.525096   -0.301296   -0.516237   -0.214941   -0.223800
# 1    H91      0.210132    0.100432    0.207179    0.106747    0.109700
# 1    H92      0.210132    0.100432    0.207179    0.106747    0.109700
# 1    H93      0.210132    0.100432    0.207179    0.106747    0.109700
# 1    SUM      1.000000    1.000000    1.000000    0.000000    0.000000


function set_charge_DAF_protN3
{
   selection="$1"
   cat <<EOF
set ${selection}.N9    charge   0.106182
set ${selection}.C8    charge   0.224553
set ${selection}.H8    charge   0.221544
set ${selection}.N7    charge  -0.606675
set ${selection}.C5    charge   0.199436
set ${selection}.C6    charge   0.703817
set ${selection}.N6    charge  -0.840601
set ${selection}.H61   charge   0.450125
set ${selection}.H62   charge   0.450125
set ${selection}.N1    charge  -0.620003
set ${selection}.C2    charge   0.416053
set ${selection}.H2    charge   0.155901
set ${selection}.N3    charge  -0.405325
set ${selection}.C4    charge   0.079179
set ${selection}.H3    charge   0.360391
set ${selection}.H1    charge   0.0
set ${selection}.H7    charge   0.0
EOF
}

# ResidueResp.perform_fit writing multifit multistate.1.resp.inp
# Res  Atom        New Q     delta Q    New Resp   Nat. Resp      Nat. Q
# 1    N9       0.218780    0.245580    0.240076   -0.005504   -0.026800
# 1    C8       0.106044   -0.054656    0.011924    0.066580    0.160700
# 1    H8       0.271487    0.083787    0.256772    0.172985    0.187700
# 1    N7      -0.430321    0.187179   -0.341386   -0.528565   -0.617500
# 1    C5      -0.096822   -0.169322   -0.169456   -0.000134    0.072500
# 1    C6       0.789611    0.099911    0.766916    0.667005    0.689700
# 1    N6      -0.991318   -0.079018   -0.952990   -0.873972   -0.912300
# 1    H61      0.470337    0.053637    0.450939    0.397302    0.416700
# 1    H62      0.470337    0.053637    0.450939    0.397302    0.416700
# 1    N1      -0.736299    0.026101   -0.684805   -0.710906   -0.762400
# 1    C2       0.665895    0.094295    0.579683    0.485388    0.571600
# 1    H2       0.087439    0.027639    0.099980    0.072341    0.059800
# 1    N3      -0.728534    0.013166   -0.645249   -0.658415   -0.741700
# 1    C4       0.376584   -0.003416    0.409876    0.413292    0.380000
# 1    H7       0.421482    0.421482    0.421482                        
# 1    C9      -0.611487   -0.387687   -0.602628   -0.214941   -0.223800
# 1    H91      0.238929    0.129229    0.235976    0.106747    0.109700
# 1    H92      0.238929    0.129229    0.235976    0.106747    0.109700
# 1    H93      0.238929    0.129229    0.235976    0.106747    0.109700
# 1    SUM      1.000000    1.000000    1.000000    0.000000    0.000000


function set_charge_DAF_protN7
{
   selection="$1"
   cat <<EOF
set ${selection}.N9    charge   0.218780
set ${selection}.C8    charge   0.106044
set ${selection}.H8    charge   0.271487
set ${selection}.N7    charge  -0.430321
set ${selection}.C5    charge  -0.096822
set ${selection}.C6    charge   0.789611
set ${selection}.N6    charge  -0.991318
set ${selection}.H61   charge   0.470337
set ${selection}.H62   charge   0.470337
set ${selection}.N1    charge  -0.736299
set ${selection}.C2    charge   0.665895
set ${selection}.H2    charge   0.087439
set ${selection}.N3    charge  -0.728534
set ${selection}.C4    charge   0.376584
set ${selection}.H7    charge   0.421482
set ${selection}.H1    charge   0.0
set ${selection}.H3    charge   0.0
EOF
}

